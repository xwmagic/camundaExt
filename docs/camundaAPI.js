//查看本地接口
http://localhost:8080/camunda-swagger-ui.html

//查看官网api
https://docs.camunda.org/manual/7.3/api-references/rest/#deployment-post-deployment

//java使用流程的过程讲解
https://blog.csdn.net/fly_fly_fly_pig/article/details/81475395

//java发起流程案例讲解
https://blog.csdn.net/qq_28767795/article/details/81239925

//ProcessDefinition/ProcessInstance/Execution/Task关系和区别
https://blog.csdn.net/yy_done/article/details/6317259

//camunda工作流（我的已办）代码示例
https://blog.csdn.net/weixin_33801856/article/details/88000430

//显示流程图示例代码 下载地址
https://codeload.github.com/camunda/camunda-bpmn.js/zip/master

//展示流程图
https://wenku.baidu.com/view/7f0621e84793daef5ef7ba0d4a7302768e996f99.html