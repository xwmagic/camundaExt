Ext.define('Admin.view.common.LRDisplayText', {
    extend: 'Ext.Container',
    xtype: 'lrdisplaytext',
    
    controller: 'lrdisplaytextcontroller',

    layout: {
        type: 'absolute'
    },
    
    text: '温度',
    x: 50,
    y: 50,
    size: 60,
    newSize: 60,
    max: 100,
    min: 0,
    // myUnit: '℃',
    myUnit: '',
    value: '------',
    orgText: '',
    titleColor:'',//标题颜色样式
    
    widthScale: 100/60,//宽的比例
    titleFontSizeScale: 20/60, //标题字体的比例
    disFontSizeScale: 25/60, //数字的字体比例
    borderScale: 4/60, //边框的比例
    labelHeightScale: 2/6, //标题框高的比例
    dispHeightScale: 4/6, //数字框高的比例
    titleMarginScale: 0,

    initComponent: function(){
        var th = this;
        var max = th.max;
        var min = th.min;
        th.orgText = th.text;
        if (th.text.length <= 4) {
            th.titleFontSizeScale = 20/60;
        }else if (th.text.length == 5) {
            th.titleFontSizeScale = 20/60;
        }else if (th.text.length == 6) {
            th.titleFontSizeScale = 16/60;
        }else if (th.text.length == 7) {
            th.titleFontSizeScale = 14/60;
        }else if (th.text.length == 8) {
            th.titleFontSizeScale = 12/60;
        }else if (th.text.length >= 9&&th.text.length <=10) {
            th.titleFontSizeScale = 18/60;
        }else if (th.text.length >= 11&&th.text.length <=12) {
            th.titleFontSizeScale = 16/60;
        }else if (th.text.length >= 13&&th.text.length <=14) {
            th.titleFontSizeScale = 14/60;
        }else if (th.text.length >= 15&&th.text.length <=16) {
            th.titleFontSizeScale = 12/60;
        }else{
            th.titleFontSizeScale = 12/60;
        }

        if (th.text.length >= 9) {
            th.titleMarginScale = -24/60;
        }

        
        th.width = th.newSize*th.widthScale;
        th.height = th.newSize;
        var margin_top = th.titleMarginScale*th.height;
        var titleFontSize = th.height*th.titleFontSizeScale;
        var disFontSize = th.height*th.disFontSizeScale;
        var border = th.height*th.borderScale;
        var labelHeight = th.height*th.labelHeightScale;
        var dispHeight = th.height*th.dispHeightScale-border*2;


        if (th.orgText.length >16) {
            th.text = Ext.util.Format.ellipsis(th.text,16);
        }
        th.items = [{
            x: 0,
            y: 0,
            width: th.width,
            height: labelHeight,
            xtype: 'label',
            name: 'lab',
            text: th.text,
            style: {
                'color': th.titleColor,
                'text-align':'center',
                'font-size':titleFontSize+'px',
                'font-weight': 'bold',
                'line-height': labelHeight+'px',
                'margin':''+margin_top+'px 0px 0px 0px'
                // 'border': 'solid '+1+'px'+ ' #7CACD3'
            }
        },
        // { 
        //     x: 0,
        //     y: labelHeight,
        //     width: th.width,
        //     // height: dispHeight+'px',
        //     xtype: 'label',//displayfield
        //     name: 'disp',
        //     value: th.value+th.unit,
        //     padding: ''-+border*2+'px 0 0 0',
        //     fieldStyle: {
        //         'background-color':'#000000',
        //         'color':'#00ff00',
        //         'font-size':disFontSize+'px',
        //         'text-align':'center',
        //         'border': 'solid '+border+'px'+ ' #7CACD3',
        //         'line-height': ''+dispHeight+'px',
        //         'height': dispHeight+'px'
        //     }
        // },
        { 
            x: 0,
            y: labelHeight,
            width: th.width,
            height: dispHeight,
            xtype: 'label',//displayfield
            name: 'disp',
            text: th.value+th.unit,
            padding: ''-+border*2+'px 0 0 0',
            style: {
                'background-color':'#000000',
                'color':'#00ff00',
                'font-size':disFontSize+'px',
                'text-align':'center',
                'border': 'solid '+border+'px'+ ' #7CACD3',
                'line-height': ''+(dispHeight-6)+'px'
                // 'height': dispHeight+'px'
            }
        }
        ];
        
        th.callParent();
    },

    listeners: {
        boxready: 'lrdisplaytextBoxready'
    }
});