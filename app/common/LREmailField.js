Ext.define('Admin.view.common.LREmailField', {
    extend: 'Ext.form.TextField',
    xtype: 'lremailfield',
	vtype: 'email'
});
