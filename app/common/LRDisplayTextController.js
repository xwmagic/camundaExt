Ext.define('Admin.view.common.LRDisplayTextController', {
	extend : 'Ext.app.ViewController',
    alias: 'controller.lrdisplaytextcontroller',

    lrdisplaytextBoxready: function(view, width, height) {
    	// console.log('lrdisplaytextBoxready');
        var label = view.down('label[name=lab]');
        var id = label.getId();
        Ext.create('Ext.tip.ToolTip', {
            target: id,
            html: view.orgText
        });
        this.setSizeWithValue(view.size, view.size);
    },

    setValueAndColor:function(obj) {
        var value, color;
        value = this.isHasDot(obj.value);
        color = obj.color;
    	var lrdisplaytextView = this.getView();
    	if (lrdisplaytextView) {
    		// var disp = lrdisplaytextView.down('displayfield[name=disp]');
            var disp = lrdisplaytextView.down('label[name=disp]');
    		if (disp) {
                if (obj.onlineStatus == -1) {//离线
                    // disp.setValue('------');
                    disp.setText('------');
                    disp.setFieldStyle({
                        color: "#AAAAAA"
                    });
                }else{//在线
                    if (value) {
                        // var oldValue = disp.getValue();
                        var oldValue = disp['text'];
                        if (oldValue != value) {
                            // disp.setValue(value+lrdisplaytextView.unit);
                            if (lrdisplaytextView.unit) {
                                disp.setText(value+lrdisplaytextView.unit);
                            }else{
                                disp.setText(value);
                            }
                        }
                        var fieldStyle = disp.style;
                        if (fieldStyle) {
                            var oldColor = fieldStyle.color;
                            if (color) {
                                if (oldColor != color) {
                                    disp.setStyle({
                                        color: color
                                    });
                                }
                            }
                        }
                    }
                }
    		}
    	}
    },

    setTitleStyle: function(titleColor) {
        var lrdisplaytextView = this.getView();
        if(lrdisplaytextView){
            var title = lrdisplaytextView.down('label[name=lab]');
            if (title) {
                var selfStyle = title.style;
                if (selfStyle) {
                    selfStyle.color = titleColor;
                    title.setStyle({
                        color: titleColor
                    });
                    title.style = selfStyle;
                }
            }
        }
    },

    setSizeWithValue:function(sizeValue, newSize) {
    	if (sizeValue) {
    		var lrdisplaytextView = this.getView();
	    	if (lrdisplaytextView) {
    			lrdisplaytextView.size = sizeValue;
                // lrdisplaytextView.newSize = newSize;
	            var title = lrdisplaytextView.down('label[name=lab]');
	            // var disp = lrdisplaytextView.down('displayfield[name=disp]');
                var disp = lrdisplaytextView.down('label[name=disp]');
	            //title字体比例
	            var titleFontSize = lrdisplaytextView.titleFontSizeScale*(newSize);
	            //disp字体比例
	            var disFontSize = lrdisplaytextView.disFontSizeScale*(newSize);
	            //边框比例
	            var border = lrdisplaytextView.borderScale*newSize;
	            var width = newSize*lrdisplaytextView.widthScale;
	            var labelHeight = newSize*lrdisplaytextView.labelHeightScale;
	            var dispHeight = newSize*lrdisplaytextView.dispHeightScale-border*2;

	            lrdisplaytextView.setHeight(newSize);
	            //宽的比例
	            lrdisplaytextView.setWidth(width);
                var margin_top = lrdisplaytextView.titleMarginScale*newSize;
	            title.setWidth(width);
	            title.setHeight(labelHeight);
	            title.setStyle({
	                'font-size':titleFontSize+'px',
	                'line-height': labelHeight+'px',
                    'margin':''+margin_top+'px 0px 0px 0px'
	            });

	            disp.setWidth(width);
	            disp.setLocalY(labelHeight);
	            disp.setHeight(dispHeight);
	            // disp.setFieldStyle({
	            //     'font-size':disFontSize+'px',
	            //     'border': 'solid '+border+'px'+' #7CACD3',
	            //     'line-height': dispHeight+'px',
             //        'height': dispHeight+'px'
	            // });
                disp.setStyle({
                    'font-size':disFontSize+'px',
                    'border': 'solid '+border+'px'+' #7CACD3',
                    'line-height': (dispHeight-7/4*border)+'px'
                    // 'height': dispHeight+'px'
                });
	    		
	    	}
    	}
    },

    isHasDot: function(value){
        if (!this.rep) {
            this.rep = /[\.]/;
        }

        if (this.rep.test(value)) {
           value = Ext.util.Format.number(value,'0.00');
        }

        return value;
    }
});