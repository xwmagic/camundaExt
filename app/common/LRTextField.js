Ext.define('Admin.view.common.LRTextField', {
    extend: 'Ext.form.field.Text',
    xtype: 'lrtext',
    regex: new RegExp("^([\u4e00-\u9fa5]+|[a-zA-Z0-9\u4e00-\u9fa5]+)$"),
    regexText: '不能输入空格或特殊字符！'
});
