/**
 * Created by lrwl on 10/9/2016.
 */
    Ext.define('Admin.view.common.LRPrompt', {
    extend: 'Ext.container.Container',
    xtype: 'lrprompt',
    border: 1,
    layout: 'absolute',
    max: 100,
    min: 0,
    propertyName: '',
    propertyName2: '', 
    propertyUnit: "",
    propertyUnit2: "", 
    width: 220,
    value: '----',
    value2: '----',

    initComponent: function() {
    	var me = this;
    	var height = 100;
        this.height = 100;
		var max = me.max;
		var min = me.min;
		var width = this.width;
		var ySpace = 2;
		var DFTitleHeight = (height-ySpace)*0.4;
		var DFValueHeight = (height-ySpace)*0.6;
		var DFVauleY = DFTitleHeight+ySpace;
		var dfTitleBorder = 1;
		var dfVauleBorder = 3;
		var dfTitle1Value;
		if (this.propertyName&&this.propertyUnit&&this.propertyUnit!="") {
			dfTitle1Value = this.propertyName+'('+this.propertyUnit+')';
		}else{
			dfTitle1Value = this.propertyName;
		}
		var dfTitle1Value2;
		if (this.propertyName2&&this.propertyUnit2&&this.propertyUnit2!="") {
			dfTitle1Value2 = this.propertyName2+'('+this.propertyUnit2+')';
		}else{
			dfTitle1Value2 = this.propertyName2;
		}
		var titleFieldStyle = {
			'line-height': DFTitleHeight+'px',
	        'color':'#fff',
	        'font-size':'18px',
        	'background':'#7CACD3',
        	'border':'solid '+dfTitleBorder+'px #000000',
        	'text-align':'center'
		};
		var valueFieldStyle = {
			'line-height': DFValueHeight+'px',
	        'color':'#00ff00',
	        'fontWeight': 'bolder',
	        'font-size':'25px',
        	'background':'#000000',//#c9c9c9
        	'border':'solid '+dfVauleBorder+'px #7CACD3',
        	'text-align':'center'
		};
    	if (this.propertyName2&&this.propertyName2 != "") {
    		var middlesSpace = 5;
    		this.height = height*2+middlesSpace;
    		this.items = [
    			{
					xtype: 'displayfield',
					name: 'dfTitle',
					value: dfTitle1Value,
					width: width,
	                // y: (height+middlesSpace)*i,
					height: DFTitleHeight,
					fieldStyle: titleFieldStyle
				},
				{
					xtype: 'displayfield',
					name: 'dfVaule',
					value: this.value,
					y: DFVauleY,
					width: width,
					height: DFValueHeight,
					fieldStyle: valueFieldStyle
				},
				{
					xtype: 'displayfield',
					name: 'dfTitle2',
					value: dfTitle1Value2,
					width: width,
	                y: height+middlesSpace,
					height: DFTitleHeight,
					fieldStyle: titleFieldStyle
				},
				{
					xtype: 'displayfield',
					name: 'dfVaule2',
					value: this.value2,
					y: height+middlesSpace+DFVauleY,
					width: width,
					height: DFValueHeight,
					fieldStyle: valueFieldStyle
				}
    		];
    	}else{
			this.height = height;
    		this.items = [
    			{
					xtype: 'displayfield',
					name: 'dfTitle',
					value: dfTitle1Value,
					width: width,
	                // y: (height+middlesSpace)*i,
					height: DFTitleHeight,
					fieldStyle: titleFieldStyle
				},
				{
					xtype: 'displayfield',
					name: 'dfVaule',
					value: this.value,
					y: DFVauleY,
					width: width,
					height: DFValueHeight,
					fieldStyle: valueFieldStyle
				}
    		];
    	}

		this.callParent();
	}

});