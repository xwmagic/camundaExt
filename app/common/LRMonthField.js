Ext.define('Admin.view.common.LRMonthField', {
    extend: 'Ext.form.field.Date',
    xtype: 'lrmonthfield',
	format:'Y-m',
    createPicker: function() {
        var me = this,
            format = Ext.String.format;

        // Create floating Picker BoundList. It will acquire a floatParent by looking up
        // its ancestor hierarchy (Pickers use their pickerField property as an upward link)
        // for a floating component.
        return new Ext.picker.Month({
            pickerField: me,
            floating: true,
            preventRefocus: true,
            hidden: true,
            minDate: me.minValue,
            maxDate: me.maxValue,
            disabledDatesRE: me.disabledDatesRE,
            disabledDatesText: me.disabledDatesText,
            ariaDisabledDatesText: me.ariaDisabledDatesText,
            disabledDays: me.disabledDays,
            disabledDaysText: me.disabledDaysText,
            ariaDisabledDaysText: me.ariaDisabledDaysText,
            format: me.format,
            showToday: me.showToday,
            startDay: me.startDay,
            minText: format(me.minText, me.formatDate(me.minValue)),
            ariaMinText: format(me.ariaMinText, me.formatDate(me.minValue, me.ariaFormat)),
            maxText: format(me.maxText, me.formatDate(me.maxValue)),
            ariaMaxText: format(me.ariaMaxText, me.formatDate(me.maxValue, me.ariaFormat)),
			handleDateClick: function(e, t) {
				var me = this,
					handler = me.handler,
					date,
					timeDate;

				e.stopEvent();
				if(!me.disabled && t.dateValue && !Ext.fly(t.parentNode).hasCls(me.disabledCellCls)){
					date = new Date(t.dateValue);
					timeDate = new Date(me.time.getValue());
					date.setHours(timeDate.getHours());
					date.setMinutes(timeDate.getMinutes());
					date.setSeconds(timeDate.getSeconds());
					me.setValue(date);
					me.fireEvent('select', me, me.value);
					if (handler) {
						handler.call(me.scope || me, me, me.value);
					}
					// event handling is turned off on hide
					// when we are using the picker in a field
					// therefore onSelect comes AFTER the select
					// event.
					me.onSelect();
				}
			},
            listeners: {
                scope: me,
                select: me.onSelect,
                tabout: me.onTabOut
            },
            keyNavConfig: {
                esc: function() {
                    me.inputEl.focus();
                    me.collapse();
                }
            }
        });
    },
	onSelect: function(m, d) {
        var me = this;

        me.setValue(d);
        me.fireEvent('select', me, d);
        me.collapse();
    }
	
});
