/**
 * Created by lrwl on 10/9/2016.
 */
Ext.define('Admin.view.common.LRDisplayfield', {
    extend: 'Ext.Container',
    xtype: 'lrdisplayfield',
    items: [
        {
        	xtype: 'displayfield',
		    // width: 150,
		    // height: 50,
		    value: '25.00',
		    fieldStyle :{
		     	'line-height': '50px',
		        'color':'#00ff00',
		        'font-size':'20px',
	        	'background':'#000000',
	        	'border':'solid 3px #7CACD3',
	        	'text-align':'center'
		    }
        }
    ]
});