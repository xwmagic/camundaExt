Ext.define('LRAjax', {
	request: function(configs){
		
		Ext.Ajax.request({
			url: configs.url ? configs.url : '',
			params: configs.params ? configs.params : '',
			method: configs.method ? configs.method : 'post',
			success: function(response, opts) {
				var obj = Ext.decode(response.responseText);
				if(obj.success){
					configs.success(obj);
				}else{
                    if(obj.errorMessage){
                        App.ux.Toast.show('提示',obj.errorMessage,'e');
                    }
                    if(configs.failure){
                        configs.failure(obj, opts);
                    }
				}
			},

			failure: function(response, opts) {
				
				configs.failure(response, opts);
			}
		});
	}
	
});
