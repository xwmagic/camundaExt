Ext.define('Admin.view.common.LRORGTypeComboBox', {
    extend: 'Admin.view.common.LRComboBox',
    xtype: 'lrorgtypecombobox',
    
	initComponent: function() {
		this.on({
			beforerender: function(obj){
				var store = obj.getStore();
				if(obj.value){
					store.clearFilter();
					store.filterBy(function(record){
						return record.get('id') >= obj.value;
					});
				}
			}
		});
        this.callParent();
    }
});