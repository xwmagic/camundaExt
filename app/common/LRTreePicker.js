Ext.define('Admin.view.common.LRTreePicker', {
    extend: 'Ext.ux.TreePicker',
    xtype: 'lrtreepicker',
	editable: true,
	enableKeyEvents: true,
	displayField: 'name',
	valueField: 'id',
	minPickerHeight: 300,
	combUrl: '',//远程请求地址
	isSynMode: false, //同步请求与异步请求设置，默认异步请求
	myFields: ['name','id'],
	
	initComponent: function() {
		var me = this;
		this.store = Ext.create('Ext.data.TreeStore', {
			fields: this.myFields
		});
		
		if(this.combUrl){
			var store = this.store;
			if(this.isSynMode){//如果是同步模式
				LRAjax.request({
					url: this.combUrl,

					success: function(obj) {
						store.setRoot(obj.tree);
						if(me.value){
							me.setValue(me.value);
						}
					}
				});
			}else{//否则采用异步请求
				this.store = Ext.create('Ext.data.TreeStore', {
					fields: this.myFields,
					proxy: {
						type: 'ajax',
						url: this.combUrl,
						reader: {
							type: 'json',
							rootProperty: 'tree.children'
						}
					},
					root: {                
						expanded: true, 
						children: 'children'
					}, 
					autoLoad: true
				});
			}
		}
		
		this.listeners = {
			keyup: function(obj,e){
				if(e.keyCode == 13 || e.keyCode == 108 || e.keyCode == 27){
					return;
				}
				if(!this.isExpanded){
					this.expand();
					this.focus();
				}
				var picker = this.getPicker();
				var val = this.getRawValue(); 
				picker.filterByText(val);
				if(e.keyCode == 40 || e.keyCode == 38){
					picker.focus();
					var r;
					if(!val){
						r = picker.getStore().getAt(1);
					}else{
						var reg = new RegExp(val, "i");
						r = picker.getStore().findRecord('name',reg,1);
					}
					
					
					picker.getSelectionModel().select(r);
					picker.getView().focusNode(r);
				}
			}
		};
		
        this.callParent();
    },
	createPicker: function() {
        var me = this,
            picker = Ext.create('Admin.view.autoviews.AutoTree',{
                baseCls: Ext.baseCSSPrefix + 'boundlist',
                shrinkWrapDock: 2,
                store: me.store,
                floating: true,
				rootVisible: false,
				lrAction: me.combUrl,
                displayField: me.displayField,
                columns: me.columns,
                isSynMode: me.isSynMode,
                minHeight: me.minPickerHeight,
                maxHeight: me.maxPickerHeight,
                manageHeight: false,
                shadow: false,
                tbar:[
                '->',
                {   
                	text:'刷新',
	                iconCls: 'x-fa fa-refresh',
	                handler:function(obj){
	                	treepanel = obj.up('treepanel');
	                	treepanel.refreshDatas();
               		}
            	}],
                listeners: {
                    scope: me,
                    itemclick: me.onItemClick,
                    itemkeydown: me.onPickerKeyDown,
					render : function(input) {
						new Ext.KeyMap(input.getEl(), [{
							key : 27,
							fn : function(){
								me.collapse();
							},
							scope : this
						}]);
					}
                }
            }),
            view = picker.getView();
        if (Ext.isIE9 && Ext.isStrict) {
            // In IE9 strict mode, the tree view grows by the height of the horizontal scroll bar when the items are highlighted or unhighlighted.
            // Also when items are collapsed or expanded the height of the view is off. Forcing a repaint fixes the problem.
            view.on({
                scope: me,
                highlightitem: me.repaintPickerView,
                unhighlightitem: me.repaintPickerView,
                afteritemexpand: me.repaintPickerView,
                afteritemcollapse: me.repaintPickerView
            });
        }
        return picker;
    }
});
