/**
 * Created by lrwl on 8/23/2016.
 */
Ext.define('Admin.view.common.LRCircleText', {
    extend: 'Ext.Container',
    xtype: 'lrcircletext',
    
    
    width: 50,
    height: 50,
//    border: 1,
    myValue: 'M',
    mybgColor: '#00FF00',
    
    
    items: [
        /* include child components here */
    ],
    
    initComponent: function() {
    	var fontSize = this.width;
    	var mybgColor = this.mybgColor;
    	if(this.myFontSize){
    		fontSize = this.myFontSize;
    	}
    	fontSize = fontSize + 'px';
    	var myValue = this.myValue;
    	this.html='<span style="font-size:'+fontSize+'; line-height:'+fontSize+'; display:block; color:#000000; text-align:center">'+myValue+'</span>';
    	console.log(this.html);
    	this.style = {
//	    	borderColor:'#000000', 
//	    	borderStyle:'solid', 
//	    	borderWidth:'1px',
	    	'background-color':mybgColor, 
	    	'border-radius':this.width/2+'px'
		};
    	
    	
    	
    	this.callParent();
    }
});