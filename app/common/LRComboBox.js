Ext.define('Admin.view.common.LRComboBox', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'lrcombobox',
    
	queryMode: 'local',
    displayField: 'name',
    valueField: 'id',
	combDatas: [],//存储数据
	combUrl: '',//远程请求地址
	autoLoad: true,
	initComponent: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['id', 'name'],
			data : this.combDatas
		});
		if(this.combUrl){
			this.store = Ext.create('Ext.data.Store', {
				fields: ['id', 'name'],
				proxy: {
					type: 'ajax',
					url: this.combUrl,
					reader: {
						type: 'json',
						rootProperty: 'list'
					}
				},
				autoLoad: this.autoLoad
			});
		}
        this.callParent();
    }
});