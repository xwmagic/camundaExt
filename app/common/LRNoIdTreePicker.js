Ext.define('Admin.view.common.LRNoIdTreePicker', {
    extend: 'Admin.view.common.LRSynTreePicker',
    xtype: 'lrnoidtreepicker',
    
	initComponent: function() {
		
		this.callParent();
		var me = this;
		
		var picker = this.getPicker();
		picker.on({
			beforeitemclick: function(th, record, item, index, e, eOpts ){
				
				var id = record.get('reqParamValue');
				var paramName = record.get('reqParamName');
				var arr = paramName.split('.');
				
				if(arr[1] && arr[1] == 'id'){
					me.name = arr[0]+'.parentId';
				}else{
					me.name = paramName;
				}
				me.reqParamValue = id;
			}
		});
	},
	
	selectItem: function(record) {
        var me = this;
        me.setValue(record.get('reqParamValue'));
        me.fireEvent('select', me, record);
        me.collapse();
    },
	
    setValue: function(value) {
        var me = this
            ,record
			,dataReps
			,reps
			,dataField
			;

        me.value = value;

        if (me.store.loading) {
            // Called while the Store is loading. Ensure it is processed by the onLoad method.
            return me;
        }
            
        record = me.store.getRoot();
		
		var reqParamName = me.name.split('.');
		if(reqParamName[1] && reqParamName[1] == 'parentId'){//重写rep
			reqParamName = reqParamName[0] + '.id';
		}else{
			reqParamName = me.name;
		}
		if(record){
			
			me.store.each(function(data){
				if(data.get('reqParamName')){
					if(data.get('reqParamValue') == value){//首先判断ID值是否相等
						if(data.get('reqParamName') == reqParamName){//其次返回的数据data的对象名称与当前控件的reqParamName是否相等
							record = data;
							return false;
						}else{//如果不相等，在通过拼接字段名称dataField与当前对象的字段名称reps[1]进行对比
							dataReps = data.get('reqParamName').split('.');
							reps = reqParamName.split('.');
							
							//拼接后的名称
							dataField = dataReps[0]+ dataReps[1].charAt(0).toUpperCase() + dataReps[1].substring(1);
							if(dataField == reps[1]){//开始对比，如果相等则返回当前节点
								record = data;
								return false;
							}
						}
					}
				}
				
			},this,{filtered:true,collapsed : true});
			
			if (value === undefined) {
				me.value = '';
			}
		}
		

        // set the raw value to the record's display field if a record was found
        me.setRawValue(record ? record.get(me.displayField) : '');

        return me;
    }
});