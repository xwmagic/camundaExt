Ext.define('App.view.ShadowContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.ShadowContainer',
    cls: 'container-shadow'
});