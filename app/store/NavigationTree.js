Ext.define('Admin.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'NavigationTree',

    fields: [{
		text: 'text'//,
		//mapping: 'name'
	}],
    
    root: {
        // expanded: true,
        children: [
			/*{
			    name: '实时监控',
			    iconCls: 'x-fa fa-bank',
			    expanded: false,
			    selectable: false,
			    children: [
			        {
					    name: '电镀一线',
					    iconCls: 'x-fa fa-magic',
					    viewType: 'monitor',
					    routeId: 'monitor', // routeId defaults to viewType
					    leaf: true
					}
			    ]
			},
			{
			    name: '数据管理',
			    iconCls: 'x-fa fa-forumbee',
			    expanded: false,
			    selectable: false,
			    children: [
			        {
					    name: '自动列表',
					    iconCls: 'x-fa fa-magic',
					    viewType: 'lrautogrid',
					    leaf: true
					},{
					    name: '历史数据',
					    iconCls: 'x-fa fa-magic',
					    viewType: '',
					    leaf: true
					},
					{
			            name: '异常数据',
			            iconCls: 'x-fa fa-map-marker',
			            viewType: '',
			            leaf: true
			        }
			    ]
			},{
			    name: '数据分析',
			    iconCls: 'x-fa fa-cogs',
			    expanded: false,
			    selectable: false,
			    children: [
			        {
			            name: '趋势分析',
			            iconCls: 'x-fa fa-sitemap',
			            viewType: '',
			            leaf: true
			        },
			        {
			            name: '异常分析',
			            iconCls: 'x-fa fa-user',
			            viewType: '',
			            leaf: true
			        }
			    ]
			},{
				name: '报表统计',
			    iconCls: 'x-fa fa-cogs',
			    viewType: '',
			    leaf: true
            },{
                name: '实验记录',
                iconCls: 'x-fa fa-cogs',
                viewType: '',
                leaf: true
            },{
                name: '参数设置',
                iconCls: 'x-fa fa-cogs',
                expanded: false,
                selectable: false,
                children: [
                   {
                       name: '工序设置',
                       iconCls: 'x-fa fa-sitemap',
                       viewType: '',
                       leaf: true
                   },
                   {
                      name: '工艺参数设置',
                      iconCls: 'x-fa fa-user',
                      viewType: '',
                      leaf: true
                   }
                ]
             },
             {
			    name: '系统管理',
			    iconCls: 'x-fa fa-cogs',
			    expanded: false,
			    selectable: false,
			    children: [
			        {
			            name: '组织架构管理',
			            iconCls: 'x-fa fa-sitemap',
			            viewType: '',
			            leaf: true
			        },
			        {
			            name: '用户管理',
			            iconCls: 'x-fa fa-user',
			            viewType: '',
			            leaf: true
			        },
			        {
			            name: '角色管理',
			            iconCls: 'x-fa fa-group',
			            viewType: '',
			            leaf: true
			        },
			        {
			            name: '技术文档',
			            iconCls: 'x-fa fa-file',
			            viewType: '',
			            leaf: true
			        }
			    ]
			}*/
        ]
    }

	// proxy: {
		// type: 'ajax',
		// url: 'menu/tree',
		// reader: {
			// type: 'json',
			// rootProperty: 'users'
		// }
	// },
	// autoLoad: true
    
});

