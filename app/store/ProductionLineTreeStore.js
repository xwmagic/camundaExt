Ext.define('Admin.store.ProductionLineTreeStore', {
    extend: 'Ext.data.TreeStore',
    alias: 'store.productionlinestore',
    fields: ['name', 'id'],
	proxy: {
		type: 'ajax',
		url: 'fmainconfig/index',
		reader: {
			type: 'json',
			rootProperty: 'tree'
		}
	},

	root: {                
		expanded: true, 
		children: 'children'
	}, 
	autoLoad: true
});