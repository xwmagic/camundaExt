Ext.define('Admin.store.LineChartStore',{
	extend: 'Ext.data.Store',

	alias: 'store.linechartstore',
    
    // fields: ['name', 'data1'],

    initComponent:function() {
    	this.fields = ['time', 'value'];
    	// this.callParent();
    }
});