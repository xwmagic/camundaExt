/**
 * 流程处理常用公共方法工具类
 * @author xiaowei
 * @date 2019年5月2日
 */
Ext.define('App.ux.ProcessUtil', {
    statics:{
        //是否第一次发起流程
        isFirstRunProcess:function (recordData) {
            if(!recordData.processInstanceId && !recordData.processStatus){
                return true;
            }
            return false;
        },
        //是否是起草节点
        isStartNode:function (recordData) {
            //起草节点分为两种情况，1是起草节点id为startTask。2是从未发过流程
            if(recordData.curNodesId == 'startTask'
                || App.ux.ProcessUtil.isFirstRunProcess(recordData)){
                return true;
            }
            return false;
        },
        //是否是结束节点
        isEndNode:function (recordData) {
            //流程状态为4，表示流程完成
            if(recordData.processStatus==4){
                return true;
            }
            return false;
        },
        //起草状态下，可以编辑
        isCanEditRecordData:function (recordData) {
            //起草节点可以编辑
            if(App.ux.ProcessUtil.isStartNode()){
                return true;
            }
            return false;
        },
        isCanEditProcessStatus:function (processStatus) {
            //流程状态不存在、为0草稿或者为3驳回时，可以编辑页面内容
            if(!processStatus || processStatus==0 || processStatus==3){
                return true;
            }
            return false;
        },
        /**
         * 判断流程是否结束
         * @param processStatus
         * @returns {boolean}
         */
        isDone:function (processStatus) {
            if(processStatus == 4){
                return true;
            }
            return false;
        },
        //判断是否发起过流程
        hadRunProcess:function () {

        }
    }

});

