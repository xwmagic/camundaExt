Ext.define('App.ux.Toast',{
	statics: {
	    //type: success s ,e error,w warning,i info,ext ext提示模式
		show:function(title,html,type){
            if(type === 'i' || !type){
                $.message({
                    message:html,
                    type:'info'
                });
            }
		    else if(type === 's'){
                $.message(html);
            }
            else if(type === 'e'){
                $.message({
                    message:html,
                    type:'error'
                });
            }
            else if(type === 'w'){
                $.message({
                    message:html,
                    type:'warning'
                });
            }else if(type === 'ext'){
                Ext.toast({
                    title : title,
                    html : html,
                    align : 't',
                    closable : true,
                    slideInDuration : 150,
                    minWidth : 300,
                    minHeight : 30
                });
            }
		}
	}
	
});