/*
 * 富文本编辑框控件
 * Created by GDC. 2016.12.25
 */
Ext.define('App.ux.ExtKindEditor', {
      extend: 'Ext.form.field.TextArea',
      alias: 'widget.extkindeditor',//xtype名称
      initComponent: function () {   
      	//Ext.loadScript('/CsotSample/kindeditor-4.1.7/kindeditor.js'); 
      	//Ext.loadScript('/CsotSample/kindeditor-4.1.7/lang/zh_CN.js');    	
        this.html = "<textarea id='" + this.getId() + "-input' name='" + this.name + "'></textarea>";
        this.callParent(arguments);
        this.on("boxready", function (t) {
              this.inputEL = Ext.get(this.getId() + "-input");
              this.editor = KindEditor.create('textarea[name="' + this.name + '"]', {
                 height: t.getHeight()-18,//有底边高度，需要减去
                 width: t.getWidth(),//宽度可能需要减去label的宽度
                 basePath: 'js/kindeditor-4.1.7/',
                 // uploadJson: '/CsotSample/kindeditor-4.1.7/jsp/upload_json.jsp',//路径自己改一下
                 // fileManagerJson: '/CsotSample/kindeditor-4.1.7/jsp/file_manager_json.jsp',//路径自己改一下
                 resizeType: 0,
                 wellFormatMode: true,
                 newlineTag: 'br',
                 allowFileManager: true,
                 allowPreviewEmoticons: true,
                 allowImageUpload: true,
								 items:	[
									        'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
									        'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
									        'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
									        'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
									        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
									        'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|',
									        'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
									        'anchor', 'link', 'unlink'
									]
             });
         });
         this.on("resize", function (t, w, h) {
             this.editor.resize(w , h-18);
         });
     },
     setValue: function (value) {
         if (this.editor) {
             this.editor.html(value);
         }
     },
     reset: function () {
         if (this.editor) {
             this.editor.html('');
         }
     },
     setRawValue: function (value) {
         if (this.editor) {
             this.editor.text(value);
         }
     },
     getValue: function () {
         if (this.editor) {
             return this.editor.html();
         } else {
             return ''
         }
     },
     getRawValue: function () {
         if (this.editor) {
             return this.editor.text();
         } else {
             return ''
         }
     }
 });