/**
 * 定义了一个背景透明的Button类，继承于Button
 */
Ext.define('App.ux.ButtonTransparent', {
    extend : 'Ext.button.Button', // 继续于Ext.button.Button
    //xtype : 'buttontransparent',
    alias : ['widget.buttonTransparent', 'widget.buttontransparent'], // 此类的xtype类型为buttontransparent
    textAlign : 'left',
    // 类初始化时执行
    initComponent : function() {
        // 设置事件监听
        this.listeners = {
            // 鼠标移开，背景设置透明
            mouseout : function() {
                this.setTransparent(document.getElementById(this.id));
            },
            // 鼠标移过，背景取消透明
            mouseover : function() {
                var b = document.getElementById(this.id);
                //b.style.backgroundColor = '';
                b.style.borderColor = '#fff';
            },
            // 背景设置透明
            afterrender : function() {
                this.setTransparent(document.getElementById(this.id));
            }
        };

        this.callParent(arguments); // 调用你模块的initComponent函数
    },

    setTransparent : function(b) {
        //b.style.backgroundColor = 'transparent';
        //b.style.backgroundColor = '#35baf6';
        b.style.borderColor = 'transparent';
    }
});