/**
 * @author xiaowei
 * @date 2017年4月25日
 */
Ext.define('App.ux.BigDecimal',{
	extend: 'Ext.data.field.Number',
	
	alias: 'data.field.bigdecimal',
	
	convert: function (value) {
		if(value == "null"){
			return '';
		}
		return value;
    }
});