/*
 * 页面Panel基类,实现自动抓取页面元素权限并控制相应元件
 * Created by GDC. 2016.5.25
 */
Ext.define('App.ux.BasePanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'base-panel',
    //是否开启权限控制
    openAuthControl:true,
    scrollable:'y',
    requires: [
        'App.view.BaseViewModel'
    ],
    viewModel: {
        type: 'baseviewmodel'
    },
    initComponent: function () {//连接集中权限系统抓取该用户在页面上元素的权限
        var me = this;
        this.on({
            beforerender: function () {
                me.hideCommonToolbar();
            }
        });
        var leftMenu = Ext.getCmp('main-menu-navigationTreeList');
        if (leftMenu) {
            var store = leftMenu.getStore();
            var record = store.findNode('viewType', me.xtype);
            if (record) {
                if(record.data.menuConfig){
                    for(var i in record.data.menuConfig){
                        if (me.getViewModel().get(record.data.menuConfig[i].perBtnValid)) {
                            me.getViewModel().set(record.data.menuConfig[i].perBtnValid, false);
                        }
                    }
                }
            }
        }
        this.callParent(arguments);
        this.showCommonToolbar();
    },
    hideCommonToolbar: function () {
        var commontoolbar = this.down('commontoolbar');
        if (commontoolbar && this.openAuthControl) {
            commontoolbar.hide();
        }
    },
    showCommonToolbar: function () {
        var commontoolbar = this.down('commontoolbar');
        if (commontoolbar) {
            setTimeout(function () {
                commontoolbar._isHideMyself();
            }, 100);
        }
    },
    _refresh:function () {
        var grid = this.down('grid');
        if(grid && grid._refresh){
            grid._refresh();
        }
    }
});
