/**
 * 导出检验批数据编辑窗口
 */

Ext.define('App.ux.ExportWin', {
    extend: 'Ext.window.Window',
    alias: 'widget.exportWin',

    requires: [
        'Ext.button.Button',
        'Ext.form.FieldSet',
        'Ext.grid.Panel',
        'Ext.layout.container.Border',
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox'
    ],
    autoShow: true,
    layout: "fit",
    modal: true,
    width: 700,
    height: 460,
    iconCls: "fa fa-file-excel-o fa-lg",
    config: {
        page: 1,
        limit: 15
    },
    items: [
        {
            xtype: "fieldset",
            title: "导出字段",
            layout: "border",
            height: 400,
            items: [
                {
                    width: 300,
                    region: "west",
                    xtype: "grid",
                    name: "gridLeft",
                    border: true,
                    columns: [{
                        text: "全部字段",
                        style: "font-weight:bold",
                        menuDisabled: true,
                        flex: 1,
                        dataIndex: "fieldText"
                    }],
                    scope: this,
                    listeners: {
                        itemdblclick: function (view, record, item, index, e, eOpts) {
                            var rightGrid = this.up("window").down("grid[name='gridRight']");
                            var rightGridStore = rightGrid.store;
                            var rightRec = rightGridStore.findRecord("fieldName", record.data.fieldName);

                            if (!rightRec) {
                                rightRec = rightGridStore.insert(rightGridStore.data.length, record);
                            }
                            rightGrid.selModel.select(rightRec);
                        }
                    }
                },
                {
                    region: "center",
                    layout: "vbox",
                    bodyPadding: "50 0 0 20",
                    items: [
                        {
                            xtype: "button",
                            iconCls: "fa fa-forward",
                            handler: function (button) {
                                var leftGrid = button.up("window").down("grid[name='gridLeft']");
                                var selRec = leftGrid.getSelection();
                                if (selRec && selRec.length > 0) {
                                    var rightGrid = button.up("window").down("grid[name='gridRight']");
                                    var rightGridStore = rightGrid.store;
                                    var rec = selRec[0];
                                    var rightRec = rightGridStore.findRecord("fieldName", rec.data.fieldName);

                                    if (!rightRec) {
                                        rightRec = rightGridStore.insert(rightGridStore.data.length, selRec);
                                    }
                                    rightGrid.selModel.select(rightRec);
                                }

                            }
                        }, {
                            xtype: "button",
                            margin: '40 0 15 0',
                            iconCls: "fa fa-backward",
                            handler: function (button) {
                                var rightGrid = button.up("window").down("grid[name='gridRight']");
                                var selRec = rightGrid.getSelection();
                                if (selRec && selRec.length > 0) {
                                    rightGrid.store.remove(selRec[0]);
                                }
                            }
                        }, {
                            xtype: 'button',
                            name: 'upbut',
                            margin: '40 0 25 0',
                            iconCls: "fa fa-arrow-up",
                            tooltip: '上移',
                            handler: function (but) {
                                var rightGrid = but.up("window").down("grid[name='gridRight']");
                                var selRec = rightGrid.getSelectionModel().getSelection();
                                if (selRec && selRec.length > 0) {
                                    var index = rightGrid.store.indexOf(selRec[0]);
                                    if (index > 0) {
                                        rightGrid.store.removeAt(index);
                                        rightGrid.store.insert(index - 1, selRec[0]);
                                        rightGrid.getView().refresh();
                                        rightGrid.getSelectionModel().selectRange(index - 1, index - 1);
                                    }
                                } else {
                                    App.ux.Toast.show('提示', "请选择一条数据",'i');
                                }

                            }
                        }, {
                            xtype: 'button',
                            name: 'downBut',
                            iconCls: "fa fa-arrow-down",
                            tooltip: '下移',
                            handler: function (but) {
                                var rightGrid = but.up("window").down("grid[name='gridRight']");
                                var selRec = rightGrid.getSelectionModel().getSelection();
                                if (selRec && selRec.length > 0) {
                                    var record = selRec[0];
                                    var index = rightGrid.store.indexOf(record);
                                    if (index < rightGrid.store.getCount() - 1) {
                                        rightGrid.store.removeAt(index);
                                        rightGrid.store.insert(index + 1, record);
                                        rightGrid.getView().refresh();
                                        rightGrid.getSelectionModel().selectRange(index + 1, index + 1);
                                    }
                                } else {
                                    App.ux.Toast.show('提示', "请选择一条数据",'i');
                                }

                            }
                        }]
                }, {
                    width: 300,
                    region: "east",
                    xtype: "grid",
                    name: "gridRight",
                    border: true,
                    columns: [{
                        style: "font-weight:bold",
                        text: "已选字段",
                        flex: 1,
                        menuDisabled: true,
                        dataIndex: "fieldText"
                    }],
                    scope: this,
                    listeners: {
                        itemdblclick: function (view, record, item, index, e, eOpts) {
                            this.store.remove(record);
                        }
                    }
                }]

        }
    ],
    buttons: [{
        text: "确定",
        iconCls: 'fa fa-check fa-lg',
        handler: function (button) {
            var rightGrid = button.up("window").down("grid[name='gridRight']");
            var store = rightGrid.store;
            if (store.getCount() > 0) {
                var fields = "";
                store.each(function (record) {
                    fields += record.data.fieldName + ",";
                });

                var aryCond = button.up("window").queryCondition;
                var queryCondition = '';
                if (aryCond) {
                    var i = 0,
                        len = aryCond.length;
                    for (; i < len; i++) {
                        var name = aryCond[i].name;
                        var value = aryCond[i].value;
                        if (value) {
                            queryCondition = queryCondition + name + '=' + value + '&';
                        }

                    }
                }

                queryCondition = queryCondition.substr(0, queryCondition.lastIndexOf("&"));

                //增加分页数据
                var pageLimit = 'page=' + this.config.page + '&limit=' + this.config.limit;
                var url = button.up("window").exportUrl + queryCondition + "&fields=" + fields;
                window.location.href = url;
                Ext.toast({
                    title: '提示',
                    html: "导出数据成功，请留意正在下载文件",
                    align: 't',
                    closable: true,
                    slideInDuration: 150,
                    minWidth: 300,
                    minHeight: 30
                });
                button.up("window").close();
            } else {
                Ext.Msg.alert("提示", "当前没有选择任何导出字段");
            }

        }
    }, {
        text: "取消",
        iconCls: 'fa fa-times fa-1x',
        handler: function (btn) {
            btn.up("window").close();
        }
    }],
    listeners: {
        show: function (win, eOpts) {
            win.down("grid[name='gridLeft']").setStore(win.exportStore);
            win.down("grid[name='gridRight']").setStore(win.exportStore);
        }
    }

});