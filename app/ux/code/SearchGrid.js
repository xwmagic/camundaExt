/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.SearchGrid',{
    extend: 'App.ux.code.Grid',
	xtype: 'uxcodeSearchGrid',
    cName:'查询组件配置列表',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'name',width:150,align:'left'},
            {name: 'text',width:150,align:'left'},
            {name: 'length',width:150},
            {name: 'width',width:150},
            {name: 'component',width:300,editor:{xtype:'classicsystemcomponentsPicker'},align:'left'},
            {name: 'group',width:150},
            {name: 'flex',width:150},
            {name: 'hidden',width:150},
            {name: 'code',flex:1,align:'left'}
        ]
    },
    loadQueryData:function (datas) {
        var vals = [];
        for(var i in datas){
            if(datas[i].isQuery == '1'){
                vals.push(datas[i]);
            }
        }
        this.getStore().loadData(vals);
    },
    updateByJson: function () {
        var newBaseJson = this.NewBaseJsonConfig;
        var temp,notFind = [];
        var store = this.getStore();
        for (var i = 0; i < newBaseJson.length; i++) {
            temp = store.findRecord('name', newBaseJson[i].name);
            if (temp) {
                temp.set('text', newBaseJson[i].text);
                temp.set('type', newBaseJson[i].type);
                temp.set('length', newBaseJson[i].length);
            }else if(newBaseJson[i].isQuery == 1){
                notFind.push(Ext.clone(newBaseJson[i]));
            }
        }
        store.loadData(notFind,true);
    }
});