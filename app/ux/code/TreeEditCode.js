/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.TreeEditCodeCheck',{
    extend: 'Ext.form.FieldSet',
	xtype: 'uxcodeTreeEditCodeCheck',
    layout: 'hbox',
    defaults: {
        xtype: 'checkbox',
        margin: 10
    },
    items: [
        {
            boxLabel: 'Main.js',
            name: 'Main'
        }, {
            boxLabel: 'SearchForm.js',
            name: 'SearchForm'
        }, {
            boxLabel: 'Toolbar.js',
            name: 'Toolbar'
        }, {
            boxLabel: 'Datas.js',
            name: 'Datas'
        },{
            boxLabel: 'TreeEditGrid.js',
            name: 'Grid'
        },  {
            boxLabel: 'ViewModel.js',
            name: 'ViewModel'
        },  {
            boxLabel: 'WindowView.js',
            name: 'WindowView'
        }, {
            boxLabel: 'TreeGridMini.js',
            name: 'GridMini'
        }, {
            boxLabel: 'Picker.js',
            name: 'Picker'
        }, {
            text: '全选',
            xtype:'button',
            name: 'allCheck',
            handler: function (th) {
                var checks = th.up('fieldset').query('checkbox');
                for(var i in checks){
                    checks[i].setValue(true);
                }
            }

        }, {
            text: '取消',
            xtype:'button',
            name: 'allCancel',
            handler: function (th) {
                var checks = th.up('fieldset').query('checkbox');
                for(var i in checks){
                    checks[i].setValue(false);
                }
            }
        }
    ]
});