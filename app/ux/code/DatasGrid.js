/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.DatasGrid', {
    extend: 'App.ux.code.Grid',
    xtype: 'uxcodeDatasGrid',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'name', flex: 1,align:'left'},
            {name: 'text', flex: 1,align:'left'},
            {name: 'type', width: 150,align:'left'}
        ]
    },
    listeners:{
        render:function () {
            this.down('button[name="check"]').hide();
        }
    }
});