/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.ToolbarGrid', {
    extend: 'App.ux.code.Grid',
    cName: '工具栏组件配置列表',
    xtype: 'uxcodeToolbarGrid',
    _mainConfs: App.view.ux.Datas.ToolbarGrid,
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'id', width: 120,align:'left'},
            {name: 'tooltip', width: 120,align:'left'},
            {name: 'actionObject', width: 200, editor: {xtype: 'classicsystemcomponentsPicker'},align:'left'},
            {name: 'actionUrl', width: 200,align:'left'},
            {name: 'iconCls', width: 120, editor: {xtype: 'classicsystemiconsPickerTab'}},
            {name: 'text', width: 150},
            {name: 'code', flex: 1,align:'left'}
        ]
    },
    tools: [
        {
            xtype: 'button',
            text: '更新',
            name: 'update',
            margin: '0 5',
            tooltip: '根据上方表单配置进行更新',
            handler: function (btn) {
                btn.up('grid').updateByJson();
            }
        },
        {
            xtype: 'button', text: '历史字段',
            handler: function (btn) {
                var me = btn.up('grid');
                var win = Ext.create('Ext.window.Window', {
                    layout: 'fit',
                    width: 1000,
                    height: 600,
                    items: [{xtype: me.xtype}],
                    buttons: [{
                        text: '确定', handler: function () {
                            var winGrid = this.up('window').down('grid');
                            var selections = winGrid.getSelectionModel().getSelection();
                            var datas = [];
                            for (var i = 0; i < selections.length; i++) {
                                datas.push(Ext.clone(selections[i].data));
                            }
                            winGrid.getStore().remove(selections);
                            me.historyRemove = winGrid.getDatas();
                            me.getStore().loadData(datas, true);
                            win.close();
                        }
                    }, {
                        text: '取消',
                        handler: function () {
                            win.close();
                        }
                    }]
                });
                win.down('grid').getStore().loadData(me.historyRemove);
                win.show();
            }
        },
        'multiEdit','plus', 'minus'
    ],
    historyRemove: [
        {id: 'c', actionObject: '', actionUrl: '', iconCls: '', tooltip: '新增', fieldLabel: ''},
        {id: 'u', actionObject: '', actionUrl: '', iconCls: '', tooltip: '编辑', fieldLabel: ''},
        {id: 'd', actionObject: '', actionUrl: '', iconCls: '', tooltip: '删除', fieldLabel: ''},
        {id: 'copy', actionObject: '', actionUrl: '', iconCls: '', tooltip: '复制新增', fieldLabel: ''},
        {id: 'export', actionObject: '', actionUrl: '', iconCls: '', tooltip: '导出Excel', fieldLabel: ''},
        {id: 'import', actionObject: '', actionUrl: '', iconCls: '', tooltip: '导入Excel', fieldLabel: ''}
    ],//存放被删除的数据
    updateByJson: function () {
        var values = this.up('window').down('form').getValues();
        var allNames = this.getAllComponetNames(values);
        var datas = [];
        //根据新增、编辑、删除、导入、导出URL配置生成对应的操作按钮
        if(this.getStore().findRecord('id', 'c')){
            this.getStore().findRecord('id', 'c').set('actionObject',allNames.windowAddXtype);
        }else{
            datas.push({id: 'c', actionObject: allNames.windowAddXtype, actionUrl: '', iconCls: '', tooltip: '新增', fieldLabel: ''});
        }
        if(this.getStore().findRecord('id', 'u')){
            this.getStore().findRecord('id', 'u').set('actionObject',allNames.windowEditXtype);
        }else{
            datas.push({id: 'u', actionObject: allNames.windowEditXtype, actionUrl: '', iconCls: '', tooltip: '编辑', fieldLabel: ''});
        }
        if(this.getStore().findRecord('id', 'd')){
            this.getStore().findRecord('id', 'd').set('actionUrl',values.delUrl);
        }else{
            datas.push({id: 'd', actionObject: '', actionUrl: values.delUrl, iconCls: '', tooltip: '删除', fieldLabel: ''});
        }

        if (values.isExport) {
            datas.push({id: 'export', actionObject: '', actionUrl: values.exportUrl, iconCls: '', tooltip: '导出Excel', fieldLabel: ''});
            datas.push({id: 'import', actionObject: '', actionUrl: values.importUrl, iconCls: '', tooltip: '导入Excel', fieldLabel: ''});
        }
        this.getStore().loadData(datas,true);
    }
});