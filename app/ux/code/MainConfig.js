/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.MainConfig',{
    extend: 'Ext.form.FieldSet',
	xtype: 'uxcodeMainConfig',
    layout: 'hbox',
    defaults: {
        xtype: 'checkbox',
        margin: 10
    },
    items: [
        {
            boxLabel: '隐藏查询栏',
            name: 'hideMainSearch'
        }, {
            boxLabel: '隐藏工具栏',
            name: 'hideMainToolbar'
        }, {
            boxLabel: '是编辑树',
            name: 'editTree',
            listeners: {
                change: function (th) {
                    var win = th.up('uxCodeGenWindow');
                    if (th.getValue()) {
                        win.down('grid[name=mainForm]').hide();
                        win.down('grid[name=mainGrid]').hide();
                        win.down('grid[name=mainEditTreeGrid]').show();
                        win.down('[name=searchUrl]').setValue(win.preUrlName + 'findTree');
                        win.down('fieldset[name=treeEditCode]').show();
                        win.down('fieldset[name=treeEditCode]').enable();
                        win.down('fieldset[name=gridCode]').hide();
                        win.down('fieldset[name=gridCode]').disable();
                    } else {
                        win.down('grid[name=mainForm]').show();
                        win.down('grid[name=mainGrid]').show();
                        win.down('grid[name=mainEditTreeGrid]').hide();
                        win.down('[name=searchUrl]').setValue(win.preUrlName + 'pageList');
                        win.down('fieldset[name=treeEditCode]').hide();
                        win.down('fieldset[name=treeEditCode]').disable();
                        win.down('fieldset[name=gridCode]').show();
                        win.down('fieldset[name=gridCode]').enable();
                    }

                }
            }
        }, {
            boxLabel: '是常规树',
            disabled:true,
            name: 'normalTree'
        }
    ]
});