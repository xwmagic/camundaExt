Ext.define("App.view.ux.Datas", {
    statics: {
        Grid: [
            {name: "name", type: "string", text: "字段名"}
            , {name: "type", type: "string", text: "类型"}
            , {name: "text", type: "string", text: "中文名"}
            , {name: "component", type: "string", text: "组件"}
            , {name: "flex", type: "string", text: "占比Flex"}
            , {name: "length", type: "string", text: "最大长度"}
            , {name: "group", type: "string", text: "分组标记"}
            , {name: "hidden", type: "string", text: "是否隐藏"}
            , {name: "code", type: "string", text: "增加代码"}
            , {name: "width", type: "string", text: "宽度"}
            , {name: "tree", type: "string", text: "名值指定"}
            ,{ name:'notEmpty', type:'string', text:'非空'}
            ,{ name:'pk', type:'string', text:'主键'}
        ],
        ToolbarGrid:[
            {name: "id", type: "string", text: "操作代码"}
            , {name: "tooltip", type: "string", text: "提示"}
            , {name: "text", type: "string", text: "按钮名称"}
            , {name: "actionObject", type: "string", text: "操作对象"}
            , {name: "actionUrl", type: "string", text: "操作URL"}
            , {name: "iconCls", type: "string", text: "iconCls"}
            , {name: "code", type: "string", text: "扩展代码"}
        ],
        WindowGrid:[
            {name: "component", type: "string", text: "组件名"}
            , {name: "name", type: "string", text: "name"}
            , {name: "isParam", type: "string", text: "参数对象"}
            , {name: "autoSetSelect", type: "string", text: "自动设值"}
            , {name: "width", type: "int", text: "宽"}
            , {name: "height", type: "int", text: "高"}
            , {name: "code", type: "string", text: "扩展代码"}
        ]
    }
});
