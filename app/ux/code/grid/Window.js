Ext.define("App.ux.code.grid.Window", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.windowAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.Window",');
        codes.push(tab1 + 'alias: "widget.' + this.codeGen.windowXtype + '",');

        codes.push(tab1 + 'width: 800, // 窗口宽度');
        codes.push(tab1 + "bodyPadding: '15px',");
        codes.push(tab1 + 'items: [// 当items中对象配置的isParam为true时，当前window的提交操作会自动调用该对象的获取参数方法和isValid方法验证数据合法性');
        codes.push(tab2 + "//如果当前窗口对象的isGetChangeValue属性值为false（默认），items中form表单，调用getValues方法获取参数。如果是grid编辑列表，调用getEncodeValues方法获取参数");
        codes.push(tab2 + "//如果当前窗口对象的isGetChangeValue属性值为true，items中的form表单，调用getChangeValues方法获取参数。grid列表，getEncodeChangeValues方法获取参数");
        codes.push(tab2 + "//当items中对象配置autoSetSelect: true,如果窗口对象的_record属性存在，将会自动将_record中的数据赋值给items的对象");
        // codes.push(tab2 + "{name: '" + this.codeGen.formXtype + "', xtype: '" + this.codeGen.formXtype + "', isParam: true, autoSetSelect: true}");
        this.createWindowComp(codes);
        codes.push(tab1 + ']');
        codes.push('});');

        return codes;
    },
    /**
     * 添加其他页面组件
     * @param codes
     */
    createWindowComp: function (codes) {
        var tab2 = this.tab2;
        var fields;
        if (this.value.windowConfig && this.value.windowConfig.length > 0) {
            fields = this.value.windowConfig;
            for (var i = 0; i < fields.length; i++) {
                if (i === 0) {
                    codes.push(tab2 + "{" + this.getPropString(fields[i]) + "}");
                }else {
                    codes.push(tab2 + ",{" + this.getPropString(fields[i]) + "}");
                }
            }
        }

    },
    getPropString: function (data) {
        var propString = 'name:"' + data.name + '",';
        if (data.component) {
            propString += 'xtype:"' + data.component + '",';
        }
        if (data.isParam == 1) {
            propString += 'isParam:true,';
        }
        if (data.autoSetSelect == 1) {
            propString += 'autoSetSelect:true,';
        }
        propString = this.commonString(data, propString);
        return propString;
    }/*,
    createAttribute: function (codes) {
        var tab1 = this.tab1;
        var attribute, code = '';
        if (this.value.windowConfig && this.value.windowConfig.length > 0) {
            attribute = this.value.windowConfig[0];
            delete attribute.id;
            for(var key in attribute){
                code = '';
                if(!Ext.isEmpty(attribute[key])){
                    if(key === 'maximized' ){
                        if(attribute[key] == 1){
                            code += key + ": true,";
                        }
                    }else if(Number(attribute[key]) == attribute[key]){//数字类型
                        code += key + ":" + attribute[key] + ",";
                    }else{//其他类型
                        code += key + ":'" + attribute[key] + "',";
                    }

                    codes.push(tab1 + code );
                }
            }
        }
    }*/
});
