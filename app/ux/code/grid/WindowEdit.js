Ext.define("App.ux.code.grid.WindowEdit", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1;
        var codes = [], name = this.codeGen.windowEditAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "' + this.codeGen.windowAllName + '",');
        codes.push(tab1 + 'alias: "widget.' + this.codeGen.windowEditXtype + '",');
        codes.push(tab1 + 'openValidation:true,');
        codes.push(tab1 + 'isGetChangeValue:true,');
        codes.push(tab1 + "buttons: [{name: 'submit', actionUrl: '" + this.value.editUrl + "'}, 'close']");
        codes.push('});');

        return codes;
    }
});
