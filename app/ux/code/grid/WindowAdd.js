Ext.define("App.ux.code.grid.WindowAdd", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;

        var tab1 = this.tab1;
        var codes = [], name = this.codeGen.windowAddAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "' + this.codeGen.windowAllName + '",');
        codes.push(tab1 + 'alias: "widget.' + this.codeGen.windowAddXtype + '",');

        codes.push(tab1 + "buttons: [{name: 'submit', actionUrl: '" + this.value.addUrl + "'}, 'close']");
        codes.push('});');

        return codes;
    }
});
