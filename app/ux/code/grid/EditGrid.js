Ext.define("App.ux.code.grid.EditGrid", {
    extend: 'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.editGridAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.EditGrid",');
        codes.push(tab1 + "xtype: '" + this.codeGen.editGridXtype + "',");
        codes.push(tab1 + '_mainConfs: ' + this.codeGen.dataName + ',');
        codes.push(tab1 + "url:'" + this.codeGen.value.searchUrl + "',//grid的查询地址");
        codes.push(tab1 + 'selType: "checkboxmodel",');
        codes.push(tab1 + 'editColIndex: 2,');
        codes.push(tab1 + 'autoLoad: true,');
        codes.push(tab1 + 'tools: ["multiEdit","plus","minus"],');
        this.createParamCode(codes);
        codes.push(tab1 + '_confs: {// 配置grid的columns,支持原生columns的所有写法');
        codes.push(tab2 + 'columns: [');
        this.createGridColumns(codes);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');

        codes.push('});');

        return codes;
    },
    //生成提交参数代码
    createParamCode: function (codes) {
        var tab1 = this.tab1, tab2 = this.tab2;
        //表的实体名
        var className = this.value.mainUrl;
        if(!this.value.mainUrl){
            return;
        }
        //将首字大写
        className = className.replace(className[0], className[0].toUpperCase());
        //生成提交参数代码
        codes.push(tab1 + 'paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交');
        codes.push(tab2 + 'add: "addJson' + className + '",');
        codes.push(tab2 + 'update: "updateJson' + className + '",');
        codes.push(tab2 + 'del: "delJson' + className + '"');
        codes.push(tab1 + '},');
    },
    getGridPropString: function (data) {
        var propString = 'name:"' + data.name + '",';

        propString = this.commonString(data, propString);

        return propString;
    }
});
