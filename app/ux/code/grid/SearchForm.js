Ext.define("App.ux.code.grid.SearchForm", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var tab3 = this.tab3, tab4 = this.tab4;
        var codes = [], name = this.codeGen.searchFormAllName;
        var fields = [];
        if (this.value.searchFromConfig && this.value.searchFromConfig.length > 0) {
            fields = this.value.searchFromConfig;
        }
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.SearchForm",');
        codes.push(tab1 + "xtype: '" + this.codeGen.searchFormXtype + "',");
        codes.push(tab1 + '_mainConfs: ' + this.codeGen.dataName + ',');
        codes.push(tab1 + '_confs: {');
        codes.push(tab2 + 'components: [//表单字段在这里配置');
        this.addCompToSearchForm(fields, codes, tab3, tab4);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');
        codes.push('});');

        return codes;
    },
    addCompToSearchForm: function (fields, codes, tab3, tab4) {
        var group = '',
            groupNum = 1//计数，统计有几个分组
        ;
        for (var i = 0; i < fields.length; i++) {
            if (i === 0) {
                codes.push(tab3 + '[');
                group = fields[i].group;
                codes.push(tab4 + "{" + this.getPropString(fields[i]) + "}");
            } else if (group !== fields[i].group || !fields[i].group) {
                groupNum ++;
                if(groupNum === 2){
                    codes.push(tab4 + ',"sr","ec"');
                }
                codes.push(tab3 + '],');
                codes.push(tab3 + '[');
                group = fields[i].group;
                codes.push(tab4 + "{" + this.getPropString(fields[i]) + "}");
            } else {
                codes.push(tab4 + ",{" + this.getPropString(fields[i]) + "}");
            }
            if (i == fields.length - 1) {
                if(groupNum === 1){
                    codes.push(tab4 + ',"sr"');
                }
                codes.push(tab3 + ']');
            }
        }
    }
});
