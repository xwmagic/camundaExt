Ext.define("App.ux.code.grid.ViewModel", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.viewModelAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.BaseViewModel",');
        codes.push(tab1 + "alias: 'viewmodel." + this.codeGen.viewModelXtype + "',");
        codes.push(tab1 + "data: {");
        codes.push(tab2 + "pageName: '" + this.codeGen.xtypeName + "',");
        codes.push(tab2 + "add: true,");
        codes.push(tab2 + "update: true,");
        codes.push(tab2 + "del: true");
        codes.push(tab1 + "}");

        codes.push('});');

        return codes;
    }
});
