/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.Grid', {
    extend: 'App.view.common.EditGrid',
    xtype: 'uxcodeGrid',
    _mainConfs: App.view.ux.Datas.Grid,
    cName:'列表组件配置列表',
    selType: "checkboxmodel",
    border: true,
    viewConfig: {
        plugins: {
            ptype: 'gridviewdragdrop',
            containerScroll: true
        }
    },
    columnsDefault:{isMultiEdit:true},
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'name', width: 150,align:'left'},
            {name: 'text', width: 150,align:'left'},
            {name: 'width', width: 150},
            {name: 'flex', width: 150},
            {name: 'hidden', width: 150},
            {name: 'tree',width:100,editor:{xtype:'uxcodeTreeKeyCombo'}},
            {name: 'code', flex: 1,align:'left'}
        ]
    },
    historyRemove: [
        { name:'id', type:'string', text:'主键',length:36, notEmpty:true,pk:true}
        ,{ name:'createName', type:'string', text:'创建人'}
        ,{ name:'createId', type:'string', text:'创建人id'}
        ,{ name:'createDate', type:'date', text:'创建时间'}
        ,{ name:'lastUpdateDate', type:'date', text:'修改时间'}
        ,{ name:'lastUpdateId', type:'string', text:'修改人id'}
        ,{ name:'isEnable', type:'string', text:'是否可用'}
        ,{ name:'companyId', type:'string', text:'所属公司'}
        ,{ name:'companyName', type:'string', text:'所属公司'}
        ,{ name:'companyCode', type:'string', text:'所属公司代码'}
        ,{ name:'orgId', type:'string', text:'所属组织'}
        ,{ name:'orgName', type:'string', text:'所属组织'}
        ,{ name:'orgCode', type:'string', text:'所属组织代码'}
        ,{ name:'appName', type:'string', text:'APP名称'}
        ,{ name:'appCode', type:'string', text:'APP编码'}
        ,{ name:'lastUpdateName', type:'string', text:'修改人'}
        ,{ name:'version', type:'int', text:'版本'}
        ,{ name:'isDelete', type:'string', text:'是否删除'}
    ],//存放被删除的数据
    tools: [
        {
            xtype:'button',
            text:'检查',
            name:'check',
            margin:'0 5',
            tooltip:'检查字段是否在【Datas配置】列表中',
            handler:function (btn) {
                btn.up('grid').checkDatasRight();
            }
        },
        {
            xtype:'button',
            text:'更新',
            name:'update',
            margin:'0 5',
            tooltip:'根据上方字段字符串进行更新',
            handler:function (btn) {
                btn.up('grid').updateByJson();
            }
        },
        {
            xtype: 'button', text: '历史字段',
            handler: function (btn) {
                var me = btn.up('grid');
                var win = Ext.create('Ext.window.Window',{
                    layout: 'fit',
                    width: 1000,
                    height: 600,
                    items: [{xtype: me.xtype}],
                    buttons: [{
                        text: '确定', handler: function () {
                            var winGrid = this.up('window').down('grid');
                            var selections = winGrid.getSelectionModel().getSelection();
                            var datas = [];
                            for (var i = 0; i < selections.length; i++) {
                                datas.push(Ext.clone(selections[i].data));
                            }
                            winGrid.getStore().remove(selections);
                            me.historyRemove = winGrid.getDatas();
                            me.getStore().loadData(datas, true);
                            win.close();
                        }
                    }, {
                        text: '取消',
                        handler: function () {
                            win.close();
                        }
                    }]
                });
                win.down('grid').getStore().loadData(me.historyRemove);
                win.show();
            }
        },
        'multiEdit','plus', 'minus'
    ],
    _removeRecords: function () {
        var selections;
        selections = this.getSelectionModel().getSelection();
        for (var i = 0; i < selections.length; i++) {
            this.historyRemove.push(Ext.clone(selections[i].data));
        }
        this.callParent();
    },
    updateByJson:function () {
        var newBaseJson = this.NewBaseJsonConfig;
        var temp,notFind = [];
        var store = this.getStore();
        for (var i = 0; i < newBaseJson.length; i++) {
            temp = store.findRecord('name', newBaseJson[i].name);
            if (temp) {
                temp.set('text', newBaseJson[i].text);
                temp.set('type', newBaseJson[i].type);
            }else{
                notFind.push(Ext.clone(newBaseJson[i]));
            }
        }
        store.loadData(notFind,true);
    },
    /**
     * 检查数据字段是否存在与Datas列表对象中,将不存在的字段找出来
     */
    checkDatasRight:function () {
        var win = this.up('window');
        var thisStore = this.getStore();
        if(win && win.down('uxcodeDatasGrid')){
            var temp,rec;
            var checkField = [];
            var checkFieldObj = [];
            var DatasGridStore = win.down('uxcodeDatasGrid').getStore();
            thisStore.each(function (record) {
                temp = DatasGridStore.findRecord('name', record.data.name);
                if(!temp || temp.data.name !== record.data.name){
                    checkField.push(record.data.name);
                    rec = Ext.clone(record.data);
                    if(Ext.isEmpty(rec.type)){
                        rec.type = 'string';
                    }
                    if(Ext.isEmpty(rec.text)){
                        rec.text = rec.name;
                    }
                    checkFieldObj.push(rec);
                }
            });
            if(checkField.length > 0){
                Ext.MessageBox.confirm('是否将其添加到【Datas配置】表中?',this.cName+'存在不匹配字段:'+checkField.toString() ,function(btn){
                    if (btn == 'yes'){
                        DatasGridStore.loadData(checkFieldObj,true);
                    }
                });
            }
        }
    },
    /**
     * 生成组件的名称
     * allNames
     */
    getAllComponetNames: function (values) {
        if(!values){
            return;
        }
        var allNames = {
            value: '',//填写参数对象
            fields: [],
            packAllName: '',
            dataName: '',
            name: '',
            className: '',
            xtypeName: '',

            formAllName: '',
            formName: 'Form',
            formXtype: '',

            gridAllName: '',
            gridName: 'Grid',
            gridXtype: '',
            editGridAllName: '',
            editGridName: 'EditGrid',
            editGridXtype: '',
            GridMiniName: 'GridMini',
            pickerName: 'Picker',
            GridSelectName: 'GridSelect',
            SelectWindowName: 'SelectWindow',
            ComboName:'Combo',

            searchFormAllName: '',
            searchFormName: 'SearchForm',
            searchFormXtype: '',

            toolbarAllName: '',
            toolbarName: 'Toolbar',
            toolbarXtype: '',

            viewModelAllName: '',
            viewModelName: 'ViewModel',
            viewModelXtype: '',

            windowAllName: '',
            windowName: 'Window',
            windowXtype: '',

            windowAddAllName: '',
            windowAddName: 'WindowAdd',
            windowAddXtype: '',

            windowEditAllName: '',
            windowEditName: 'WindowEdit',
            windowEditXtype: '',

            windowViewAllName: '',
            windowViewName: 'WindowView',
            windowViewXtype: '',

            datasName: '',
            xtypePreName: ''
        };
        var name = values.classExtName;
        var cNames = name.split('.');
        if(!cNames[cNames.length -1]){
            return '';
        }
        var packName = '', packAllName = '';
        for (var i = 0; i < cNames.length; i++) {
            if (i > 1 && i < cNames.length - 1) {
                packName = packName + cNames[i];
            }
            if (i == 0) {
                packAllName = cNames[i];
            } else if (i < cNames.length - 1) {
                packAllName = packAllName + '.' + cNames[i];
            }
        }

        allNames.packAllName = packAllName;
        allNames.xtypePreName = packName;
        var cName = cNames[cNames.length - 1];
        allNames.className = cName;
        allNames.dataName = packAllName + '.Datas.' + allNames.className;
        allNames.xtypeName = packName + cName;

        //生成form的类全名、类名、xtype名
        //this.formName = 'Form';
        allNames.formAllName = packAllName + '.' + allNames.formName;
        allNames.formXtype = packName + allNames.formName;

        //生成grid的类全名、类名、xtype名
        //this.gridName = 'Grid';
        allNames.gridAllName = packAllName + '.' + allNames.gridName;
        allNames.gridXtype = packName + allNames.gridName;
        allNames.editGridAllName = packAllName + '.' + allNames.editGridName;
        allNames.editGridXtype = packName + allNames.editGridName;

        //生成searchForm的类全名、类名、xtype名
        //this.searchFormName = 'SearchForm';
        allNames.searchFormAllName = packAllName + '.' + allNames.searchFormName;
        allNames.searchFormXtype = packName + allNames.searchFormName;

        //生成toolbar的类全名、类名、xtype名
        //this.toolbarName = 'Toolbar';
        allNames.toolbarAllName = packAllName + '.' + allNames.toolbarName;
        allNames.toolbarXtype = packName + allNames.toolbarName;

        //生成toolbar的类全名、类名、xtype名
        //this.viewModelName = 'ViewModel';
        allNames.viewModelAllName = packAllName + '.' + allNames.viewModelName;
        allNames.viewModelXtype = packName + allNames.viewModelName;

        //生成window的类全名、类名、xtype名
        //this.windowName = 'Window';
        allNames.windowAllName = packAllName + '.' + allNames.windowName;
        allNames.windowXtype = packName + allNames.windowName;

        //生成windowAdd的类全名、类名、xtype名
        //this.windowAddName = 'WindowAdd';
        allNames.windowAddAllName = packAllName + '.' + allNames.windowAddName;
        allNames.windowAddXtype = packName + allNames.windowAddName;

        //生成windowAdd的类全名、类名、xtype名
        //this.windowEditName = 'WindowEdit';
        allNames.windowEditAllName = packAllName + '.' + allNames.windowEditName;
        allNames.windowEditXtype = packName + allNames.windowEditName;

        allNames.windowViewAllName = packAllName + '.' + allNames.windowViewName;
        allNames.windowViewXtype = packName + allNames.windowViewName;

        //生成Datas的类全名
        allNames.datasName = packAllName + '.Datas';

        return allNames;
    }
});