Ext.define("App.ux.code.tree.TreeEditGrid", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.gridAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.EditTreeGridDrag",');
        codes.push(tab1 + "xtype: '" + this.codeGen.gridXtype + "',");
        codes.push(tab1 + '_mainConfs: ' + this.codeGen.dataName + ',');
        codes.push(tab1 + "url:'" + this.codeGen.value.searchUrl + "',//tree的查询地址");
        codes.push(tab1 + '_confs: {// 配置grid的columns,支持原生columns的所有写法');
        codes.push(tab2 + 'columns: [');
        this.createGridColumns(codes);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');

        codes.push('});');
        return codes;
    },
    createGridColumns: function (codes) {
        var tab3 = this.tab3;
        var cols = this.codeGen.fields;
        if (this.value.mainEditTreeGridConfig && this.value.mainEditTreeGridConfig.length > 0) {
            cols = this.value.mainEditTreeGridConfig;
        }
        for (var i = 0; i < cols.length; i++) {
            if (i === cols.length - 1) {
                codes.push(tab3 + "{" + this.getTreeGridPropString(cols[i]) + "}");
                continue;
            }
            codes.push(tab3 + "{" + this.getTreeGridPropString(cols[i]) + "},");
        }
    }
});
