Ext.define("App.ux.code.tree.Picker", {
    extend:'App.ux.code.tree.TreeGridMini',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.packAllName + '.' + this.codeGen.pickerName;
        var keyData = this.getTreeKeyData();
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.TreeMuiltiPicker",');
        codes.push(tab1 + "xtype: '" + this.codeGen.xtypePreName + this.codeGen.pickerName + "',");
        codes.push(tab1 + "gridClassName:'"+this.codeGen.packAllName + '.' + this.codeGen.treeGridMiniName+"',");
        codes.push(tab1 + "checkModel:'single',//选择模式");
        codes.push(tab1 + "onlyLeaf:false,");
        codes.push(tab1 + "displayField: '"+keyData.name+"',");
        codes.push(tab1 + "valueField: 'id'");
        codes.push('});');
        return codes;
    }
});
