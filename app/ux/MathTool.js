Ext.define('App.ux.MathTool', {
    initMaths: function () {
        /**
         * 加法运算 arg1 + arg2
         * @param arg1
         * @param arg2
         * @returns {number}
         * @constructor
         */
        function NumberAdd(arg1, arg2) {
            var r1, r2, m, n;
            try {
                r1 = arg1.toString().split(".")[1].length
            } catch (e) {
                r1 = 0
            }
            try {
                r2 = arg2.toString().split(".")[1].length
            } catch (e) {
                r2 = 0
            }
            m = Math.pow(10, Math.max(r1, r2));
            n = (r1 >= r2) ? r1 : r2;
            return Number(((arg1 * m + arg2 * m) / m).toFixed(n));
        }

        /**
         * 减法运算 arg1 - arg2
         * @param arg1
         * @param arg2
         * @returns {number}
         * @constructor
         */
        function NumberSub(arg1, arg2) {
            var re1, re2, m, n;
            try {
                re1 = arg1.toString().split(".")[1].length;
            } catch (e) {
                re1 = 0;
            }
            try {
                re2 = arg2.toString().split(".")[1].length;
            } catch (e) {
                re2 = 0;
            }
            m = Math.pow(10, Math.max(re1, re2));
            n = (re1 >= re2) ? re1 : re2;
            return Number(((arg1 * m - arg2 * m) / m).toFixed(n));
        }

        /**
         * 乘法运算 arg1*arg2
         * @param arg1
         * @param arg2
         * @returns {number}
         * @constructor
         */
        function NumberMul(arg1, arg2) {
            var m = 0;
            var s1 = arg1.toString();
            var s2 = arg2.toString();
            try {
                m += s1.split(".")[1].length;
            } catch (e) {
            }
            try {
                m += s2.split(".")[1].length;
            } catch (e) {
            }

            return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
        }

        /**
         * 除法运算 arg1/arg2
         * @param arg1
         * @param arg2
         * @param digit 保留的小数点后的位数
         * @returns {number}
         * @constructor
         */
        function NumberDiv(arg1, arg2, digit) {
            var t1 = 0, t2 = 0, r1, r2;
            try {
                t1 = arg1.toString().split(".")[1].length
            } catch (e) {
            }
            try {
                t2 = arg2.toString().split(".")[1].length
            } catch (e) {
            }
            r1 = Number(arg1.toString().replace(".", ""));
            r2 = Number(arg2.toString().replace(".", ""));
            //获取小数点后的计算值
            var result = ((r1 / r2) * Math.pow(10, t2 - t1)).toString();
            var result2 = result.split(".")[1];
            result2 = result2.substring(0, digit > result2.length ? result2.length : digit);

            return Number(result.split(".")[0] + "." + result2);
        }

        /**
         * 多个值求和 1+2+3
         * @param arr 数字数组[1,2,3]
         * @returns {*}
         * @constructor
         */
        function NumberAdds(arr) {
            if (!arr || arr.length < 1) {
                return '';
            }
            if (arr.length === 1) {
                return arr[0];
            }
            var sum = arr[0];
            for (var i = 1; i < arr.length; i++) {
                sum = NumberAdd(sum, arr[i]);
            }
            return sum;
        }

        /**
         * 多个值相减 1-2-3
         * @param arr 数字数组[1,2,3]
         * @returns {*}
         * @constructor
         */
        function NumberSubs(arr) {
            if (!arr || arr.length < 1) {
                return '';
            }
            if (arr.length === 1) {
                return arr[0];
            }
            var sum = arr[0];
            for (var i = 1; i < arr.length; i++) {
                sum = NumberSub(sum, arr[i]);
            }
            return sum;
        }

        /**
         * 多个值相乘 1*2*3
         * @param arr 数字数组 [1,2,3]
         * @returns {*}
         * @constructor
         */
        function NumberMuls(arr) {
            if (!arr || arr.length < 1) {
                return '';
            }
            if (arr.length === 1) {
                return arr[0];
            }
            var sum = arr[0];
            for (var i = 1; i < arr.length; i++) {
                sum = NumberMul(sum, arr[i]);
            }
            return sum;
        }

        /**
         * 多个值相除 1/2/3
         * @param arr 数字数组[1,2,3]
         * @returns {*}
         * @constructor
         */
        function NumberDivs(arr) {
            if (!arr || arr.length < 1) {
                return '';
            }
            if (arr.length === 1) {
                return arr[0];
            }
            var sum = arr[0];
            for (var i = 1; i < arr.length; i++) {
                sum = NumberDiv(sum, arr[i]);
            }
            return sum;
        }

        function toFixed(val, precision) {
            if (val == Number(val)) {
                return val.toFixed(precision);
            }
            return val;
        }

        Math.add = NumberAdd;
        Math.adds = NumberAdds;
        Math.sub = NumberSub;
        Math.subs = NumberSubs;
        Math.mul = NumberMul;
        Math.muls = NumberMuls;
        Math.div = NumberDiv;
        Math.divs = NumberDivs;
        Math.toFixed = toFixed;
    }

});

