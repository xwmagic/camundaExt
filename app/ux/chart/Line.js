Ext.define("App.view.ux.chart.Line", {
    extend: "App.ux.BaseECharts",
    xtype: 'uxchartLine',
    optionName:'App.view.ux.chart.base.Option',
    seriesName:'App.view.ux.chart.series.Line'
});
