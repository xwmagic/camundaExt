Ext.define("App.view.ux.chart.series.Pie", {
    extend:'App.view.ux.chart.series.Base',
    baseConfig:{
        name:'',
        type:'pie',
        radius : '70%',
        center: ['50%', '50%'],
        data:[

        ].sort(function (a, b) { return a.value - b.value; }),
        roseType: 'radius',
        label: {
            normal: {
                formatter: '{c}'
            }
        },
        itemStyle: {
            normal: {
                shadowBlur: 200,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        },
        animationType: 'scale',
        animationEasing: 'elasticOut',
        animationDelay: function (idx) {
            return Math.random() * 200;
        }
    }
});
