/**
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.Container',{
    extend: 'Ext.container.Container',
	xtype: 'approvalContainer',
	layout: {
        type: 'hbox',
        align: 'stretch'
    },
    border: 1,
	height: 26,//默认33，移植到其他系统需要修改
	padding: '0 0 0 10',
    style: {borderColor:'#999', borderStyle:'solid', borderWidth:'1px',background:'#eee'},
	isFirst: false,
	right: false,	//右边面板支持字符串、对象
	left: false,	//左边面板支持字符串、对象
	openDisable: false,//组件将具备enbale和disable容器中的field对象
	initComponent: function(){
		this.items = this.createItems();
		if(!this.isFirst){
			this.border = '0 1 1 1';
		}
		this.callParent();
	},
	
	listeners: {
		hide: function(){//组件是隐藏状态是内置组件不可用
			this.disable();
		},
		show: function(){
			this.enable();
		},
		beforerender: function(){
			var me = this;
			setTimeout(function(){
				if(me.hidden){
					me.disable();
				}else{
					me.enable();
				}
			},500);
			
		}
	},
	
	disable: function(){
		if(this.openDisable){
			var fields = this.query('field');
			for(var i =0 ; i < fields.length; i++ ){
				fields[i].isFormField = false;
				fields[i].disable();
			}
		}
	},
	enable: function(){
		if(this.openDisable){
			var fields = this.query('field');
			for(var i =0 ; i < fields.length; i++ ){
				fields[i].isFormField = true;
				fields[i].enable();
			}
		}
	},
	createItems:function(){
		if(this.items){
			return;
		}
		
		var left =  { xtype: 'displayfield',width: 200,readOnly: false},
		centerLine = { xtype: 'container',width: 1,style: {background:'#999'} },
		right = { xtype: 'container', flex: 1, style: {background:'#fff'} };
		
		if(Ext.isString(this.left)){
			left.value = '<b>' +this.left + '</b>';
		}else if(Ext.isObject(this.left)){
			left = Ext.applyIf(Ext.clone(this.left), left);
		}else{
			left = false;
		}
		
		if(Ext.isString(this.right)){
			right.html = this.right;
			right.style.fontWeight = 'bold';
			right.padding = '5 0 0 5';
		}else if(Ext.isObject(this.right)){
			right = Ext.applyIf(Ext.clone(this.right), right);
		}else{
			right = false;
		}
		
		if(right && left){
			return [left,centerLine,right];
		}
		
		if(left && !right){
			left = Ext.applyIf(left,{flex: 1});
			return [left];
		}
		
		if(!left && right){
			return [right];
		}
		
	}
	
});