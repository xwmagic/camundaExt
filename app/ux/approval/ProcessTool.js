Ext.define('App.ux.approval.ProcessTool', {
    extend: 'Ext.Component',
    xtype: 'approvalProcessTool',

    //获取业务端数据的action地址，默认采用GET请求。
    // actionUrl: 'sqm/inspectionsheet/pagingQuery',
    defaultMethod: 'GET',
    // title: 'IQC检验管理详细信息',
    // useXtype: 'inspectionsheetDetailSPWindow',
    /* btn: {
     text: 'IQC检验管理详细信息',
     name: "s",
     actionObject: {
     xtype: 'inspectionsheetDetailSPWindow',
     baseParams:{
     transId: param.transId
     }
     }
     },*/
    interfaceXtype: 'commontoolbar',//接口对象
    interfaceName: '_getViewWindow',//接口对象的方法
    exParam: {
        page: 1,
        start: 0,
        limit: 1
    },
    paramKeyToUrl: [
        {name: 'processInstanceId', inUrlName: 'processInstanceId'},
        {name: 'targetUserId', inUrlName: 'targetUserId'}
    ],

    showApprovalProcess: function (param) {
        var me = this,
            btn = Ext.clone(this.btn),
            i,
            ajaxParam = {};

        if (!btn) {
            btn = {
                text: this.title,
                name: "s",
                actionObject: {
                    xtype: this.useXtype,
                    baseParams: {
                        transId: param.transId
                    }
                }
            };
        } else {
            if (Ext.isObject(btn.actionObject)) {//如果是对象
                btn.actionObject.baseParams = {
                    transId: param.transId
                }
            } else {//如果是字符串
                btn.actionObject = {
                    xtype: btn.actionObject,
                    baseParams: {
                        transId: param.transId
                    }
                }
            }
        }

        for (i = 0; i < this.paramKeyToUrl.length; i++) {
            ajaxParam[this.paramKeyToUrl[i].name] = param[this.paramKeyToUrl[i].inUrlName];
        }
        ajaxParam = Ext.apply(ajaxParam, this.exParam);

        Ext.Ajax.request({
            url: me.actionUrl,
            params: ajaxParam,
            method: this.defaultMethod,
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                if (Ext.isEmpty(obj.data)) {
                    me.showToast('提示', '无法找到审批数据！请刷新页面在重试或联系管理员');
                    return;
                }
                me.openProcess(obj.data, btn, param);
            },

            failure: function (response, opts) {
                me.showToast('失败', '获取' + me.title + '失败');
            }
        });
    },
    openProcess: function (data, btn, param) {
        var me = this;
        if (Ext.isArray(data)) {
            data = data[0];
        }
        var valid = me.isValid(data);
        if (!valid.valid) {
            me.showToast('提示', valid.message);
            return;
        }
        var commontoolbar = Ext.create({
            xtype: me.interfaceXtype,
            _confs: {
                components: [
                    btn
                ]
            }
        });
        // if(me.notCurrentCompanyCode(obj.data[0])){
        // 	var message = '申请单公司是【'+obj.data[0].companyName+'】,请切换公司帐套到该公司才能审批';
        //    App.ux.Toast.show('提示',message);
        // 	return;
        // }
        var win = commontoolbar[me.interfaceName](commontoolbar.down('button'), data);
        if (win.down('approvalMain')) {
            win.down('approvalMain').processXtype = param.route;
        }
        win._show();
    },
    showToast: function (title, html) {
        Ext.toast({
            title: title,
            html: html,
            align: 't',
            closable: true,
            slideInDuration: 150,
            minWidth: 300,
            minHeight: 30
        });
    },
    isValid: function (data) {
        return {
            valid: true,
            message: ''
        };
    },

    //如果不是当前公司，返回true, 否则返回false
    notCurrentCompanyCode: function (record) {
        if (!record) {
            return true;
        }
        if (record.companyCode != Ext.CSOTUserInfo.curAccount) {
            return true;
        }

        return false;
    }

});