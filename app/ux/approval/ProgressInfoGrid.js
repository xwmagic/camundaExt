/**审批节点流向记录列表
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.ProgressInfoGrid',{
    extend: 'App.ux.approval.BaseGrid',
	requires: [
		'App.ux.approval.store.ProgressInfoGridStore',
		'App.ux.approval.BaseGrid'
	],
	xtype: 'approvalProgressInfoGrid',
	
	_createStore: function(){
		var me = this, 
			className,
			params = '',
			url = this.mainRunTimeUrl + 'getActivityRecordPage';
		
		if(this.defaultParams){
			params = Ext.clone(this.defaultParams);
		}
		
		var store = Ext.create('App.ux.approval.store.ProgressInfoGridStore',{
			autoLoad : true,
			url: url,
			defaultParam: params
		});
		if(this.defaultDatas){
			store.loadData(Ext.clone(this.defaultDatas));
		}
		
		return store;
	},
	
	_createColumns: function(){
		var columns = [
			 {xtype: 'rownumberer'}
			,{text: '时间', dataIndex: 'createTime',flex: 1}
			,{text: '节点名称', dataIndex: 'activityName',flex: 1}
			,{text: '操作者', dataIndex: 'operatorName',flex: 1}
			,{text: '操作', dataIndex: 'log',flex: 1}
		];
		return columns;
	}
	
});