/**
 * @author xiaowei
 * @date 2017��6��2��
 */
Ext.define('App.ux.approval.BaseForm',{
    extend: 'Ext.form.Panel',
	xtype: 'approvalBaseForm',
	
	initComponent : function() {
		this.on({
			boxready: function(){
				this.setOrginValues();
			}
		});
		this.callParent(arguments);
	},
	
	setOrginValues: function(val){
		if(val){
			this.orginValues = Ext.clone(val);
		}else{
			this.orginValues = Ext.clone(this.getValues());
		}
	},
	
	getChangeValues: function(requires){
		var change = {}, orgin = this.orginValues, value = this.getValues();
		
		if(!this.isChange()){
			return;
		}
		
		Ext.Object.each(value, function(key, value, myself) {
			if (orgin[key] != value) {
				change[key] = value;
			}
		});
		
		if(Ext.isArray(requires)){
			for(var i = 0; i<requires.length; i++){
				change[requires[i]] = value[requires[i]];
			}
		}
		
		return change;
	},
	
	
	isChange: function(){
		return !Ext.Object.equals(this.orginValues,this.getValues());
	},
	
	reset: function(btn){
		var form = btn.up('form');
		form.getForm().reset();
	},
	
	setValues: function(data){
		this.getForm().setValues(data);
	},
	
	_submit: function(btn){
		var me = this;
		me.submit({
			//url:url,
			//method: 'POST',
			//params:params,
			success: function(form, action) {
				me._submitSuccess(btn,Ext.decode(action.response.responseText));
			},
			failure: function(form, action) {
				me._submitFailure(btn,Ext.decode(action.response.responseText));
				
			}
		});
	},
	
	_submitSuccess: function(btn,response,options){
		if(btn){
			btn.enable();
		}
		if(this._targetObject){
			this._targetObject._refresh();
		}
	},
	
	_submitFailure: function(btn,response,options){
		if(btn){
			btn.enable();
		}
	},
	
	getShowCompParams: function(){
		var fields = this.queryBy(function(me){
			if(!me.hidden && me.isFormField && me.name){
				return true;
			}
			return false;
		});
	}
});