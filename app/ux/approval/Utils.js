/**审批引擎提供的工具类
 * @author xiaowei
 * @date 2017年7月5日
 */
Ext.define('App.ux.approval.Utils',{
	openWindowByUrl: function(remoteUrl,fun){
		var param = this.parseParams(remoteUrl);
		if(!param){
			return;
		}
		if(!param.route){
		    return;
        }
		this.openWindowByParam(param,fun);
	},
	
	openWindowByParam: function(param,fun){
		if(!Ext.isEmpty(fun)){
			fun(param);
		}else{
			var targetObj = Ext.create({xtype: param.route});
			targetObj.showApprovalProcess(param);
		}
	},
	
	parseParams: function (remoteUrl) {
        //remoteUrl = 'http://10.108.139.7:8080/sqm-web/?processInstanceId_=595b8462-62b2-11e7-9803-dc4a3e8237a0&taskId_=d03afe7e-6129-11e7-b988-dc4a3e8237a0&transId_=d03afe7e-6129-11e7-b988-dc4a3e8237a0&route_=inspectionsheetProcess#all';
        var index = remoteUrl.indexOf('#');
        if(index > -1){
            remoteUrl = remoteUrl.substring(0,index);
        }

        var urls = remoteUrl.split('?'),
            urlParams, i, temp, param = {}, name;

        if(urls.length < 2){
            return;
        }
        var urlParams = urls[1];
        urlParams = urlParams.split('&');
        for(i = 0; i<urlParams.length; i++){
            temp = urlParams[i].split('=');
            name = temp[0];
            if(temp[0].lastIndexOf('_')){
                name =  temp[0].substring(0,temp[0].length-1)
            }
            if(temp.length == 2){
                param[name] = temp[1];
            }else{
                param[name] = '';
            }
        }
        return param;
    }
});