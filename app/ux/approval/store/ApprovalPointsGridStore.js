/**审批节点实例列表store
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.store.ApprovalPointsGridStore', {
	extend : 'App.ux.approval.store.BaseStore',
	alias : 'store.approvalPointsGridStore',
	url: 'getActivityInstanceList',
	fields:[ 'activityId', 'activityName', 'operatorName','transitionType','outgoing','activityStatus']
});
