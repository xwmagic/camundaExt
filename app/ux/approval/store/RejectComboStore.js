/**驳回节点下拉框store
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.store.RejectComboStore', {
	extend : 'App.ux.approval.store.BaseStore',
	alias : 'store.rejectComboStore',
	url   : 'getActivityListForReject',
	fields: ['activityId', 'activityName','isReturnMe','activityType']
});
