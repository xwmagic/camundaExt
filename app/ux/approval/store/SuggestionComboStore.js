/**选择意见下拉框Store
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.store.SuggestionComboStore', {
	extend : 'App.ux.approval.store.BaseStore',
	alias : 'store.suggestionComboStore',
	fields: ['name', 'val'],
	data : [
		{"val":"AL", "name":"同意"},
		{"val":"AK", "name":"退回"},
		{"val":"AZ", "name":"已阅"},
		{"val":"AK", "name":"很好"},
		{"val":"AK", "name":"驳回"},
		{"val":"AK", "name":"请尽快处理"},
		{"val":"AK", "name":"请处理完OA上的待审事项"}
	]
});
