Ext.define('App.ux.approval.FloatWindow', {
    extend: 'Ext.window.Window',
	xtype: 'approvalFloatWindow',
	minHeight: 43,
	height: 43,
	width: 234,
	draggable: false,
	closable: false,
	resizable: false,
	shadow : false,
	border: false,
	scrollMain: '',//包含滚动条的主体对象的
	sTargets: [],//保存滚动页签的位置值
	bodyStyle: {
		background: 'transparent'
	},
	style: {
		background: 'transparent',
		'border-width': '0px'
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	buttonItems: [
		
	],//菜单对象数组
	initComponent: function(){
		var childItems = this.createChildItems();
		this.items = [{
			xtype: 'tabpanel',
			layout: 'fit',
			name: 'tab',
			height: this.height,
			tabBar: {
				name: 'tab',
				style: {
					background: 'transparent'
				},
				height: this.height,
				defaults: {
					height: this.height,
					minWidth: 100
				}
			},
			listeners: {
				beforerender: function(){
					var tab = this.down('tabbar[name=tab]');
					var btns = tab.query('button');
					for(var i=0; i<btns.length; i++){
						btns[i].setStyle({
							fontSize: '20px',
							fontWeight: 'bold'
						});
					}
				}
			},
			items: childItems
		}];
		this.callParent();
	},
	createChildItems: function(){
		var i, items = Ext.clone(this.buttonItems), me = this, sTargets = [];
		if(!Ext.isEmpty(this.buttonItems)){
			var extendProperty = {
				isScrollerChange: false,
				listeners:{
					activate: function(){
						
						if(!this.isScrollerChange && me.scrollMain){
							var scroller = me.scrollMain.getScrollable();
							scroller.isOpen = false;
							scroller.scrollTo(null, this.scrollToY);
						}
						this.isScrollerChange = false;
						
					}
				}
			};
			for(i=0; i<items.length; i++){
				items[i] = Ext.apply(items[i],extendProperty);
				sTargets.push(items[i].scrollToY);
			}
		}
		this.sTargets = sTargets;
		return items;
	},
	_showAt: function(width, height){
		if(this.width + 300 >= width){
			this.showAt(10,height-120);
		}else{
			this.showAt(200,height-120);
		}
	},
	createMainScroller: function(){
		var me = this;
		return {
			y: true,
			isOpen: true,
			listeners: {
				scrollend: function(th, x, y){
					if(!th.isOpen){
						th.isOpen = true;
						return;
					}
					var tabpanel = me.down('tabpanel[name=tab]'), pan = '';
					for(var i=me.sTargets.length - 1 ; i >=0; i--){
						if(y >= me.sTargets[i]){
							pan = tabpanel.down('panel[scrollToY='+me.sTargets[i]+']');
							break;
						}
					}
					if(pan && pan.hidden || !pan.hasOwnProperty('hidden')){
						pan.isScrollerChange = true;
						tabpanel.setActiveTab(pan);
					}
					
				}
			}
		};
	}
	
});