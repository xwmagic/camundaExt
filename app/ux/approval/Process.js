/**审批流程页面对象
 * @author xiaowei
 * @date 2017年6月2日
 */

var approval_xw_Click = function(id){
	var comp = Ext.getCmp(id);
	Ext.create('Ext.window.Window',{title: '人员信息',width: 500,height: 500,modal: true}).show();
};

Ext.define('App.ux.approval.Process',{
	extend: 'App.ux.approval.BaseForm',
	alias: 'widget.approvalProcess',
	requires: [
		'App.ux.approval.Container',
		'App.ux.approval.store.BaseStore',
		'App.ux.approval.BaseForm',
		'App.ux.approval.ApprovalGrid',
		'App.ux.approval.ApprovalPointsGrid',
		'App.ux.approval.ProgressInfoGrid',
		'App.ux.approval.RejectCombo',
		'App.ux.approval.SuggestionCombo',
		'App.ux.approval.TransCombo',
		'App.ux.approval.models.InititorWindow',
		'App.ux.approval.MemberMulitSelectField',
		'App.ux.approval.NodePersonsWindow'
	],
    isReturnMeConfig:{
        checked:false,
		hidden: false,
		disabled:false
	},
	_isView: false,//是否是查看功能
    isServerStart: false,//是否是从服务端发起流程
    submitButton:{},//提交按钮
    startSubmitHidden: false,//起草节点是都隐藏提交按钮，默认不隐藏
	openValidation: false,
	//mainUrl: '/activiti/service/assignee',
	url: 'startProcess',
	/*baseParams: {
		userId: 123,
		companyId:220093
	},
	triggerObj: {},
	customParams: {
		globalValue: [{a: 1,b:2}],
		myCustom: '0092'
	},*/
	initComponent: function(){
		
		this.items = this._getItems();
		
		this.callParent(arguments);
	},
	
	_getItems: function(){
		var me = this, frontArea, centerArea, lastArea;
		
		frontArea = [
			 this._topButtons()//顶部按钮
			,this._processInfo()//流程说明
			,this._showApprRecords()//显示审批记录复选框
			,this._approvalGrid()//审批记录列表
		];
        centerArea = this._centerArea();//中间操作区域
		if(this.isServerStart && this._data){//如果是服务器发起流程

            if(!this._data.task){//如果tast不存在不渲染中间区域
                centerArea = [];
            } else if(this._data.task && this._data.task.activity && this._data.task.activity.activityId == "startTask"){
                //如果task存在并且节点id是起草节点不渲染中间区域
            	centerArea = [];
            }
		}

		if(Ext.isEmpty(centerArea)){
			frontArea[2].left.checked = true;
			frontArea[3].hidden = false;
		}
		
		lastArea = [
			 this._progressMapCheckbox()//流程图复选框
			,this._progressMap()//流程图
			,this._moreInfoCheckbox()//更多信息复选框
			,this._moreInfo()//更多信息
		];
		return frontArea.concat(centerArea).concat(lastArea);
		
	},
	
	_getCurrTrans: function(){
		if(!this._data || Ext.isEmpty(this._data.currTransList)){
			return;
		}
		var currTransList = this._data.currTransList;
		var currTransId = this._data.currTransId;
		for(var i = 0 ; i < currTransList.length; i++){
			if(currTransList[i].transId == currTransId){
				return currTransList[i];
				break;
			}
		}
		return;
	},
	
	_centerArea: function(){
		var currTran = this._getCurrTrans();
		if(!currTran || Ext.isEmpty(currTran.optionButtonList)){
			return [
				this._currentPerson()//当前处理人
                ,this._donePerson()//已处理人
			];
		}
		var front ,center = [],last ;
		
		front = [
			this._transaction()//处理事务
		];
		center = this._centerAreaCenter(currTran.optionButtonList);
		last = [
			this._turnOffProcess()
			,this._importantLevel()//通知紧急程度
			,this._opinions()//处理意见
			//,this._attachment()//附件
			,this._currentPerson()//当前处理人
			,this._donePerson()//已处理人
			//,this._submitPerson()//提交身份
			,this._turnOffProcess()
		];
		
		return front.concat(center).concat(last);
	},
	_centerAreaCenter: function(optionButtonList){
		
		var radios = [], front = [],center = [];
			
		for(var i=0; i<optionButtonList.length; i++){
			radios.push(optionButtonList[i].type);
			center.push(this._getOptionButtonsRalations(optionButtonList[i].type));
		}
		
		front = [this._optionButtons(radios)];//操作按钮
		/* center = [
			this._getOptionButtonsRalations('101'),
			this._getOptionButtonsRalations('102'),
			this._getOptionButtonsRalations('105'),
			this._getOptionButtonsRalations('106'),
			this._getOptionButtonsRalations('6')
		]; */
		
		return front.concat(center);
		
	},
	//顶部按钮
	_topButtons: function(){
		var me = this, isHidden = true;;
		if(!this._data){
			return;
		}
		if(this._data.isInititor || this._data.isPriPerson){
			isHidden = false;
		}
		
		return {
			xtype:'fieldcontainer',
			name: 'topButtons',
			layout: 'hbox',
			hidden: isHidden,
			items: [
				{flex: 1,xtype:'container'}
				,{ xtype: 'button', text: '以起草人身份操作', margin: '0 10 0 0',
					hidden: this._data.isInititor ? false : true,
					handler: function(btn){
						var win = Ext.create({
							xtype: 'approvalmodelsInititorWindow',
							title: btn.text,
							maximizable: true,
							modal: true,
							width: 800,
							height: 405,
							layout: 'fit',
							approvalProcess:{
								_data: me._data,
								parentProcess:me,
								triggerObj: me.triggerObj,
								baseParams: me.baseParams,
								customParams: me.customParams,
                                paramMap: me.paramMap,
								processInstanceId : me.processInstanceId,
								mainUrl:'activiti/service/inititor/',
								taskId: me.taskId,
                                customParamsToVariables: me.customParamsToVariables
							}
						});
						win.show();
					} 
				}
				,{ xtype: 'button', text: '以特权人身份操作',hidden: this._data.isPriPerson ? false : true}
			]
		};
	},
	//流程说明OK
	_processInfo: function(){
		var detail = '';
		if(this._data && this._data.processInstance && this._data.processInstance.processDefinitionConfig){
			detail = Ext.decode(this._data.processInstance.processDefinitionConfig).detail;
		}
		return {
			xtype:'approvalContainer',
			name : 'processInfo',
			left : {value: '<b style="color:#345;font-size:16px;">流程说明</b>'},
			isFirst: true,
			right: detail
		};
	},
	//显示审批记录复选框OK
	_showApprRecords: function(){
		var me = this;
		return {
			xtype:'approvalContainer',
			name: 'showApprRecords',
			left: {xtype: 'checkbox',checked: true, boxLabel: '<b>显示审批记录</b>',name: 'showApprRecords',
				isFormField: false,
				listeners: {
					change: function(){
						if(this.checked){
							me.down('approvalContainer[name=approvalGrid]').show();
						}else{
							me.down('approvalContainer[name=approvalGrid]').hide();
						}
					}
				}
			}
		};
	},
	//审批记录列表OK
	_approvalGrid: function(){
		var records, height = 32;
		if(this._data && this._data.taskRecordList){
			records = this._data.taskRecordList;
			height = height + 32*records.length
		}
		
		return {
			xtype:'approvalContainer',
			name: 'approvalGrid',
			// height: height,
            height: '100%',
			padding: 0,
			left: {xtype: 'approvalGrid',name: 'approvalGrid',defaultDatas: records}
		};
	},
	//流程状态-此流程已被暂停 数据返现
	_turnOffProcess: function(){
		var me = this;
		return {
			xtype:'approvalContainer',
			name: 'turnOffProcess',
			hidden: true,
			left: '流程状态',
			right: '<span style="color:#f00;">此流程已被暂停</span>'
		};
	},
	//处理事务 获取参数名 切换事务
	_transaction: function(){
		
		if(!this._data || Ext.isEmpty(this._data.currTransList)){
			return {hidden: true};
		}
		
		var me = this
			,records = this._data.currTransList
			,currTran = this._getCurrTrans()
			,value = ''
			,_this = this;
		
		if(currTran){
			value = currTran.transId;
			this.taskId = currTran.taskId;
			this.activityId = currTran.activityId;
		}
		
		return {
			xtype:'approvalContainer',
			height: 44,
			name: 'transaction',
			hidden: records.length <= 1 ? true : false,
			left: {
				value: '<b>事务处理</b>',
				margin: '5 0 0 0'
			},
			right: {
				padding: 5,
				layout: {type: 'hbox', align: 'stretch'},
				defaults: { 
					readOnly: this._isView
				},
				items:[
					{
						xtype: 'approvalTransCombo',
						defaultDatas: records,
						value: value,
						name: 'transaction',
						listeners: {
							select: function(combo, record){
								
								var radiogroup = me.down('approvalContainer[name=optionButtons]').down('radiogroup'),
								optionButtonList = record.get('optionButtonList'),
								optionButtonsObj = me._optionButtonsObj(),
								val = radiogroup.getValue(),
								urlInfo = record.get('transUrl'),
								args = [], i, param, urlParams,transParam, 
								radios;
								
								if(!optionButtonList || optionButtonList.length < 1){//如果选择的事务，不存在操作按钮
								
									var approveUtils = Ext.create('App.ux.approval.Utils');
									approveUtils.openWindowByUrl(
										urlInfo,
										function(param){
											me.transChange(param);
										}
									);
									
								}else{//如果存在操作按钮，那么切换操作按钮组
									if(val && val.cz && optionButtonsObj[val.cz].relationName){
										me.down(optionButtonsObj[val.cz].relationName).hide();
									}
									
									radiogroup.removeAll();
									
									for(i=0; i<optionButtonList.length; i++){
										args.push(optionButtonList[i].type);
									}
									
									radios = me._getOptionButtonsItems(args);
									
									for(i=0; i<radios.length; i++){
										if(radios[i].checked){
											radiogroup.lastValue = {cz:radios[i].inputValue};
											if(radios[i].relationName){
												me.down(radios[i].relationName).show();
											}
											break;
										}
									}
									
									radiogroup.add(radios);
								}

                                // 将当前事物id置为选中事物id
                                _this._data.currTransId = record.get('transId');
							}
						}
					}
				]
			}
		};
	},
	
	//操作按钮 获取参数名
	_optionButtons: function(args){
		var me = this;
		return {
			xtype:'approvalContainer',
			left: '操作',
			flagCZ: true,//操作区域对象
			name: 'optionButtons',
			right: {
				items:[
					{
						xtype: 'radiogroup',
						defaults: { 
							width: 60,
							readOnly: this._isView
						},
						
						items: this._getOptionButtonsItems(args),
						listeners: {
							change: function(th,newValue,oldValue){
								var newRelationName = th.down('radio[inputValue='+newValue.cz+']').relationName;
								var oldComp,oldRelationName, ralation, approvalRejectCombo, store, textarea;
								if(oldValue){
									oldComp = th.down('radio[inputValue='+oldValue.cz+']');
									if(oldComp){
										oldRelationName = oldComp.relationName;
									}
								}
								
								if(oldRelationName){
									me.down(oldRelationName).hide();
								}
								
								if(newRelationName){
									me.down(newRelationName).show();
								}
								if(newValue.cz == '101'){//如果选择了通过，默认同意
									textarea = me.down('textarea[name=comment]');
									if(textarea && Ext.isEmpty(textarea.getValue())){
										textarea.setValue('同意');
									}
								}
							},
							beforerender: function(th){
								var radios = th.getChecked();
								var newRelationName;
								if(radios[0]){
									newRelationName = radios[0].relationName;
								}
								if(newRelationName){
									var ralation = me.down(newRelationName);
									if(!ralation){
										return;
									}
									ralation.show();
									if(ralation.name == 'approvalReject'){
										approvalRejectCombo = ralation.down('approvalRejectCombo');
										store = approvalRejectCombo.getStore()
										store.proxy.extraParams = {
											taskId: me.taskId,
											rejectType: radios[0].rejectType
										};
										store.load();
									}
								}
								
							}
						}
					}
				]
			}
		};
	},
	
	//操作按钮触发对象OK
	_getOptionButtonsRalations: function(name){
		var i, radiosRelationObj;
		var defaultObj = this._optionButtonsObj();
		
		index = this._nameToIndex(name);
		if(defaultObj['re'+index]){
			radiosRelationObj = Ext.clone(defaultObj['re'+index]);
		}
		if(!radiosRelationObj){
			return {xtype: 'container', hidden: true}
		}
		return radiosRelationObj;
	},
	//通知紧急程度 - 数据返现
	_importantLevel: function(){
		return {
			xtype:'approvalContainer',
			left : '通知紧急程度',
			flagCZ: true,//操作区域对象
			name : 'importantLevel',
			right: {
				items:[
					{
						xtype: 'radiogroup',
						defaults: { 
							width: 60,
							readOnly: this._isView
						},
						items: [
							{ boxLabel: '紧急', name: 'sendLevel', inputValue: 1,style: {
								color: '#f00'
							}}
							,{ boxLabel: '急', name: 'sendLevel', inputValue: 2,style: {
								color: '#00f'
							}},
							,{ boxLabel: '一般', name: 'sendLevel', inputValue: 3, checked: true}
						]
					}
				]
			}
		};
	},
	//即将流向 OK
	_nextInto: function(){
		var obj;
		if(this._data && this._data.outgoingList){
			obj = this._getHtmlHeightMargin(this._data.outgoingList,'activityName');
		}
		if(obj){
			return {
				xtype:'approvalContainer',
				name: 'nextInto',
				flagCZ: true,//操作区域对象
				hidden: true,
				left: {
					value: '<b>即将流向</b>',
					margin: obj.margin
				},
				right: {
					style:{fontWeight: 'bold',background:'#fff'},
					html: obj.html,
					padding: '5 0 0 5'
				},
				height: obj.height
			}
		}
		return {
			xtype:'approvalContainer',
			flagCZ: true,//操作区域对象
			name: 'nextInto',
			hidden: true,
			left: '<b>即将流向</b>',
			right: ''
		};
	},
	//驳回到 - 数据返现 获取参数
	_approvalReject: function(){
		var me = this, isReturnMeConfig = this.isReturnMeConfig;
		/*if(this._data && this._data.processInstance && this._data.processInstance.processDefinitionConfig){
			checked = Ext.decode(this._data.processInstance.processDefinitionConfig).isAutoBackForReject;
		}*/
		return {
			xtype:'approvalContainer',
			height: 44,
			name: 'approvalReject',
			flagCZ: true,//操作区域对象
			openDisable: true,
			hidden: true,
			left: {
				value: '<b>驳回到</b>',
				margin: '5 0 0 0'
			},
			right: {
				padding: 5,
				layout: {type: 'hbox', align: 'stretch'},
				defaults: { 
					readOnly: this._isView
				},
				items:[
					{
						xtype: 'approvalRejectCombo',
						name: 'endActivityId',
						mainRunTimeUrl: this.mainRunTimeUrl,
						defaultParams: {taskId : me.taskId},
						listeners:{
							// select: function(combo,record){
							// 	var isReturnMe = this.up('approvalContainer[name=approvalReject]').down('checkbox[name=isReturnMe]');
							// 	if(record.get('isReturnMe')){
							// 		isReturnMe.show();
							// 		//isReturnMe.setValue(true);
							// 		isReturnMe.isFormField = true;
							// 	}else{
							// 		isReturnMe.hide();
							// 		isReturnMe.setValue(false);
							// 		isReturnMe.isFormField = false;
							// 	}
							// },
							show: function(th){
								var param = me.getValues();
								var optionButtonsObj = me._optionButtonsObj();
								th.defaultParam = {
									taskId: me.taskId,
									rejectType: optionButtonsObj[params.cz].rejectType
								};
							},
							beforerender: function(th){
								var parent = th.up('container');
								th.getStore().on({
									load: function(t, records, successful){
										if(records.length<1){//如果没有数据，不显示驳回节点下拉框
											parent.removeAll();
											parent.add({xtype: 'container',html: '没有驳回节点',padding: '5 0 0 5'});
										}
									}
								});
							}
						}
					}
					,{
						xtype: 'checkbox', checked: isReturnMeConfig.checked,boxLabel: '驳回的节点通过后直接返回本人',name: 'isReturnMe',
						hidden:isReturnMeConfig.hidden, disabled:isReturnMeConfig.disabled,isFormField: true
					}
				]
			}
		};
	},
	//超级驳回 - 数据返现 获取参数
	_approvalSuperReject: function(){
		var me = this, isReturnMeConfig = this.isReturnMeConfig;
		/*if(this._data && this._data.processInstance && this._data.processInstance.processDefinitionConfig){
			checked = Ext.decode(this._data.processInstance.processDefinitionConfig).isAutoBackForReject;
		}*/
		return {
			xtype:'approvalContainer',
			height: 44,
			name: 'approvalSuperReject',
			flagCZ: true,//操作区域对象
			openDisable: true,
			hidden: true,
			left: {
				value: '<b>驳回到</b>',
				margin: '5 0 0 0'
			},
			right: {
				padding: 5,
				layout: {type: 'hbox', align: 'stretch'},
				defaults: { 
					readOnly: this._isView
				},
				items:[
					{
						xtype: 'approvalRejectCombo',
						name: 'endActivityId',
						mainRunTimeUrl: this.mainRunTimeUrl,
						defaultParams: {taskId : me.taskId},
						listeners:{
							// select: function(combo,record){
							// 	var isReturnMe = this.up('approvalContainer[name=approvalSuperReject]').down('checkbox[name=isReturnMe]');
							// 	if(record.get('isReturnMe')){
							// 		isReturnMe.show();
							// 		//isReturnMe.setValue(true);
							// 		isReturnMe.isFormField = true;
							// 	}else{
							// 		isReturnMe.hide();
							// 		isReturnMe.setValue(false);
							// 		isReturnMe.isFormField = false;
							// 	}
							// },
							show: function(th){
								var param = me.getValues();
								var optionButtonsObj = me._optionButtonsObj();
								th.defaultParam = {
									taskId: me.taskId,
									rejectType: optionButtonsObj[params.cz].rejectType
								};
							},
							beforerender: function(th){
								var parent = th.up('container');
								th.getStore().on({
									load: function(t, records, successful){
										if(records.length<1){//如果没有数据，不显示驳回节点下拉框
											parent.removeAll();
											parent.add({xtype: 'container',html: '没有驳回节点',padding: '5 0 0 5'});
										}
									}
								});
							}
						}
					}
					,{
						xtype: 'checkbox', checked: isReturnMeConfig.checked, boxLabel: '驳回的节点通过后直接返回本人',name:'isReturnMe',
                        hidden:isReturnMeConfig.hidden, disabled:isReturnMeConfig.disabled, isFormField: true
					}
				]
			}
		};
	},
	//转办人员 - 数据返现 获取参数
	_transactor: function(){
		var excludes = [];
		if(this._data && this._data.task && this._data.task.assigneeList){
			var assigneeList = this._data.task.assigneeList;
			for(var i = 0; i<assigneeList.length ;i++){
				excludes.push(assigneeList[i].userId);
			}
		}
		
		return {
			xtype:'approvalContainer',
			height: 44,
			name: 'transactor',
			flagCZ: true,//操作区域对象
			openDisable: true,
			hidden: true,
			left: {
				value: '<b>转办人员</b>',
				margin: '5 0 0 0'
			},
			right: {
				padding: 5,
				items:[
					{
						xtype: 'memberMulitSelectField',
						name: 'transactor',
						excludes: excludes,
						memberSelModel: 'single',
						readOnly: this._isView,
						width: 600
					}
				]
			}
		};
	},
	//沟通人员 - 数据返现 获取参数
	_communicator: function(){
		var html = '' , i, temp, person = '', height = 68, hidden = false, excludes = [];
		if(this._data && this._data.currUser){
			excludes.push(this._data.currUser.userId);
		}
		if(this._data && this._data.communicateUserList){
			temp = this._data.communicateUserList;
			for(i = 0 ; i<temp.length; i++){
				person += temp[i].name + '，';
				excludes.push(temp[i].userId);
			}
			person = person.substr(0,person.length-1);
		}
		if(person){
			html = person;
		}else{
			height = height - 24;
			hidden = true;
		}
		
		return {
			xtype:'approvalContainer',
			height: height,
			flagCZ: true,//操作区域对象
			openDisable: true,
			name: 'communicator',
			hidden: true,
			left: {
				value: '<b>沟通人员</b>',
				margin: '5 0 0 0'
			},
			right: {
				padding: '0 0 0 5',
				layout: {
					type: 'vbox'
				},
				items:[
					{
						xtype: 'container',
						name: 'communicatorName',
						padding: '5 0 0 5',
						hidden: hidden,
						html: html,
						height: 26,
						width: 800
					},
					{
						xtype: 'memberMulitSelectField',
						name: 'communicator',
						excludes: excludes,
						padding: hidden ? '5 0 0 0' : 0,
						height: 34,
						readOnly: this._isView,
						width: 600
					}
				]
			}
		};
	},
	//取消人员 - 数据返现 获取参数
	_cancelPersons: function(){
		var items = [], i, temp;
		if(this._data && this._data.communicateUserList){
			temp = this._data.communicateUserList;
			for(i = 0 ; i<temp.length; i++){
				items.push({
					boxLabel: temp[i].name,
					name: 'cancelPerson',
					margin: '0 5 0 0',
					inputValue: temp[i].userId
				});
			}
			
		}
		return {
			xtype:'approvalContainer',
			//height: 44,
			name: 'cancelPersons',
			flagCZ: true,//操作区域对象
			openDisable: true,
			hidden: true,
			left: {
				value: '<b>取消人员</b>'
				//margin: '5 0 0 0'
			},
			right: {
				//padding: '5 0 0 0',
				items:[
					{
						xtype: 'checkboxgroup',
						defaults: { 
							width: 60,
							readOnly: this._isView
						},
						items: items
					}
				]
			}
		};
	},
	//处理意见 - 数据返现
	_opinions: function(){
		var me = this;
		
		return {
			xtype:'approvalContainer',
			height: 150,
			name: 'opinions',
			flagCZ: true,//操作区域对象
			left:{
				value: '<b>处理意见</b>',
				margin: '55 0 0 0'
			},
			right: {
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{xtype: 'container',height: 33, margin: '5 0 5 5',layout: {type: 'hbox'},
						hidden: this._isView,
						items: [
							{
								xtype: 'approvalSuggestionCombo',
								name: 'opinions',
								isFormField: false,
								minHeight: 32,
								fieldLabel: '常用意见',
								labelWidth: 70,
								listeners: {
									select: function(combo, record){
										var approvalProcess = combo.up('approvalProcess')
										,comment = approvalProcess.down('textarea[name=comment]')
										,val = comment.getValue();
										
										if(!val){
											comment.setValue(record.get('name'));
										}else if(val.indexOf(record.get('name'))<0){
											comment.setValue(val + record.get('name'));
										}
										
									}
								}
							}
						]
					},
					{xtype: 'container',flex: 1,layout: {type: 'hbox',align: 'stretch'},
						margin: this._isView ? '5 0 0 0' : 0,
						items: [
							{
								xtype: 'textarea',
								name:'comment',
								readOnly: this._isView,
								value: this.defaultComment ? this.defaultComment : '',
								margin: '0 5 5 5',
								flex: 1
							},
							this._submitBtn(),
							{
								xtype: 'container',
								width: 40
							}
						]
					}
				]
			}
		};
	},
	//附件
	_attachment: function(){
		return {
			xtype:'approvalContainer',
			height: 44,
			flagCZ: true,//操作区域对象
			name: 'attachment',
			left:{
				value: '<b>附件</b>',
				margin: '5 0 0 0'
			},
			right: {
				padding: 5,
				items:[
					{
						xtype:'filefield',
						width: 600,
						emptyText : '请选择需要上传的附件',
						buttonConfig: {
							text: '上传文件',
							iconCls: 'fa fa-file-o fa-lg'
						}
					}
				]
			}
		};
	},
	//当前处理人 OK
	_currentPerson: function(){
		var me = this;
		var obj;
		if(this._data && this._data.currAssigneeList){
			obj = this._getHtmlHeightMargin(this._data.currAssigneeList,'info');
		}
		if(obj){
			return {
				xtype:'approvalContainer',
				name: 'currentPerson',
				flagCZ: true,//操作区域对象
				left: {
					value: '<b>当前处理人</b>',
					margin: obj.margin
				},
				right: {
					style:{fontWeight: 'bold',background:'#fff'},
					html: obj.html,
					padding: '5 0 0 5'
				},
				height: obj.height
			}
		}
		
		return {
			xtype:'approvalContainer',
			name: 'currentPerson',
			flagCZ: true,//操作区域对象
			left: '当前处理人',
			right: ''//'N4.管理员审批节点: <a href="#" onclick=approval_xw_Click("'+me.id+'")></a>'
		};
	},
	//已处理人 Ok
	_donePerson: function(){
		var obj;
		if(this._data && this._data.completedUserList){
			obj = this._getHtmlHeightMargin(this._data.completedUserList,'name',true);
		}
		if(obj){
			return {
				xtype:'approvalContainer',
				name: 'donePerson',
				flagCZ: true,//操作区域对象
				left: {
					value: '<b>已处理人</b>',
					margin: obj.margin
				},
				right: {
					style:{fontWeight: 'bold',background:'#fff'},
					html: obj.html,
					padding: '5 0 0 5'
				},
				height: obj.height
			}
		}
		
		return {
			xtype:'approvalContainer',
			name: 'donePerson',
			flagCZ: true,//操作区域对象
			left: '已处理人',
			right: ''
		};
	},
	//提交身份 - 数据返现 获取参数
	_submitPerson: function(){
		var obj;
		if(this._data && this._data.currUser){
			obj = this._getHtmlHeightMargin([this._data.currUser],'name');
		}
		if(obj){
			return {
				xtype:'approvalContainer',
				name: 'donePerson',
				flagCZ: true,//操作区域对象
				left: {
					value: '<b>提交身份</b>',
					margin: obj.margin
				},
				right: {
					style:{fontWeight: 'bold',background:'#fff'},
					html: obj.html,
					padding: '5 0 0 5'
				},
				height: obj.height
			}
		}
		return {
			xtype:'approvalContainer',
			name: 'submitPerson',
			flagCZ: true,//操作区域对象
			left: '提交身份',
			right: ''
		};
	},
	//流程图复选框
	_progressMapCheckbox: function(){
		var me = this;
		return {
			xtype:'approvalContainer',
			name: 'progressMapCheckbox',
			left: {xtype: 'checkbox', boxLabel: '<b>流程图</b>',name: 'progressChart',
				listeners: {
					change: function(){
                        var progressMap = me.down('approvalContainer[name=progressMap]');
                        if(this.checked){
                            progressMap.show();
                        	var dom = progressMap.down('box').el.dom;
                            var outerHTML = dom.childNodes[0].outerHTML;
                            dom.innerHTML = outerHTML;

						}else{
                            progressMap.hide();
						}
					}
				}
			}
		};
	},
	//流程图
	_progressMap: function(){
		
		var url= "";
        if(this._data){
            url = this._data.processImgUrl;
        }
		return {
			xtype:'approvalContainer',
			height: 600,
			name: 'progressMap',
			padding: 0,
			hidden: true,
			left: {xtype: 'box',name: 'panel',flex: 1,html:'<iframe name="frame" src="'+url+'" height="100%" width="100%"/></iframe>'}
		};
	},
	//更多信息复选框
	_moreInfoCheckbox: function(){
		var me = this;
		return {
			xtype:'approvalContainer',
			name: 'moreInfoCheckbox',
			left: {xtype: 'checkbox', boxLabel: '<b>更多信息</b>',name: 'moreInfo',isFormField: false,
				listeners: {
					change: function(){
						if(this.checked){
							me.down('approvalContainer[name=moreInfo]').show();
						}else{
							me.down('approvalContainer[name=moreInfo]').hide();
						}
					}
				}
			}
		};
	},
	//更多信息 - 数据返现
	_moreInfo: function(){
		var informOptions = this._informOptions();
		informOptions.margin = '5 5 5 5';
		informOptions.isFirst = true;
		var  me = this;
		
		return {
			xtype:'approvalContainer',
			height: 500,
			name: 'moreInfo',
			padding: 0,
			hidden: true,
			right: {xtype: 'tabpanel',name: 'panel',flex: 1,items:[
				/*{
					xtype:'container',
					title:'高级',
					iconCls: 'x-fa fa-wrench',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [
						informOptions,//通知选项
						{
							xtype:'approvalContainer',
							flex: 1,
							border: false
						}
					]
				},*/
				{xtype: 'approvalPointsGrid',title: '表格',iconCls: 'x-fa fa-sliders',mainRunTimeUrl: this.mainRunTimeUrl,
					defaultParams: {	processInstanceId : me.processInstanceId}
				},
				{xtype: 'approvalProgressInfoGrid',title: '流程日志',iconCls: 'x-fa fa-book ',mainRunTimeUrl: this.mainRunTimeUrl,
					defaultParams: {	processInstanceId : me.processInstanceId}
				}
			]}
		};
	},
	
	//通知选项 - 数据返现
	_informOptions: function(){
		return {
			xtype:'approvalContainer',
			name: 'informOptions',
			left: '通知选项',
			right: {
				items: [
					{xtype: 'checkbox',margin: '0 0 0 10', boxLabel: '<b>流程结束后通知我</b>',name: 'infoEnd'}
				]
			}
		};
	},
	
	//提交按钮 - 触发接口功能
	_submitBtn: function(){
		var me = this;
		var hidden = this._isView;
		if(this.startSubmitHidden && this._data){
			if(!this._data.task || this._data.task.activity && this._data.task.activity.activityId == "startTask"){
                hidden = this.startSubmitHidden;
			}
		}

		var btn = {
			xtype: 'button',
			text: '提交',
			name: 'aprroval_Submit',
			hidden: hidden,
			margin: '0 5 5 0',
			width: 100,
			scale: 'large',
			handler: function(btn){
				if(!me._valid()){
					return;
				}
				me.submitBefore(btn);
				
			}
		};
        if(this.submitButton){
            var submitButton = Ext.clone(this.submitButton);
            btn = Ext.applyIf(submitButton,btn);
            btn.name = 'aprroval_Submit';
        }
        return btn;
	},
	
	_valid: function(){
		var value = this.getValues() , me = this;
		
		if(!value.cz){
			me.showToast('提示','请选择【操作】');
			return;
		}
		
		var optionButtonsObj = this._optionButtonsObj(),
			option;
		
		option = optionButtonsObj[value.cz];
		if(!option){
			return;
		}
		
		if(!option.getParam || !option.getParam()){
			me.showToast('提示','该【操作】无法提交');
			return;
		}
		
		var approvalMain = this.up('approvalMain');
		//如果当期操作不是通过或提交文档操作，并且approvalMain.isSubmitTrans为true被标记为需要先提交业务数据时
		if(value.cz != '101' && value.cz != '1' && value.cz != '112' && approvalMain && approvalMain.isSubmitTrans){
			//将isSubmitTrans置为false，表示不需要提交业务端数据。
			approvalMain.isSubmitTrans = false;
		}
		
		return true;
	},
	
	_submit: function(){
		var me = this, data = '';
		if(me._data && !Ext.isEmpty(me._data.setAssigneeNodeList)){
			data = me._data.setAssigneeNodeList;
		}
		var param = me.getValues();
		if(!data || (param.cz != '101' && param.cz != '1' && param.cz != '112')){//如果为data空或者不是选择通过、提交文档操作，表示不需要设置处理人，直接提交
			me._ajaxRequest();
			return;
		}
		//设置节点的处理人并提交
		Ext.Msg.show({
			title:'提示',
			message: '有节点必须设置处理人，请设置该节点的处理人后再进行提交操作！',
			buttons: Ext.Msg.YES,
			fn: function(btn) {
				//弹出窗口配置节点处理人
				if(btn == 'yes'){
					var win = Ext.create({
						xtype: 'approvalNodePersonsWindow',
						title: '审批节点处理人设置',
						data: data,
						onConfirm: function(btn){
							var exParam = win.getValues();
							if(exParam){
								me._ajaxRequest(exParam);
								win.close();
							}
							
						}
					}).show();
				}
				
			}
		});
	},
	_getOperatorInfo:function () {
        var value = this.getValues();
        var optionButtonsObj = this._optionButtonsObj();
        var option = optionButtonsObj[value.cz];
        if(option){
            value.operatorName = option.boxLabel;
        }
		return value;
    },

	_ajaxRequest: function(exParam){
		var value = this.getValues() , me = this , data = '',submitBtn = this.down('button[name="aprroval_Submit"]');
		
		var optionButtonsObj = this._optionButtonsObj(),
			url = '',params = '', option;
		
		option = optionButtonsObj[value.cz];
		url = option.url;
		
		params = option.getParam();
		
		if(this.customParams){
			Ext.apply(params, this.customParamsToVariables());
		}
		
		if(exParam){
			Ext.apply(params, exParam);
		}
		if(submitBtn){
            submitBtn.disable();
		}

		Ext.Ajax.request({
			url: this.mainUrl + url,
			params: {
				dataJson:JSON.stringify(params),
				paramMap:JSON.stringify(this.paramMap)
			},
			success: function(response, opts) {
				var obj = Ext.decode(response.responseText);
				if(submitBtn && !submitBtn.isDestroyed){
                    submitBtn.enable();
				}

				if(obj.success){
                    me.showToast('提示','流程提交成功！');

                    me.updateApproval(obj.data);
					me.submitSuccess(obj.data);
				}else{
					me.showToast('提交失败','流程审批提交失败！');
					me.submitFailure(obj);
				}
			},

			failure: function(response, opts) {
				me.showToast('提交失败','流程审批提交失败！');
                if(submitBtn){
                    submitBtn.enable();
                }
				me.submitFailure(response);
			}
		});
	},
	
	_getOptionButtonsItems: function(args/*[1,2,..7]*/){
		var i, radios = [],index, temp, hasTG = false;
		var defaultObj = this._optionButtonsObj();
		
		for(i=0; i<args.length; i++){
			index = this._nameToIndex(args[i]);
			temp = Ext.clone(defaultObj[index]);
			if(index == 101){//有通过按钮，默认选中通过按钮
				temp.checked = true;
				this.defaultComment = '同意';
				hasTG = true;
			}
			radios.push( temp );
		}
		
		if(radios.length == 0){
			return {xtype: 'container', hidden: true}
		}
		if(radios.length > 0 && radios[0] && !hasTG){//如果没有通过按钮，那么默认选中第一个按钮
			radios[0].checked = true;
		}
		
		
		return radios;
	},
	
	_getHtmlHeightMargin: function(list,name,noBr){
		var html = '',height,approvalMain, defaultHeight = this.ApprovalContainerHeight, br = '</br>';
		if(!defaultHeight){
			approvalContainer = Ext.create({xtype: 'approvalContainer'});
			defaultHeight = approvalContainer.height;
			this.ApprovalContainerHeight = defaultHeight;
		}
			
		height = defaultHeight;
		
		if(noBr){
			br = ' , '
		}
		
		if(list){
			if(list.length > 1){
				height = (list.length - 1) * 17 + height;
			}
			for(var i = 0 ; i < list.length; i++){
				
				if(i > 0){
					html = html + br + list[i][name];
				}else{
					html = list[i][name];
				}
				
			}
			return {
				html : html,
				height: height,
				margin: (height - defaultHeight)/2 +' 0 0 0'
			};
		}
		
		return;
	},
	
	_nameToIndex: function(name){
		switch(name){
			case '通过':
				return 101;
			case '驳回':
				return 102;
			case '转办':
				return 105;
			case '沟通':
				return 106;
			case '废弃':
				return 109;
			case '取消沟通':
				return 107;
			case '提交文档':
				return 112;
			case '驳回到起草人':
				return 104;
			case '超级驳回':
				return 103;
			case '节点暂停':
				return 111;
			case '节点唤醒':
				return 110;
			case '催办':
				return 203;
			case '撤回':
				return 201;
			case '起草人废弃':
				return 202;
			default:
				return name;
		}
	},
	
	_optionButtonsObj: function(){
		var me = this;
		var getParam = function(){
			var param = me.getValues();
			return {
				taskId: me.taskId, 
				comment :param.comment,
				sendLevel: param.sendLevel
			};
		};
		
		if(!this.optionButtonsObj){
			this.optionButtonsObj = {
				1:{ boxLabel: '提交文档', name: 'cz', inputValue: '1',width: 80, relationName: 'approvalContainer[name=nextInto]',url: 'startProcess',getParam: function(){
					var param = me.getValues();
					return {
						processDefinitionKey: me.baseParams.processDefinitionKey, 
						processInstanceName: me.baseParams.processInstanceName,
						comment : param.comment,
						sendLevel: param.sendLevel,
                        token:me.baseParams.token,
                        businessKey: me.baseParams.businessKey
					};
				}},
				// 办理人操作按钮
				101:{ boxLabel: '通过', name: 'cz', inputValue: '101' , relationName: 'approvalContainer[name=nextInto]',url: 'completeTask',getParam: function(){
					var param = me.getValues();
					if(!param.comment){
						me.showToast('提示','请输入【处理意见】');
						return ;
					}
					return {
						taskId: me.taskId, 
						sendLevel: param.sendLevel,
						comment : param.comment,
                        businessKey: me.baseParams.businessKey
					};
				}},
				
				re101: this._nextInto(),//即将流向
				
				102:{ boxLabel: '驳回', name: 'cz', inputValue: '102',rejectType: 0, relationName: 'approvalContainer[name=approvalReject]',url: 'rejectTask',
					getParam: function(){
						var param = me.getValues();
						if(!param.endActivityId){
							me.showToast('提示','请选择需要【驳回到】的节点');
							return ;
						}
						var isReturnMe = false;
						if(param.isReturnMe){
							isReturnMe = true;
						}
						return {
							taskId: me.taskId, 
							comment : me.getValues().comment,
							sendLevel: param.sendLevel,
							rejectType: this.rejectType, 
							endActivityId: param.endActivityId,
							isReturnMe: isReturnMe
						};
					},
					listeners: {
						change: function(){
							if(this.checked){
								var reject = me.down(this.relationName).down('approvalRejectCombo');
								if(!reject){
									return;
								}
								reject.getStore().proxy.extraParams = {
									taskId: me.taskId,
									rejectType: this.rejectType
								};
								reject.getStore().load();
							}
						}
					}
				},
				
				re102: this._approvalReject(),//驳回到
				
				103:{ boxLabel: '超级驳回', name: 'cz', inputValue: '103',rejectType: 1, width: 80,url: 'rejectTask',relationName: 'approvalContainer[name=approvalSuperReject]',
					getParam: function(){
						var param = me.getValues();
						if(!param.endActivityId){
							me.showToast('提示','请选择需要【驳回到】的节点');
							return ;
						}
						var isReturnMe = false;
						if(param.isReturnMe){
							isReturnMe = true;
						}
						return {
							taskId: me.taskId, 
							sendLevel: param.sendLevel,
							comment : me.getValues().comment,
							rejectType: this.rejectType, 
							endActivityId: param.endActivityId,
							isReturnMe: isReturnMe
						};
					},
					listeners: {
						change: function(){
							if(this.checked){
								var reject = me.down(this.relationName).down('approvalRejectCombo');
								if(!reject){
									return;
								}
								reject.getStore().proxy.extraParams = {
									taskId: me.taskId,
									rejectType: this.rejectType
								};
								reject.getStore().load();
							}
						}
					}
				},
				
				re103: this._approvalSuperReject(),//超级驳回
				
				104:{ boxLabel: '驳回到起草人', name: 'cz', inputValue: '104',rejectType: 2, width: 100,url: 'rejectTask',
					getParam: function(){
						var param = me.getValues();
						return {
							taskId: me.taskId, 
							sendLevel: param.sendLevel,
							comment : me.getValues().comment,
							rejectType: this.rejectType
						};
					}
				},
				
				105:{ boxLabel: '转办', name: 'cz', inputValue: '105' ,relationName: 'approvalContainer[name=transactor]',url: 'assigneeTask',getParam: function(){
					var param = me.getValues();
					if(!param.transactor || param.transactor.length == 0){
                        me.showToast('提示','请选择【转办人员】');
						return;
					}
					return {
						taskId: me.taskId, 
						sendLevel: param.sendLevel,
						comment : param.comment,
						toPerson: param.transactor.join(",")
					};
				}},
				
				re105: this._transactor(),//转办人员
				
				106:{ boxLabel: '沟通', name: 'cz', inputValue: '106' ,relationName: 'approvalContainer[name=communicator]',url: 'communicateTask',getParam: function(){
					var param = me.getValues(),toPersons = [], i, temp ;
					if(!param.communicator && Ext.isEmpty(me._data.communicateUserList)){
						me.showToast('提示','请选择【沟通人员】');
						return ;
					}
					if(!param.comment){
						me.showToast('提示','请输入【处理意见】');
						return ;
					}
					return {
						taskId: me.taskId, 
						sendLevel: param.sendLevel,
						comment : param.comment,
						toPersons: [].concat(param.communicator)
					};
				}},
				
				re106: this._communicator(),//沟通人员
				
				107:{ boxLabel: '取消沟通', name: 'cz', inputValue: '107',width: 80,relationName: 'approvalContainer[name=cancelPersons]',url: 'communicateCancelTask',getParam: function(){
					var param = me.getValues();
					if(!param.cancelPerson){
						me.showToast('提示','请选择【取消人员】');
						return ;
					}
					return {
						taskId: me.taskId, 
						sendLevel: param.sendLevel,
						comment : param.comment,
						toPersons: [].concat(param.cancelPerson)
					};
				}},
				
				re107: this._cancelPersons(),//取消人员
				
				108:{ boxLabel: '回复沟通', name: 'cz', inputValue: '108' ,width: 80,url: 'communicateReplyTask',getParam: function(){
					var param = me.getValues();
					var currTran = me._getCurrTrans()
					if(!param.comment){
						me.showToast('提示','请输入【处理意见】');
						return ;
					}
					return {
						taskId: me.taskId, 
						sendLevel: param.sendLevel,
						comment : param.comment,
						communicateLinkId: currTran.communicateLinkId
					};
				}},
				
				109:{ boxLabel: '废弃', name: 'cz', inputValue: '109',url: 'trashTask',getParam: function(){
					var param = me.getValues();
					return {taskId: me.taskId, sendLevel: param.sendLevel,comment :me.getValues().comment};
				}},
				
				110:{ boxLabel: '节点唤醒', name: 'cz', inputValue: '110',width: 80,relationName: 'approvalContainer[name=turnOffProcess]',url: 'activeTask',getParam: function(){
					var param = me.getValues();
					return {taskId: me.taskId,sendLevel: param.sendLevel, comment :me.getValues().comment};
				}},
				
				111:{ boxLabel: '节点暂停', name: 'cz', inputValue: '111',width: 80,url: 'suspendTask',getParam: function(){
					var param = me.getValues();
					return {taskId: me.taskId,sendLevel: param.sendLevel, comment :me.getValues().comment};
				}},
				
				
				112:{ boxLabel: '提交文档', name: 'cz', inputValue: '112',width: 80, relationName: 'approvalContainer[name=nextInto]',url: 'completeTask',getParam: function(){
					var param = me.getValues();
					return {
						taskId: me.taskId, 
						comment : param.comment,
						sendLevel: param.sendLevel,
                        businessKey: me.baseParams.businessKey
					};
				}},
				
				
				// 起草人操作按钮
				201:{ boxLabel: '撤回', name: 'cz', inputValue: '201',url: 'backStartTask',
				getParam: getParam },
				202:{ boxLabel: '废弃', name: 'cz', inputValue: '202',url: 'trashTask',
				getParam: getParam },
				203:{ boxLabel: '催办', name: 'cz', inputValue: '203',url: 'urgeTask',
				getParam: function(){
                    var param = me.getValues();
                    return {
                        taskId: me.taskId,
                        comment :param.comment,
                        sendLevel: param.sendLevel,
                        infoEnd: param.infoEnd ? 1 : 0
                    };
                } },
				
				// 特权人操作按钮
				301:{ boxLabel: '终审通过', name: 'cz', inputValue: '301',url: 'finalCompleteTask',
				getParam: getParam },
				302:{ boxLabel: '前后跳转', name: 'cz', inputValue: '302',url: 'jumpTask',relationName: '',getParam: function(){
					var param = me.getValues();
					return {
						taskId: me.taskId, 
						comment :param.comment,
						sendLevel: param.sendLevel,
						endActivityId: param.endActivityId
					};
				}},
				303:{ boxLabel: '直接废弃', name: 'cz', inputValue: '303',url: 'trashTask',
				getParam: getParam },
				304:{ boxLabel: '修改当前处理人', name: 'cz', inputValue: '304',url: 'batchAssigneeTask',
				getParam: function(){
					var param = me.getValues();
					return {
						comment :param.comment,
						sendLevel: param.sendLevel,
						assignees: '',//暂时不知从哪里区
						toPersons: param.communicator
					};
				}},
				//305:{ boxLabel: '修改流程', name: 'cz', inputValue: '305',url: ''},
				//306:{ boxLabel: '结束并行分支', name: 'cz', inputValue: '306',url: ''},
				307:{ boxLabel: '节点暂停', name: 'cz', inputValue: '307',url: 'suspendTask',
				getParam: getParam },
				308:{ boxLabel: '节点唤醒', name: 'cz', inputValue: '308',url: 'activeTask',
				getParam: getParam },
				309:{ boxLabel: '流程暂停', name: 'cz', inputValue: '309',url: 'suspendProcess',
				getParam: getParam },
				310:{ boxLabel: '流程唤醒', name: 'cz', inputValue: '310',url: 'activeProcess',
				getParam: getParam },
				311:{ boxLabel: '催办', name: 'cz', inputValue: '311',url: 'urgeTask',
				getParam: getParam }
				
			};
		}
		
		return this.optionButtonsObj;
	},
	showToast:function(title,html){
		Ext.toast({
			title : title,
			html : html,
			align : 't',
			closable : true,
			slideInDuration : 150,
			minWidth : 300,
			minHeight : 30
		});
	},
	
	hideFlagCZ: function(){
		var comps = this.query('approvalContainer[flagCZ=true]');
		
		for(var i = 0; i<comps.length; i++){
			if(!comps[i].hidden){
				comps[i].hide();
			}
		}
	},
	
	//提交成功回调方法
	updateApproval: function(callBack){
		
	},
	
	submitSuccess: function(callBack){
		
	},
	
	submitFailure: function(callBack){
		
	},
	
	submitBefore: function(btn){
		this._submit();
	},
	
	//切换事务回调方法
	transChange: function(callBack){
		
	}
});