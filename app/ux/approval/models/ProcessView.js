/**审批流程查看状态下的模版
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.models.ProcessView', {
	extend : 'App.ux.approval.Process',
	alias: 'widget.approvalmodelsProcessView',
	_getItems: function(){
		return [
            this._topButtons()//顶部按钮
			,this._processInfo()//流程说明
            ,this._showApprRecords()//显示审批记录复选框
            ,this._approvalGrid()//审批记录列表
            ,this._currentPerson()//当前处理人
            ,this._donePerson()//已处理人
			,this._progressMapCheckbox()//流程图
			,this._progressMap()//流程图
            ,this._moreInfoCheckbox()//更多信息复选框
            ,this._moreInfo()//更多信息
		];
	}
});
