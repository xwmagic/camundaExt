/**办理人模版
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.models.Assignee', {
	extend : 'App.ux.approval.Process',
	alias: 'widget.approvalmodelsAssignee',
	_getItems: function(){
		var me = this;
		return [
			 this._topButtons()//顶部按钮
			,this._processInfo()//流程说明
			,this._optionButtons(['通过','驳回','转办','沟通','废弃','取消沟通'])//操作按钮
			,this._importantLevel()//通知紧急程度
			,this._getOptionButtonsRalations('通过') //操作按钮触发对象
			,this._getOptionButtonsRalations('驳回') //操作按钮触发对象
			,this._getOptionButtonsRalations('转办') //操作按钮触发对象
			,this._getOptionButtonsRalations('沟通') //操作按钮触发对象
			,this._getOptionButtonsRalations('取消沟通') //操作按钮触发对象
			,this._nextInto()//即将流向
			,this._opinions()//处理意见
			,this._submitPerson()//提交身份
			,this._progressMapCheckbox()//流程图复选框
			,this._progressMap()//流程图
			,this._moreInfoCheckbox()//更多信息复选框
			,this._moreInfo()//更多信息
		];
	},
	
	_getOptionButtonsItems: function(args){
		var items = this.callParent([args]);
		items[0].readOnly = true;
		items[0].checked = true;
		return items;
	}
	
});
