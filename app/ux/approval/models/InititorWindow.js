/**以起草人操作弹出窗口
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.models.InititorWindow', {
	extend : 'Ext.window.Window',
	alias: 'widget.approvalmodelsInititorWindow',
	approvalProcess: {},
	initComponent: function(){
		this.items = this._createItems();
		
		this.buttons = this._createButtons();
		
		this.callParent()
	},
	
	_createItems: function(){
		var me = this;
		var approvalProcess = {
			xtype: 'approvalmodelsInititor',
			listeners: {
				beforerender: function(th){
					var currentPerson = th.down('approvalContainer[name=currentPerson]');
					var donePerson = th.down('approvalContainer[name=donePerson]');
					var height = currentPerson.height + donePerson.height - 66;
					me.setHeight(me.getHeight() + height);
					
				}
			}
		};
		approvalProcess = Ext.applyIf(Ext.clone(this.approvalProcess) , approvalProcess);
		
		return [approvalProcess];
	},
	
	_createButtons: function(){
		var me = this;
		return [{
			text: '提交',
			handler: function(){
				me.down('approvalmodelsInititor')._submit();
			}
		},{
			text: '取消',
			handler: function(){
				me.close();
			}
		}];
	}
	
});
