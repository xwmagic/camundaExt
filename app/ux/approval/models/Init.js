/**审批流程初始化模版
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.models.Init', {
	extend : 'App.ux.approval.Process',
	alias: 'widget.approvalmodelsInit',
	_getItems: function(){
		/*var me = this, frontArea, centerArea, lastArea;
		frontArea = [
			 this._processInfo()//流程说明
		];
		centerArea = this._centerArea();//中间操作区域
		lastArea = [
			 this._progressMapCheckbox()//流程图复选框
			,this._progressMap()//流程图
			,this._moreInfoCheckbox()//更多信息复选框
			,this._moreInfo()//更多信息
		];
		return frontArea.concat(centerArea).concat(lastArea);
		*/
		return [
			// this._topButtons()//顶部按钮
			,this._processInfo()//流程说明
			,this._optionButtons([1])//操作按钮
			,this._importantLevel()//通知紧急程度
			,this._nextInto()//即将流向
			,this._opinions()//处理意见
			,this._submitPerson()//提交身份 就是当前用户，只显示
			,this._progressMapCheckbox()//流程图复选框
			,this._progressMap()//流程图
			//,this._moreInfoCheckbox()//更多信息复选框
			//,this._moreInfo()//更多信息只显示表格
		];
	},
	_centerArea: function(){
		
		if(this._data && Ext.isEmpty(this._data.optionButtonList)){
			return [];
		}
		
		var center = [],last ;
		
		center = this._centerAreaCenter(this._data.optionButtonList);
		last = [
			 this._importantLevel()//通知紧急程度
			,this._opinions()//处理意见
		];
		
		return center.concat(last);
	},
	
	_getOptionButtonsItems: function(args){
		var items = this.callParent([args]);
		items[0].readOnly = true;
		items[0].checked = true;
		return items;
	}
	
});
