/**节点实例列表
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.ApprovalPointsGrid',{
    extend: 'App.ux.approval.BaseGrid',
	requires: [
		'App.ux.approval.store.ApprovalPointsGridStore',
		'App.ux.approval.BaseGrid'
	],
	xtype: 'approvalPointsGrid',
	isPageGrid: false,
	newDatas: [],
	allDatas: [],
	
	initComponent: function() {
		//this.tbar = this._createTbar();
		
		this.callParent();	
	},
	
	_createTbar: function(){
		var bar;
		bar = [
			{
				xtype: 'checkbox',
				boxLabel: '显示所有节点',
				isFormField: false,
				margin:'0 10 0 10',
				name: 'showAll'
			}
			,{
				xtype: 'checkbox',
				boxLabel: '过滤分支',
				isFormField: false,
				name: 'filter'
			}
		];
		return bar ;
	},
	
	_createStore: function(){
		var me = this, 
			className,
			params = '',
			url = this.mainRunTimeUrl + 'getActivityInstanceList';
		
		if(this.defaultParams){
			params = Ext.clone(this.defaultParams);
		}
		
		var store = Ext.create('App.ux.approval.store.ApprovalPointsGridStore',{
			autoLoad : true,
			url: url,
			defaultParam: params,
			listeners: {
				load: function(th, records, successful){
							
				}
			}
		});
		if(this.defaultDatas){
			store.loadData(Ext.clone(this.defaultDatas));
		}
		return store;
	},
	
	_showNewDatas: function(){
		if(this.newDatas && this.newDatas.length > 0){
			this.getStore().loadData(this.newDatas);
		}
	},
	
	_showAllDatas: function(){
		if(this.allDatas && this.allDatas.length > 0){
			this.getStore().loadData(this.allDatas);
		}
	},
	
	_createColumns: function(){
		var columns = [
			 {xtype: 'rownumberer'}
			,{text: '节点', dataIndex: 'activityId',flex: 1, filter: true}
			,{text: '节点名称', dataIndex: 'activityName',flex: 1, filter: true}
			//,{text: '默认处理人', dataIndex: 'operatorName',flex: 1, filter: true}
			,{text: '流转方式', dataIndex: 'transitionType',flex: 1, filter: true}
			,{text: '流向', dataIndex: 'outgoing',flex: 4, filter: true}
			,{text: '节点状态', dataIndex: 'activityStatus',flex: 4, filter: {type: 'list'}}
		];
		return columns;
	}
	
});