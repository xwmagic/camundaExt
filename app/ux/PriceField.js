/**
 * 金钱输入组件
 * @author xiaowei
 * @date 2017年4月25日
 */
Ext.define('App.ux.PriceField', {
    extend: 'Ext.form.NumberField', //'Ext.form.NumberField'
    alias: 'widget.pricefield',
    hideTrigger: true,
    baseChars: '0123456789,',
    format:'0,0.0',
    setValue: function (v) {
        v = typeof v == 'number' ? v : String(v).replace(this.decimalSeparator, ".").replace(/,/g, "");
        v = Ext.util.Format.number(this.fixPrecision(String(v)), this.format);
        this.setRawValue(v);
    },
    getValue: function () {
        return (String(Ext.form.NumberField.superclass.getValue.call(this)).replace(",", ""));
    },
    fixPrecision: function (value) {
        var nan = isNaN(value);
        if (!this.allowDecimals || this.decimalPrecision == -1 || nan || !value) {
            return nan ? '' : value;
        }
        return parseFloat(value).toFixed(this.decimalPrecision);
    },
    validateValue: function (value) {
        value = String(value).replace(this.decimalSeparator, ".").replace(/,/g, "");

        if (!Ext.form.NumberField.superclass.validateValue.call(this, value)) {
            return false;
        }
        if (value.length < 1) {
            return true;
        }
        if (isNaN(value)) {
            this.markInvalid(String.format(this.nanText, value));
            return false;
        }
        var num = this.parseValue(value);
        if (num < this.minValue) {
            this.markInvalid(String.format(this.minText, this.minValue));
            return false;
        }
        if (num > this.maxValue) {
            this.markInvalid(String.format(this.maxText, this.maxValue));
            return false;
        }
        return true;
    },
    parseValue: function (value) {
        value = parseFloat(String(value).replace(this.decimalSeparator, ".").replace(/,/g, ""));
        return isNaN(value) ? '' : value;
    }
});