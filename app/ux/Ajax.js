/**
 * @author xiaowei
 * @date 2017年4月25日
 */
Ext.define('App.ux.Ajax', {
	statics:{
		request: function(configs){
            var myMask;
            if(configs.mask){
            	Ext.applyIf(configs.mask,{
                    msg    : '数据加载中...'
                });
                myMask = new Ext.LoadMask(configs.mask);
                myMask.show();
			}

			Ext.Ajax.request({
				url: configs.url ? configs.url : '',
				params: configs.params ? configs.params : '',
				method: configs.method ? configs.method : 'post',
				async: configs.async ? configs.async : true,
                defaultHeaders: configs.defaultHeaders ? configs.defaultHeaders : null,
				success: function(response, opts) {
					if(!response.responseText){
						return;
					}
					var obj = Ext.decode(response.responseText);
					if(myMask){
                        myMask.destroy();
					}
					if(obj.success || obj.pageSize){
						if(configs.success){
                            configs.success(obj);
						}
					}else{
						if(obj.errorMessage){
                            App.ux.Toast.show('提示',obj.errorMessage,'e');
						}

						if(configs.failure){
							configs.failure(obj, opts);
						}
					}
				},

				failure: function(response, opts) {
					var obj = Ext.decode(response.responseText);
                    if(myMask){
                        myMask.destroy();
                    }
                    if(obj.errorMessage) {
                        App.ux.Toast.show('提示', obj.errorMessage,'e');
                    }
					if(configs.failure){
						configs.failure(obj, opts);
					}
				}
			});
		}
	}
});
