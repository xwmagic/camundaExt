Ext.define("App.classic.view.code.Datas", {
    statics: {
        Gen:[
            {name: "id", type: "string", text: "主键ID"}
            ,{name:"tableName", type:"string", text:"数据库表"}
            ,{name:"entityName", type:"string", text:"实体名称"}
            ,{name:"moduleName", type:"string", text:"模块"}
            ,{name:"tempGroupId", type:"string", text:"配制参数"}
            ,{name:"tempGroupName", type:"string", text:"配制参数"}
            ,{name:"content", type:"string", text:"功能描述"}
            ,{name:"dataJson", type:"string", text:"json字段"}
            ,{name:"dataJsonText", type:"string", text:"json字段"}
            ,{name:"isCreateAppOrg", type:"string", text:"继承类"}
            ,{name:"created", type:"string", text:"是否已生成"}
            ,{name:"isDbSynchronization", type:"string", text:"是否同步"}
            ,{name:"updateDate", type:"string", text:"更新时间"}
            ,{name:"version", type:"int", text:"版本"}
            ,{name:"isTree", type:"string", text:""}
            ,{name:"isExport", type:"string", text:"导入导出"}
            ,{name:"tableType", type:"string", text:"表类型"}
            ,{name:"tableInfoIds", type:"string", text:"关联表id"}
            ,{name:"tableInfoNames", type:"string", text:"关联表"}
            ,{name:"treeSelName", type:"string", text:"树显示字段"}
            ,{name:"treeSelSql", type:"string", text:"树显示字段"}
        ],
        Fields:[
            {name:"name", type:"string", text:"字段名"}
            ,{name:"type", type:"string", text:"类型"}
            ,{name:"text", type:"string", text:"中文名"}
            ,{name:"length", type:"int", text:"长度"}
            ,{name:"default", type:"string", text:"默认"}
            ,{name:"pk", type:"string", text:"主键"}
            ,{name:"notEmpty", type:"string", text:"非空"}
            ,{name:"isQuery", type:"string", text:"可查询"}
            ,{name:"isExport", type:"string", text:"导入导出"}
            ,{name:"operate", type:"string", text:"查询条件"}
            ,{name:"dictTable", type:"string", text:"字典Table"}
            ,{name:"dictCode", type:"string", text:"字典Code"}
            ,{name:"dictText", type:"string", text:"字典Text"}
            ,{name:"dictSource", type:"string", text:"字典Source"}
            ,{name:"fieldValidType", type:"string", text:"验证规则"}
            ,{name:"info", type:"string", text:"注释"}
        ]
    }
});