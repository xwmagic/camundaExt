Ext.define("App.view.classic.code.gengroup.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classiccodegengroupMain',
    viewModel: 'classiccodegengroupViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classiccodegengroupSearchForm',hidden: false},
        {xtype: 'classiccodegengroupToolbar',hidden: false},
        {xtype: 'classiccodegengroupGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classiccodegengroupToolbar');
            var form = th.down('classiccodegengroupSearchForm');
            var grid = th.down('classiccodegengroupGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
