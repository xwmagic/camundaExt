Ext.define("App.view.classic.code.gengroup.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classiccodegengroupPicker',
    gridClassName:'App.view.classic.code.gengroup.GridMini',
    displayField: 'name',
    pickerWidth: 500, // 窗口宽度
    valueField:'id'
});
