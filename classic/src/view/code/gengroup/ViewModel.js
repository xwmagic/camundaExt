Ext.define("App.view.classic.code.gengroup.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classiccodegengroupViewModel',
    data: {
        pageName: 'classiccodegengroupMain',
        add: true,
        update: true,
        del: true
    }
});
