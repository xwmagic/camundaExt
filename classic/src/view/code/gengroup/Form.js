Ext.define("App.view.classic.code.gengroup.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classiccodegengroupForm',
    _mainConfs: App.view.classic.code.gengroup.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        name:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: "id", maxLength: 36, hidden: true}
            ],[
                {name:"name",maxLength:100}
            ],
            [
                {name:"groupNote",maxLength:100}
            ]
        ]
    }
});
