Ext.define("App.view.classic.code.gengroup.SearchForm", { 
    extend: "App.view.common.Form",
    xtype: 'classiccodegengroupSearchForm',
    openEnterSearch:true,
    height:45,
    _mainConfs: App.view.classic.code.gengroup.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:100}
                ,"sr"
            ]
        ]
    }
});
