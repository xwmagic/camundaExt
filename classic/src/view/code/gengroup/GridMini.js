Ext.define("App.view.classic.code.gengroup.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classiccodegengroupGridMini',
    _mainConfs: App.view.classic.code.gengroup.Datas.Main,
    url:'cgGroupCtl/pageList',//tree的查询地址
    filterName: 'name',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",hidden:true},
            {name:"name",hyperlinkMode:true,hyperlinkWindow:"classiccodegengroupWindowView"},
            {name:"createName"},
            {name:"groupNote",flex:'1'}
        ]
    }
});
