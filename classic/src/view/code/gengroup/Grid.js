Ext.define("App.view.classic.code.gengroup.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classiccodegengroupGrid',
    _mainConfs: App.view.classic.code.gengroup.Datas.Main,
    url:'cgGroupCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",hyperlinkMode:true,hyperlinkWindow:"classiccodegengroupWindowView"},
            {name:"createName"},
            {name:"groupNote",flex:'1'}
        ]
    }
});
