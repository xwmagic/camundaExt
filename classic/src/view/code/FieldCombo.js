Ext.define("App.view.classic.code.FieldCombo", {
    extend: "Ext.form.ComboBox",
    xtype: 'classiccodeFieldCombo',
    store:Ext.create('Ext.data.Store', {
        fields: ['name','text']
    }),
    displayField: 'name',
    valueField: 'name',
    dynamicAddMode:true
});
