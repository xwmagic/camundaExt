Ext.define("App.classic.view.code.GenForm", {
    extend: "App.view.common.Form",
    xtype: 'viewcodeGenForm',
    _mainConfs: App.classic.view.code.Datas.Gen,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        tableName: {allowBlank: false},
        entityName: {allowBlank: false},
        projectPack: {allowBlank: false},
        tempGroupId: {allowBlank: false},
        tempGroupName: {allowBlank: false},
        tableType: {allowBlank: false},
        moduleName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'id',hidden:true},
                {name: 'tableName',listeners:{
                    change:function (th) {
                        var form = th.up('viewcodeGenForm');
                        var name = Ext.Component.camelCase(th.getValue());
                        name = name.charAt(0).toUpperCase() + name.substring(1);
                        form.down('[name=entityName]').setValue(name);
                    }
                }},
                {name: 'entityName'},
                {name: 'moduleName'}
            ],
            [
                {name: 'content'},
                {name:'tempGroupName',xtype:'classiccodegenconfigPicker',keyName: 'tempGroupId'},
                {
                    boxLabel  : '是否为Tree',
                    xtype:'checkbox',
                    name: 'isTree',
                    flex:1,
                    listeners:{
                        change:function (th) {
                            var value = th.getValue();
                            var form = th.up('viewcodeGenForm');
                            if(value == true){
                                form.down('viewcodeGrid').getStore().loadData([
                                    {id:'Ext-pid',name:'pid',type:'string',length:36,text:'父ID',notEdit:true},
                                    {id:'Ext-layer',name:'layer',type:'int',length:11,text:'层级',notEdit:true},
                                    {id:'Ext-sort_num',name:'sort_num',type:'int',length:11,text:'排序号',notEdit:true}
                                ],true);
                                form.down('[name=treeSelName]').enable();
                            }else{
                                var store = form.down('viewcodeGrid').getStore();
                                store.remove(store.getById('Ext-pid'));
                                store.remove(store.getById('Ext-layer'));
                                store.remove(store.getById('Ext-sort_num'));
                                form.down('[name=treeSelName]').disable();
                            }
                        }
                    }
                },{
                    name:'treeSelName',
                    flex:2,
                    readOnly:true,
                    hidden:true,
                    disabled:true
                },
                {
                    name:'treeSelSql',
                    hidden:true
                }
            ],[
                {name:'tableType',xtype:'codeTableTypeCombo',flex:1,value:'SINGLE'},
                {name:'tableInfoIds',flex:2,xtype:'codeTagField',keyName:'tableInfoNames'}
            ],[
                {name: 'isCreateAppOrg',xtype:'ExtendsRadioGroup',flex:3}
                ,{name: 'isExport',xtype:'checkbox'}
            ],
            [
                {name: 'dataJsonText',xtype:'textarea',height:100,listeners:{
                    change:function (th) {
                        var value = th.getValue();
                        var datas;
                        if(value.substring(5).split('[').length > 0){
                            datas =eval(value);
                        }else {
                            datas =eval('[' + value + ']');
                        }

                        var form = th.up('viewcodeGenForm');
                        var store = form.down('viewcodeGrid').getStore();
                        if(store.getCount() > 4){
                            return;
                        }
                        store.loadData(datas);
                    }
                }}
            ],
            [
                {name: 'dataJson',xtype:'viewcodeGrid',height:470, isParam: true}
            ]
        ]
    },
    setValues:function (val) {
        this.callParent([val]);
        this.down('[name=dataJsonText]').setValue(val.dataJson);
    }
});
