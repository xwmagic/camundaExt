Ext.define("App.view.classic.code.Picker", {
    extend: "App.view.common.GridPicker",
    xtype: 'classiccodePicker',
    gridClassName:'App.classic.view.code.GenGridMini',
    editable: true,
    dynamicAddMode:true,
    displayField: 'tableName',
    valueField:'id'
});
