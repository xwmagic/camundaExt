/**
 * 验证
 */
Ext.define('App.view.code.TableTypeCombo', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.codeTableTypeCombo',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '单表',
			width: 50,
			inputValue: 'SINGLE'
		}, {
			boxLabel  : '主表',
			width: 50,
			inputValue: 'MAIN'
		}, {
			boxLabel  : '从表',
			width: 50,
			inputValue: 'INFO'
		}
	]
});

