Ext.define("App.view.classic.code.gentemp.WindowAdd", { 
    extend: "App.view.classic.code.gentemp.Window",
    alias: "widget.classiccodegentempWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'cgTemplateCtl/add'}, 'close']
});
