Ext.define("App.view.classic.code.gentemp.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classiccodegentempGridMini',
    _mainConfs: App.view.classic.code.gentemp.Datas.Main,
    url:'cgTemplateCtl/pageList',//tree的查询地址
    filterName: 'tempName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",hidden:true},
            {name:"tempName"},
            {name:"tempNote"},
            {name:"lastName"},
            {name:"tempSuffix"},
            {name:"groupTypeName"},
            {name:"codePath"},
            {name:"tempCode", flex: 1}
        ]
    }
});
