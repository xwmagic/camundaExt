Ext.define("App.view.classic.code.gentemp.SearchForm", { 
    extend: "App.view.common.Form",
    xtype: 'classiccodegentempSearchForm',
    openEnterSearch:true,
    height:45,
    _mainConfs: App.view.classic.code.gentemp.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"tempName",maxLength:100}
                , {name:"lastName",maxLength:100}
                ,{name:"groupType",maxLength:36}
                ,"sr"
            ]
        ]
    }
});
