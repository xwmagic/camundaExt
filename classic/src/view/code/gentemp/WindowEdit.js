Ext.define("App.view.classic.code.gentemp.WindowEdit", { 
    extend: "App.view.classic.code.gentemp.Window",
    alias: "widget.classiccodegentempWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'cgTemplateCtl/update'}, 'close']
});
