Ext.define("App.view.classic.code.genconfig.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classiccodegenconfigToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classiccodegenconfigWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'classiccodegenconfigWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'cgConfigCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
