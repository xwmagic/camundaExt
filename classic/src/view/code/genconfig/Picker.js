Ext.define("App.view.classic.code.genconfig.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classiccodegenconfigPicker',
    gridClassName:'App.view.classic.code.genconfig.GridMini',
    displayField: 'configName',
    valueField:'id'
});
