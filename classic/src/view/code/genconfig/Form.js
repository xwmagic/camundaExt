Ext.define("App.view.classic.code.genconfig.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classiccodegenconfigForm',
    _mainConfs: App.view.classic.code.genconfig.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        genName: {allowBlank: false}
        ,configName: {allowBlank: false}
        ,codePath: {allowBlank: false}
        ,tempName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
            ],
            [
                {name:"genName",maxLength:100}
                ,{name:"configName",maxLength:100}
                ,{name:"tempName",maxLength:100,xtype:'classiccodegengroupPicker',keyName:'tempId'}
            ],
            [
                {name:"codePath",maxLength:520}
            ],
            [
                {name:"jsPath",maxLength:520}
            ]
        ]
    }
});
