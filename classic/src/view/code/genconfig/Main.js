Ext.define("App.view.classic.code.genconfig.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classiccodegenconfigMain',
    viewModel: 'classiccodegenconfigViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classiccodegenconfigSearchForm',hidden: false},
        {xtype: 'classiccodegenconfigToolbar',hidden: false},
        {xtype: 'classiccodegenconfigGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classiccodegenconfigToolbar');
            var form = th.down('classiccodegenconfigSearchForm');
            var grid = th.down('classiccodegenconfigGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
