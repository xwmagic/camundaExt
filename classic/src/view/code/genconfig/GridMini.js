Ext.define("App.view.classic.code.genconfig.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classiccodegenconfigGridMini',
    _mainConfs: App.view.classic.code.genconfig.Datas.Main,
    url:'cgConfigCtl/pageList',//tree的查询地址
    filterName: 'configName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",hidden:true},
            {name:"genName"},
            {name:"configName"},
            {name:"codePath"},
            {name:"jsPath"}
        ]
    }
});
