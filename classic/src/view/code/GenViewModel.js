Ext.define("App.classic.view.code.GenViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.viewcodeGenViewModel',
    data: {
        pageName: 'Gen',
        'Gen-btnCreate': true,
        'Gen-btnUpdate': true,
        'Gen-btnDelete': true
    }
});
