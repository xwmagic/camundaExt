Ext.define("App.classic.view.code.GenWindowEdit", { 
    extend: "App.classic.view.code.GenWindow",
    alias: "widget.viewcodeGenWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'myGenerate/update'}, 'close']
});
