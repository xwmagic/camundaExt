/*
 * 页面ViewModel基类,实现自动抓取页面元素权限并控制相应元件
 * Created by GDC. 2016.5.25
 */
Ext.define('App.view.BaseViewModel', {
	extend : 'Ext.app.ViewModel',
	alias : 'viewmodel.baseviewmodel',
	data : { //以下属性与按钮元素进行绑定,默认为隐藏,其他有需要的页面元素也可以添加进来
		btnAdd    : true,
		btnModify : true,
		btnDel    : true,
		btnQuery  : true,
		btnSave   : true
	}
});