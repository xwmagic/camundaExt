Ext.define("App.view.classic.process.run.WindowAdd", {
    extend: "App.view.classic.process.run.Window",
    alias: "widget.classicprocessrunWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'bpmnCtl/add'}, 'close']
});
