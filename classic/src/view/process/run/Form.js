Ext.define("App.view.classic.process.run.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicprocessrunForm',
    _mainConfs: App.view.classic.process.run.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id"}
            ],
            [
                {name:"key"}
            ],
            [
                {name:"variables"}
            ]
        ]
    }
});
