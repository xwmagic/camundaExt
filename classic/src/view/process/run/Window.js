Ext.define("App.view.classic.process.run.Window", { 
    extend: "App.view.common.Window",
    alias: "widget.classicprocessrunWindow",
    width: 1200, // 窗口宽度
    layout:'fit',
    height:700,
    bodyPadding: '15px',
    maximized:true,
    items:[
        {
            xtype:'classicprocessrunForm',isParam:true, autoSetSelect: true
        }
    ],
    url:'process-definition/{id}/submit-form',
    _submit:function (btn,form) {
        var values = this.down('form').getValues();
        debugger
        $.ajax({
            url: JURLPreName+'process-definition/'+values.id+'/submit-form',
            type: 'post',
            contentType: 'application/json',
            data:JSON.stringify({
                businessKey:values.key,
                variables: {
                    submit_doc: {
                        type: Boolean,
                        value: true
                    }
                }
            }),
            success: function (data,a,response,c) {

            }
        })
    }

});
