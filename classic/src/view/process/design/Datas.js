Ext.define("App.view.classic.process.design.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'ID'}
            ,{ name:'name', type:'string', text:'流程名称'}
            ,{ name:'tenantId', type:'string', text:'租户ID'}
            ,{ name:'source', type:'string', text:'来源'}
            ,{ name:'deploymentId', type:'string', text:'部署id'}
            ,{ name:'deploymentTime', type:'datetime', text:'部署时间'}
            ,{ name:'key', type:'string', text:'流程Key'}
            ,{ name:'version', type:'string', text:'版本'}
            ,{ name:'taskName', type:'string', text:'任务名称'}
            ,{ name:'assignee', type:'string', text:'办理人'}
            ,{ name:'description', type:'string', text:'描述'}
            ,{ name:'createTime', type:'datetime', text:'创建时间'}
        ]
    }
});
