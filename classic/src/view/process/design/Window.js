Ext.define("App.view.classic.process.design.Window", { 
    extend: "App.view.common.Window",
    alias: "widget.classicprocessdesignWindow",
    width: 1200, // 窗口宽度
    height:700,
    bodyPadding: '15px',
    maximized:true,
    items:[
        {
            xtype:'box',
            flex:1,
            html:'<div class="content with-diagram" id="js-drop-zone">\n' +
            '    <div class="canvas" id="js-canvas"></div>\n' +
            '    <div class="properties-panel-parent" id="js-properties-panel"></div>\n' +
            '  </div>'
        },
        {
            xtype:'classicprocessdesignForm'
        }
    ],
    url:'deployment/create',
    _submit:function (btn,form) {
        var me = this;
        YUEOE_bpmnModeler.saveXML({ format: true }, function(err, xml) {
            var param = me.getParam(xml);
            $.ajax({
                url: JURLPreName + me.url,
                type: 'post',
                processData: false,
                contentType: false,
                data: param,
                success: function () {
                    me.close();
                    me._targetObject._refresh();
                }
            })
        });
    },
    getParam:function (xml) {
        var param = new FormData();
        var xmlJson = $.xml2json(xml);
        var name = Ext.Date.format(new Date(),"Y-m-d H:i:s")+'.bpmn';
        var bpmn = xmlJson['bpmn2:process'];
        var xm;

        if(!bpmn){
            bpmn = xmlJson['bpmn:process'];
            xm = $.json2xml(xmlJson,{rootTagName:'bpmn:definitions'});
        }else {
            xm = $.json2xml(xmlJson,{rootTagName:'bpmn2:definitions'});
        }
        /*xm = '<?xml version="1.0" encoding="UTF-8"?>' + xm;
        var xm = xml;*/
        if(bpmn['@name']){
            name = bpmn['@name'] + '.bpmn';
        }

        param.append('tenant-id', "xiaowei");
        param.append('enable-duplicate-filtering', true);
        param.append('deployment-source', 'process application');
        param.append('deployment-name', name);
        param.append('file', new Blob([xml], { type: 'text/xml' }), name);
        return param;
    }

});
