Ext.define("App.view.classic.process.design.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicprocessdesignGrid',
    openRowButtons:true,
    columnsDefault:{autoLinefeed:true,flex:1},
    _mainConfs: App.view.classic.process.design.Datas.Main,
    //url:'deployment',//grid的查询地址
    url:'deployment',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",flex:2},
            {name:"name"},
            {name:"tenantId"},
            {name:"source"},
            {name:"deploymentId"},
            {name:"deploymentTime"}
        ]
    }
});
