Ext.define("App.view.classic.process.design.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicprocessdesignMain',
    viewModel: 'classicprocessdesignViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicprocessdesignSearchForm',hidden: true},
        {xtype: 'classicprocessdesignToolbar',hidden: false},
        {xtype: 'classicprocessdesignGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicprocessdesignToolbar');
            var form = th.down('classicprocessdesignSearchForm');
            var grid = th.down('classicprocessdesignGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
            grid._toolbar = toolbar;//给grid添加工具栏对象引用
            grid.actionControlLogic();//添加选择列表数据控制操作按钮的逻辑
        }
    }
});
