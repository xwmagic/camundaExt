Ext.define("App.view.classic.process.design.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicprocessdesignToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name:'c',actionObject:'classicprocessdesignWindowAdd',text:'流程图设计'},
            {name:'u',actionObject:'classicprocessdesignWindowEdit',text:'编辑流程图'},
            {name:'d',actionUrl:'deployment/',text:'删除流程图'},
            {name:'start',actionUrl:'deployment/{id}/redeploy',text:'部署流程',handler:function () {
                this.up('commontoolbar').startProcess();
            }},
        ]
    },
    startProcess:function () {
        var params = this._targetObject._getSelectionDataIds();
        $.ajax({
            url: '/process/bpmn/projects/definition',
            type: 'post',
            data:{
                id:params.toString()
            },
            success: function () {
                App.Toast.show('','流程定义成功')
            }
        })
    },
    _delete: function (btn) {
        if (App && App.Application && App.Application.forbidRepeatClick) {
            App.Application.forbidRepeatClick(btn);
        }
        //拿到列表选中数据
        var me = btn.up('commontoolbar'),
            params = me._deleteValid(btn),
            text = btn.text,
            url = btn.actionUrl;
        if (!text) {
            text = '操作';
        }
        if (!params) {
            return;
        }

        Ext.MessageBox.confirm(text + '确认', '共选中<span style="color:red;">' + params.length + '</span>行！</br>您确认要' + text + '选中的数据吗?', function (btn) {
            if (btn == 'yes') {
                //父节判断
                $.ajax({
                    url: '/process/bpmn/rest/deployment/'+params.toString(),
                    type: 'delete',
                    success: function () {
                        me._targetObject._refresh();
                    }
                })

            }
        });

    }
});
