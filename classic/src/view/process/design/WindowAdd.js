Ext.define("App.view.classic.process.design.WindowAdd", {
    extend: "App.view.classic.process.design.Window",
    alias: "widget.classicprocessdesignWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'bpmnCtl/add'}, 'close'],
    listeners: {
        boxready: function () {
            YUEOE_drawMapLogic(YUEOE_require, YUEOE_module, YUEOE_exports, null, null, null, null, true);
            YUEOE_NewProcess();
            $("#js-drop-zone").click(function () {

            });
        }
    }
});
