Ext.define("App.view.classic.process.design.WindowEdit", { 
    extend: "App.view.classic.process.design.Window",
    alias: "widget.classicprocessdesignWindowEdit",
    openValidation:true,
    isGetChangeValue:true,
    buttons: [{name: 'submit', actionUrl: 'bpmnCtl/update'}, 'close'],
    listeners:{
        boxready:function () {
            YUEOE_drawMapLogic(YUEOE_require,YUEOE_module,YUEOE_exports,null,null,null,null,true);
            var rec = this._record;
            $.ajax({
                url: JURLPreName+'deployment/'+rec.id+'/resources/',
                type: 'get',
                processData: false,
                contentType: false,
                success: function (data) {
                    $.ajax({
                        url: JURLPreName+'deployment/'+rec.id+'/resources/'+data[0].id+'/data/',
                        type: 'get',
                        success: function (data,a,response,c) {
                            YUEOE_NewProcess();
                            YUEOE_bpmnModeler.importXML(response.responseText, function(err) {
                                if (err) {
                                    console.error(err)
                                } else {

                                }
                            })

                        }
                    })
                }
            })
        }
    },
    getParam:function (xml) {
        var param = this.callParent([xml]);
        param.append('deploy-changed-only', true);
        return param;
    }
});
