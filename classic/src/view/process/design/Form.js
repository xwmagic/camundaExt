Ext.define("App.view.classic.process.design.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicprocessdesignForm',
    _mainConfs: App.view.classic.process.design.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:100}
                ,{name:"deploymentId",maxLength:100}
                ,{name:"key",maxLength:100}
            ]
        ]
    }
});
