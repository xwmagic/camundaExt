Ext.define("App.view.classic.process.design.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicprocessdesignViewModel',
    data: {
        pageName: 'classicprocessdesignMain',
        add: false,
        update: false,
        del: false
    }
});
