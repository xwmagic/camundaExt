Ext.define("App.view.classic.process.definition.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'ID'}
            ,{ name:'name', type:'string', text:'流程名称'}
            ,{ name:'category', type:'string', text:'类型'}
            ,{ name:'resource', type:'string', text:'资源位置'}
            ,{ name:'deploymentId', type:'string', text:'部署id'}
            ,{ name:'deploymentTime', type:'datetime', text:'部署时间'}
            ,{ name:'key', type:'string', text:'流程Key'}
            ,{ name:'version', type:'string', text:'版本'}
            ,{ name:'diagram', type:'string', text:'图形'}
            ,{ name:'suspended', type:'string', text:'是否被挂起'}
            ,{ name:'description', type:'string', text:'描述'}
            ,{ name:'tenantId', type:'datetime', text:'创建人'}
        ]
    }
});
