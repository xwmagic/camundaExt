Ext.define("App.view.classic.process.definition.WindowAdd", {
    extend: "App.view.classic.process.definition.Window",
    alias: "widget.classicprocessdefinitionWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'bpmnCtl/add'}, 'close'],
    listeners: {
        boxready: function () {
            YUEOE_drawMapLogic(YUEOE_require, YUEOE_module, YUEOE_exports, null, null, null, null, true);
            YUEOE_NewProcess();
        }
    }
});
