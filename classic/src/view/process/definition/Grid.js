Ext.define("App.view.classic.process.definition.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicprocessdefinitionGrid',
    openRowButtons:true,
    columnsDefault:{autoLinefeed:true,flex:1},
    _mainConfs: App.view.classic.process.definition.Datas.Main,
    url:'process-definition',//grid的查询地址
    defaultParams:{
        latest:true
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",flex:2},
            {name:"name"},
            {name:"key"},
            {name:"resource"},
            {name:"deploymentId"},
            {name:"category"},
            {name:"suspended"},
            {name:"version"}
        ]
    }
});
