Ext.define("App.view.classic.process.definition.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicprocessdefinitionMain',
    viewModel: 'classicprocessdefinitionViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicprocessdefinitionSearchForm',hidden: true},
        {xtype: 'classicprocessdefinitionToolbar',hidden: false},
        {xtype: 'classicprocessdefinitionGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicprocessdefinitionToolbar');
            var form = th.down('classicprocessdefinitionSearchForm');
            var grid = th.down('classicprocessdefinitionGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
            grid._toolbar = toolbar;//给grid添加工具栏对象引用
            grid.actionControlLogic();//添加选择列表数据控制操作按钮的逻辑
        }
    }
});
