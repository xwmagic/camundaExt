Ext.define("App.view.classic.process.definition.WindowEdit", { 
    extend: "App.view.classic.process.definition.Window",
    alias: "widget.classicprocessdefinitionWindowEdit",
    openValidation:true,
    isGetChangeValue:true,
    buttons: [{name: 'submit', actionUrl: 'bpmnCtl/update'}, 'close'],
    listeners:{
        boxready:function () {
            YUEOE_drawMapLogic(YUEOE_require,YUEOE_module,YUEOE_exports,null,null,null,null,true);
            var rec = this._record;
            $.ajax({
                url: JURLPreName+'process-definition/'+rec.id+'/xml',
                type: 'get',
                processData: false,
                contentType: false,
                success: function (data) {
                    YUEOE_NewProcess();
                    YUEOE_bpmnModeler.importXML(data.bpmn20Xml, function(err) {
                        if (err) {
                            console.error(err)
                        } else {

                        }
                    })
                }
            })
        }
    },
    getParam:function (xml) {
        var param = this.callParent([xml]);
        param.append('deploy-changed-only', true);
        return param;
    }
});
