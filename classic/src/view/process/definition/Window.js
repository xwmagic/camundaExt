Ext.define("App.view.classic.process.definition.Window", { 
    extend: "App.view.common.Window",
    alias: "widget.classicprocessdefinitionWindow",
    width: 1200, // 窗口宽度
    layout:'fit',
    height:700,
    bodyPadding: '15px',
    maximized:true,
    items:[
        {
            xtype:'box',
            html:'<div class="content with-diagram" id="js-drop-zone">\n' +
            '    <div class="canvas" id="js-canvas"></div>\n' +
            '    <div class="properties-panel-parent" id="js-properties-panel"></div>\n' +
            '  </div>'
        }
    ],
    url:'deployment/create',
    _submit:function (btn,form) {
        var me = this;
        YUEOE_bpmnModeler.saveXML({ format: true }, function(err, xml) {
            var param = me.getParam(xml);
            $.ajax({
                url: JURLPreName + me.url,
                type: 'post',
                processData: false,
                contentType: false,
                data: param,
                success: function () {
                    me.close();
                    me._targetObject._refresh();
                }
            })
        });
    },
    getParam:function (xml) {
        var param = new FormData();
        var xmlJson = $.xml2json(xml);
        var name = Ext.Date.format(new Date(),"Y-m-d H:i:s")+'.bpmn';
        if(xmlJson['bpmn2:process']['@name']){
            name = xmlJson['bpmn2:process']['@name'] + '.bpmn';
        }
        var xm = $.json2xml(xmlJson,{rootTagName:'bpmn2:definitions'});
        xm = '<?xml version="1.0" encoding="UTF-8"?>' + xm;
        param.append('tenant-id', "1-0001");
        param.append('enable-duplicate-filtering', true);
        param.append('deployment-source', 'process application');
        param.append('deployment-name', name);
        param.append('file', new Blob([xm], { type: 'text/xml' }), name);
        return param;
    }

});
