Ext.define("App.view.classic.process.definition.Form", {
    extend: "App.view.common.Form",
    xtype: 'classicprocessdefinitionForm',
    _mainConfs: App.view.classic.process.definition.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:100}
                ,{name:"deploymentId",maxLength:100}
                ,{name:"key",maxLength:100}
            ],
            [
                {name:"id",maxLength:100}
            ],
            [
                {name:"tenantId",maxLength:100}
            ],
            [
                {name:"source",maxLength:100}
            ],
            [
                {name:"deploymentTime",maxLength:100}
            ],
            [
                {name:"version",maxLength:100}
            ],
            [
                {name:"taskName",maxLength:100}
            ],
            [
                {name:"assignee",maxLength:100}
            ],
            [
                {name:"description",maxLength:100}
            ],
            [
                {name:"createTime",maxLength:100}
            ]
        ]
    }
});
