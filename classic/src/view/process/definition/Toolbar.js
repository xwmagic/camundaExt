Ext.define("App.view.classic.process.definition.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicprocessdefinitionToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name:'c',actionObject:'classicprocessdefinitionWindowAdd',tooltip:'新增'},
            {name:'u',actionObject:'classicprocessdefinitionWindowEdit',tooltip:'编辑'},
            {name:'d',actionUrl:'deployment/',tooltip:'删除'},
            {name:'start',actionObject:'classicprocessrunWindowEdit',text:'启动一个流程',handler:function () {
                this.up('commontoolbar').startProcess(this);
            }},
        ]
    },
    startProcess:function (btn) {
        this._update(btn);
    },
    _delete: function (btn) {
        if (App && App.Application && App.Application.forbidRepeatClick) {
            App.Application.forbidRepeatClick(btn);
        }
        //拿到列表选中数据
        var me = btn.up('commontoolbar'),
            params = me._deleteValid(btn),
            text = btn.text,
            url = btn.actionUrl;
        if (!text) {
            text = '操作';
        }
        if (!params) {
            return;
        }

        Ext.MessageBox.confirm(text + '确认', '共选中<span style="color:red;">' + params.length + '</span>行！</br>您确认要' + text + '选中的数据吗?', function (btn) {
            if (btn == 'yes') {
                //父节判断
                $.ajax({
                    url: '/process/bpmn/rest/deployment/'+params.toString(),
                    type: 'delete',
                    success: function () {
                        me._targetObject._refresh();
                    }
                })

            }
        });

    }
});
