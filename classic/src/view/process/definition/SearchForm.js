Ext.define("App.view.classic.process.definition.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicprocessdefinitionSearchForm',
    _mainConfs: App.view.classic.process.definition.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:100}
                ,{name:"deploymentId",maxLength:100}
                ,{name:"key",maxLength:100}
                ,"sr"
            ]
        ]
    }
});
