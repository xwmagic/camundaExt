Ext.define("App.view.classic.process.definition.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicprocessdefinitionViewModel',
    data: {
        pageName: 'classicprocessdefinitionMain',
        add: false,
        update: false,
        del: false
    }
});
