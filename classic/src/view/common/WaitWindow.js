Ext.define('App.view.common.Waitwindow', {
    extend: 'App.view.BaseWindow',
	xtype: 'commonWaitwindow',
	closable: false,
	listeners: {
		afterrender: function(){

			var myMask = new Ext.LoadMask({
				msg    : '数据加载中...',
				target : this
			});
			this.myMask = myMask;
			myMask.show();
		},
		beforeclose: function(){
			this.myMask.destroy();
		}
	}
});