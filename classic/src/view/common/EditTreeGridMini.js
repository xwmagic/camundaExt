Ext.define('App.view.common.EditTreeGridMini', {
    extend: 'App.view.common.CRUDTreeGrid',
    xtype: 'commonEditTreeGridMini',
    _autoLoad: true,//是否自动加载
    isOneLoad:true,//一次性加载
    rowLines: true,//是否显示行线条
    rootVisible: false,
    readOnly: true,
    queryMode:'remote',
    searchMask:false,
    rownumberer: false,
    hideHeaders: true,
    isShowSearchBar: true,
    reserveScrollbar: false,
    forceNoCheckTree:true
});