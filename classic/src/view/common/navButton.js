Ext.define("App.view.rdpm.common.navButton", {
    extend: "Ext.button.Button",
    xtype: 'commonnavButton',
    height: 33,
    width: 220,
    border: false,
    style: {
        'background-color': 'transparent'
    }

});
