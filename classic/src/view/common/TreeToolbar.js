Ext.define("App.view.common.TreeToolbar", {
    extend: "App.view.common.Toolbar",
    xtype: 'commonTreeToolbar',
    _getComponentByName: function(obj){
        var button = this.callParent([obj]);

        if(button){
            button.disabled = true;
        }
        switch(obj){
            case 'lock': //锁定
                button = {
                    name: 'lock',
                    xtype: 'button',
                    tooltip: '页面锁定与解锁，锁定状态不能进行任何操作!',
                    iconCls: 'x-fa fa-lock',
                    bind: {hidden: '{lock}'},
                    disabled: false,
                    handler: function () {
                        this.up('commontoolbar')._lock(this);
                    }
                };
                break;
            case 'addChild': //添加孩子节点
                button = {
                    name: 'addChild',
                    xtype: 'button',
                    tooltip: '新增子节点',
                    iconCls: 'x-fa fa-plus-circle',
                    bind: {hidden: '{addChild}'},
                    disabled: true,
                    handler: function () {
                        this.up('commontoolbar')._createChild();
                    }
                };
                break;
            case 'save': //添加孩子节点
                button = {
                    name: 'save',xtype: 'button', tooltip: '保存修改', iconCls: 'x-fa fa-save', bind: {hidden: '{save}'},disabled:true,
                    handler: function (btn) {
                        this.up('commontoolbar')._save(btn);
                    }
                };
                break;
        }

        return button;
    },

    enableBtns: function () {
        var btns = this.query('button');
        for (var i in btns) {
            if (btns[i].name !== 'lock') {
                btns[i].enable();
            }
        }
    },
    disableBtns: function () {
        var btns = this.query('button');
        for (var i in btns) {
            if (btns[i].name !== 'lock') {
                btns[i].disable();
            }
        }
    },
    _lock: function (btn) {
        if (!this._targetObject.editStatus) {
            btn.setIconCls('x-fa fa-unlock');
            this._targetObject.editStatus = true;
            this.enableBtns();
        } else {
            btn.setIconCls('x-fa fa-lock');
            this._targetObject.editStatus = false;
            this.disableBtns();
        }
        this._targetObject._refresh();
    },
    _create: function (btn) {
        btn.up('commontoolbar')._targetObject._addRecords();
    },
    _createChild: function () {
        this._targetObject._addChildRecords();
    },
    _save: function (btn) {
        var me = this;
        var param = this._targetObject.getEncodeChangeValues();
        if (!param) {
            App.ux.Toast.show('提示', '您未做任何修改！','i');
            return;
        }
        App.ux.Ajax.request({
            url: btn.actionUrl,
            params: param,
            success: function (obj) {
                App.ux.Toast.show('提示', '保存成功！','s');
                me._targetObject._refresh();
            },
            failure: function () {
            }
        });
    },
    _delete: function (btn) {
        btn.up('commontoolbar')._targetObject._removeRecords();
    }
});
