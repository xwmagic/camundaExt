/**
 * 中国式可合并列的grid列表
 * 目前只支持纵向合并
 * @author xiaowei
 * @date 2018年7月2日
 */
Ext.define('App.view.common.ChinaGrid', {
    extend: 'App.view.common.CRUDGrid',
    xtype: 'commonchinagrid',
    rowLines: false,
    mergeColumns: [],//需要合并数据的列的序号。例如：[0,1,2,'name'] 表示每一行第1-3列和名称为name的列需要纵向合并。
    mergeL1Columns:[],//子1级合并
    mergeL2Columns:[],//子2级合并
    mergeL3Columns:[],//子3级合并
    /** mergeAllEqual属性
     * true：每一行在mergeColumns中指定的列的值与合并行的值均相等时才进行合并。
     * false：逐条逐列合并。
     */
    mergeAllEqual: true,
    sortableColumns: false,
    enableColumnHide:false,
    storeClassName:'App.view.common.ChinaGridStore',
    storeConfigs:{pageSize:5},//store的属性配置对象
    viewConfig: {
        cellTpl: [
            '<td class="{tdCls} china-grid-cell" role="{cellRole}" {tdAttr} {cellAttr:attributes}',
            ' style="width:{column.cellWidth}px;border-width:0 1px 1px 0;<tpl if="tdStyle">{tdStyle}</tpl>"',
            ' tabindex="-1" data-columnid="{[values.column.getItemId()]}">',
            '<div {unselectableAttr} class="' + Ext.baseCSSPrefix + 'grid-cell-inner {innerCls}" ',
            'style="text-align:{align};<tpl if="style">{style}</tpl>" ',
            '{cellInnerAttr:attributes}>{value}</div>',
            '</td>',
            {
                priority: 0
            }
        ]
    },
    initComponent: function () {
        var me = this;
        this.on({
            beforerender: function (th) {
                this.getStore().on('load', function (store, records) {
                    store.commitChanges();
                    me.mergeGrid(me, me.mergeColumns);
                });
            },
            beforeitemclick:function (th, record,item, index) {
                if(record.data.Group_Id){
                    if(me.checkIsSelectGroupRecord(record)){
                        me.deselectGroupRecord(record.data.Group_Id)
                    }else{
                        me.selectGroupRecord(record.data.Group_Id);
                    }
                }
            },
            beforeselect:function (th, record, index) {
                this.oldSelections = this.getSelectionModel().getSelection();
            }
        });
        this.callParent();
    },
    checkIsSelectGroupRecord:function (record) {
        var selection = this.oldSelections;
        for(var i=0; i<selection.length; i++){
            if(selection[i] == record){
                return true;
            }
        }
        return false;
    },
    selectGroupRecord:function (groupValue,groupName,bool) {
        if(!groupName){
            groupName = 'Group_Id';
        }
        var records = this.getStore().getData().items;
        var selectionModel = this.getSelectionModel();
        var array = [];
        Ext.suspendLayouts();
        for(var i = 0 ;i <records.length; i++){
            if(records[i].get(groupName) == groupValue){
                array.push(records[i]);
            }
        }
        if(bool){
            selectionModel.deselect(array);
        }else{
            selectionModel.select(array);
        }
        Ext.resumeLayouts(true);
    },
    deselectGroupRecord:function (groupValue,groupName) {
        this.selectGroupRecord(groupValue,groupName,true)
    },
    /**
     * 重写_setStore
     * @param className
     * @param configs
     * @private
     */
    _setStore:function (className) {
        var configs = Ext.apply({grid:this},this.storeConfigs);
        this.callParent([this.storeClassName,configs]);
    },

    /**
     * 在列表从远程服务器获取数据并开始加载records之前的处理方法
     * @param records
     * @returns records
     */
    beforeAddDatas:function (records) {
        //自己的处理逻辑
        return records;
    },
    getColIndexArray:function (mergeColumns) {
        var columns = this.getVisibleColumns(), colIndexArray = [], i, j;
        for(i=0; i<mergeColumns.length; i++){
            if(Ext.isString(mergeColumns[i])){
                for(j=0; j<columns.length; j++){
                    if(mergeColumns[i] == columns[j].dataIndex){
                        colIndexArray.push(j);
                    }
                }
            }else{
                colIndexArray.push(mergeColumns[i]);
            }
        }
        return colIndexArray;
    },
    /**
     * 合并Grid的数据列
     * @param grid {Ext.Grid.Panel} 需要合并的Grid
     * @param mergeColumns {Array} 需要合并列的Index(序号)数组；支持直接配置列的序号（数字）与列的字段名（字符串）的混合配置。
     * @param isAllSome {Boolean} 是否2个tr的colIndexArray必须完成一样才能进行合并。true：完成一样；false：不完全一样
     */
    mergeGrid: function (grid, mergeColumns) {

        var isAllSome = this.mergeAllEqual; // 默认为true
        var me = this, colIndexArray = this.getColIndexArray(mergeColumns);
        // var mergeChildColumns = this.getColIndexArray(mergeColumns);
        if (colIndexArray.length < 1) {
            return;
        }
        // 1.是否含有数据
        if(!grid.getView().body){
            return;
        }
        var gridView = grid.getView().body.dom;
        if (gridView == null) {
            return;
        }
        // 2.获取Grid的所有tr
        var trArray = [], colIndex, thisTr, lastTr, i, j;
        if (grid.layout.type == 'table') { // 若是table部署方式，获取的tr方式如下
            trArray = gridView.childNodes;
        } else {
            trArray = gridView.getElementsByTagName('tr');
        }

        // 3.进行合并操作
        if (isAllSome) { // 3.1 全部列合并：只有相邻tr所有指定的td的innerText值都相同才会进行合并
            this.allSomeMerge(trArray,colIndexArray,0);
        } else { // 3.2 逐个列合并：每个列在前面列合并的前提下可分别合并
            this.notAllSomeMerge(trArray,colIndexArray);
        }
    },
    allSomeMerge:function(trArray,colIndexArray,level){
        var me = this,
            isEqual = true;
        var currentArray = [];//用于保存当前相同的数据
        // 2.获取Grid的所有tr
        var colIndex, thisTr, lastTr, i, j;

        lastTr = trArray[0]; // 指向第一行
        currentArray.push(lastTr);

        // 1)遍历grid的tr，从第二个数据行开始
        for (i = 1; i < trArray.length; i++) {
            thisTr = trArray[i];
            // 2)遍历需要合并的列
            for (j = 0; j < colIndexArray.length; j++) {
                colIndex = colIndexArray[j];
                // 3)比较2个td的列是否匹配，若不匹配，就把last指向当前列
                if (
                    lastTr.childNodes[colIndex] && !thisTr.childNodes[colIndex] ||
                    !lastTr.childNodes[colIndex] && thisTr.childNodes[colIndex] ||
                    lastTr.childNodes[colIndex] && thisTr.childNodes[colIndex] &&
                    lastTr.childNodes[colIndex].innerText != thisTr.childNodes[colIndex].innerText
                ) {
                    isEqual = false;
                    j = colIndexArray.length;
                }
            }
            if (!isEqual) {
                //合并
                me.mergeAllCell(currentArray,colIndexArray,i);
                //多级合并
                this.multiLevelMerge(level,currentArray);
                //重置
                lastTr = thisTr;
                currentArray = [];
                currentArray.push(thisTr);
                isEqual = true;
                continue;
            }
            currentArray.push(thisTr);
            if (i == trArray.length - 1) {
                me.mergeAllCell(currentArray,colIndexArray,trArray.length);
                //多级合并
                this.multiLevelMerge(level,currentArray);
            }
        }
    },
    /**
     * 多级合并,最多支持3级
     * @param level
     * @param currentArray
     */
    multiLevelMerge:function(level,currentArray){
        var me = this;
        //当为根级（0级）合并时，判断是否存在1级合并，如果存在继续合并。
        if(level == 0 && me.mergeL1Columns && me.mergeL1Columns.length > 0){
            me.allSomeMerge(currentArray,me.getColIndexArray(me.mergeL1Columns),1);
        }
        //当为1级（1级）合并时，判断是否存在2级合并，如果存在继续合并。
        if(level == 1 && me.mergeL2Columns && me.mergeL2Columns.length > 0){
            me.allSomeMerge(currentArray,me.getColIndexArray(me.mergeL2Columns),2);
        }
        //当为2级（2级）合并时，判断是否存在3级合并，如果存在继续合并。
        if(level == 1 && me.mergeL3Columns && me.mergeL3Columns.length > 0){
            me.allSomeMerge(currentArray,me.getColIndexArray(me.mergeL3Columns),3);
        }
    },
    mergeAllCell:function(currentArray,colIndexArray,i){
        var colIndex,j,me = this;
        for (j = 0; j < colIndexArray.length; j++) {
            colIndex = colIndexArray[j];
            me.mergeCell(currentArray, colIndex, i);
        }
    },
    notAllSomeMerge:function(trArray,colIndexArray){
        var me = this,
            isEqual = true;
        var currentArray = [];//用于保存当前相同的数据
        // 2.获取Grid的所有tr
        var colIndex, thisTr, lastTr, i, j;

        for (j = 0; j < colIndexArray.length; j++) {
            colIndex = colIndexArray[j];
            lastTr = trArray[0]; // 指向第一行
            currentArray = [];
            currentArray.push(lastTr);

            for (i = 1; i < trArray.length; i++) {
                thisTr = trArray[i];
                if (lastTr.childNodes[colIndex] && !thisTr.childNodes[colIndex] ||
                    !lastTr.childNodes[colIndex] && thisTr.childNodes[colIndex] ||
                    lastTr.childNodes[colIndex] && thisTr.childNodes[colIndex] &&
                    lastTr.childNodes[colIndex].innerText != thisTr.childNodes[colIndex].innerText
                ) {
                    //合并内容以及设置显示的位置，并将currentArray重置
                    me.mergeCell(currentArray, colIndex, i);
                    lastTr = thisTr;
                    currentArray = [];
                    currentArray.push(thisTr);
                } else {
                    currentArray.push(thisTr);
                    if (i == trArray.length - 1) {
                        me.mergeCell(currentArray, colIndex, trArray.length);
                    }
                }

            }
        }
    },
    mergeCell: function (rows, colIndex, positionNum) {
        if (rows.length <= 1) {
            return;
        }
        var num = rows.length % 2;
        var index, i, j, hgt;
        if (num == 1) {//奇数,获取中间行号
            index = (rows.length - 1) / 2;
            for (i = 0; i < rows.length; i++) {
                if(!rows[i].childNodes[colIndex]){
                    continue;
                }
                if (i != index) {
                    rows[i].childNodes[colIndex].innerHTML = '';
                }
                if (i < rows.length - 1) {//除了最后一行，均去掉下边框
                    rows[i].childNodes[colIndex].style['border-width'] = '0 1px 0px 0';
                }

            }
        } else {//偶数，获取未行
            for (i = 0; i < rows.length; i++) {
                if(!rows[i].childNodes[colIndex]){
                    continue;
                }
                if (i != rows.length / 2) {
                    rows[i].childNodes[colIndex].innerHTML = '';
                }else {
                    rows[i].childNodes[colIndex].childNodes[0].style['bottom'] = '12px';
                }
                if (i < rows.length - 1) {//除了最后一行，均去掉下边框,并去掉内容
                    rows[i].childNodes[colIndex].style['border-width'] = '0 1px 0px 0';
                }
                /*if (Ext.isIE) {//IE浏览器

                } else {//其他浏览器
                    if (i < rows.length - 1) {//除了最后一行，均去掉下边框,并去掉内容

                        rows[i].childNodes[colIndex].innerHTML = '';
                        rows[i].childNodes[colIndex].style['border-width'] = '0 1px 0px 0';
                    } else {
                        hgt = positionNum * 24 - 30 - (rows.length / 2 - 1) * 24;
                        if (positionNum > 10) {
                            hgt = hgt + 12;
                        }
                        //设置当前内容纵向居中
                        if(rows[i].childNodes[colIndex].childNodes[0].childNodes[0].nodeType == 1){
                            //如果是html元素
                            rows[i].childNodes[colIndex].childNodes[0].innerHTML = '<span style="position:fixed;float:left;margin-left:-8px;top:' +
                                hgt + 'px;">' + rows[i].childNodes[colIndex].childNodes[0].innerHTML + '</span>';
                        }else{//如果是文本元素
                            rows[i].childNodes[colIndex].childNodes[0].innerHTML = '<span style="position:fixed;float:left;top:' +
                                hgt + 'px;">' + rows[i].childNodes[colIndex].childNodes[0].innerHTML + '</span>';
                        }

                    }
                }*/

            }
        }
    },
    getRealSelection:function(select){
        var realSelection = [], prev;
        prev = select[0];
        realSelection.push(prev);
        for(var i=1 ; i<select.length; i++){
            if(prev.get('Group_Id') != select[i].get('Group_Id')){
                prev = select[i];
                realSelection.push(select[i]);
            }
        }
        return realSelection;
    }
});