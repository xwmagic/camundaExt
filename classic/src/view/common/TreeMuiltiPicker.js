/**
 * 下拉多选grid
 * @author xiaowei
 * @date 2018年6月7日
 */
Ext.define('App.view.common.TreeMuiltiPicker', {
    extend: "Ext.form.field.Picker",
    requires: ["Ext.tree.Panel"],
    xtype:'commonTreeMuiltiPicker',
    pickerWidth: 300,
    displayField: 'text',
    valueField: 'id',
    gridClassName: '',//App.view.test.treeGrid
    _targetObject: '',//外部对象
    checkModel: 'double',//single
    onlyLeaf: true,//只能选择叶子节点
    matchFieldWidth:true,
    keyValue:'',//键值
    keyName:'',//键名
    data:{
        id:'',
        name:''
    },//存值的对象
    initComponent: function () {
        var self = this;
        Ext.apply(self, {
            fieldLabel: self.fieldLabel,
            labelWidth: self.labelWidth
        });
        self.callParent();
    },
    setValue:function (value) {
        var me = this;
        if(!this.data){
            this.data = {};
        }
        this.data.id = value;
        this.keyValue = value;
        return me.mixins.field.setValue.call(me, value);
    },
    setRawValue:function (value) {
        if(!this.data){
            this.data = {};
        }
        this.data.name = value;
        this.callParent([value]);
    },
    getValue:function () {
        if(this.data){
            return this.data.id;
        }
        return this.callParent();
    },
    getSubmitValue: function() {
        return this.processRawValue(this.getValue());
    },
    setKeyValue:function (value) {
        this.keyValue = value;
    },
    getKeyValue:function () {
        return this.keyValue;
    },
    createPicker: function () {
        var self = this;
        self.picker = Ext.create(this.gridClassName, {
            height: this.pickerWidth,
            autoScroll: true,
            floating: true,
            focusOnToFront: false,
            shadow: true,
            ownerCt: this.ownerCt
        });
        self.picker.on({
            checkchange: function (record, checked) {
                var checkModel = self.checkModel;
                if (checkModel == 'single') {
                    var root = self.picker.getRootNode();
                    root.cascadeBy(function (node) {
                        if (node.get(self.valueField) != record.get(self.valueField)) {
                            node.set('checked', false);
                        }
                    });
                    if (!self.onlyLeaf || record.get('leaf') && checked) {
                        self.setKeyValue(record.get(self.valueField));
                        self.setValue(record.get(self.displayField));
                        self.setRawValue(record.get(self.displayField));

                    } else {
                        record.set('checked', false);
                        self.setValue(''); // 显示值
                        self.setKeyValue(''); // 显示值
                    }
                    self.fireEvent('select', self, record.get(self.valueField), record);
                } else {

                    var cascade = self.cascade;

                    if (checked == true) {
                        if (cascade == 'child' || cascade == 'both') {
                            if (!record.get("leaf") && checked){
                                record.cascadeBy(function (record) {
                                    record.set('checked', true);
                                });
                            }
                        }
                        if (cascade == 'parent' || cascade == 'both') {
                            var pNode = record.parentNode;
                            for (; pNode != null; pNode = pNode.parentNode) {
                                pNode.set("checked", true);
                            }
                        }

                    } else if (checked == false) {
                        if (cascade == 'child' || cascade == 'both') {
                            if (!record.get("leaf") && !checked) record.cascadeBy(function (record) {

                                record.set('checked', false);

                            });
                        }
                    }

                    var records = self.picker.getView().getChecked(),
                        names = [],
                        values = [];
                    Ext.Array.each(records,
                        function (rec) {
                            names.push(rec.get(self.displayField));
                            values.push(rec.get(self.valueField));
                        });
                    self.setKeyValue(values.join(','));
                    self.setValue(names.join(';'));
                    self.setRawValue(names.join(';'));

                    self.fireEvent('select', self, record.get(self.valueField), record);
                }

            },
            itemclick: function (tree, record, item, index, e, options) {
                var checkModel = self.checkModel;
                if(checkModel == 'single'){
                    if (!self.onlyLeaf || self.onlyLeaf && record.get('leaf')) {
                        self.setValue(record.get(self.displayField));
                        self.setKeyValue(record.get(self.valueField));
                        self.setRawValue(record.get(self.displayField));
                    } else {
                        self.setRawValue('');
                        self.setValue('');
                        self.setKeyValue("");
                    }
                    self.collapse();
                }
            }
        });
        return self.picker;
    },
    alignPicker: function () {
        var me = this,
            picker, isAbove, aboveSfx = '-above';
        if (this.isExpanded) {
            picker = me.getPicker();
            if (me.matchFieldWidth) {
                picker.setWidth(me.bodyEl.getWidth());
            }
            if (picker.isFloating()) {
                picker.alignTo(me.inputEl, "", me.pickerOffset);
                isAbove = picker.el.getY() < me.inputEl.getY();
                me.bodyEl[isAbove ? 'addCls' : 'removeCls'](me.openCls + aboveSfx);
                picker.el[isAbove ? 'addCls' : 'removeCls'](picker.baseCls + aboveSfx);
            }
        }
    }
});