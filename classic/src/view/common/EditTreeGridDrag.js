/**
 * 可拖动编辑树
 */
Ext.define("App.view.common.EditTreeGridDrag", {
    extend: "App.view.common.EditTreeGrid",
    xtype: 'commonedittreegriddrag',
    _autoLoad: true,//是否自动加载
    isOneLoad:true,//一次性加载
    rowLines: true,//是否显示行线条
    rootVisible: false,
    viewConfig: {
        plugins: {
            ptype: 'treeviewdragdrop',
            appendOnly:false,
            allowParentInserts:true
        }
    },
    editStatus:false,
    paramName: {
        add: 'addJson', update: 'updateJson', del: 'delJson'
    },
    listeners: {
        drop: function (node, data, overModel, dropPosition, eOpts) {
            var records = data.records[0].parentNode.childNodes;
            for (var index in records) {
                records[index].set('sortNum', records[index].get('index'));
            }
        }
    },
    _removeRecords: function () {
        var selections;

        selections = this.getSelectionModel().getSelection();
        if (!selections || selections.length < 1) {
            App.ux.Toast.show('提示', '请选中需要删除的行！','i');
            return;
        }
        if(selections[0].childNodes.length > 0){
            App.ux.Toast.show('提示', '节点含有子节点不能删除！','i');
            return;
        }
        selections[0].remove();
        if (this.isRefreshRownum) {
            this.getView().refresh();
        }
    },
    isCanEdit: function (index, record) {
        //阶段不可编辑实际结束时间
        if (record.get('notEdit') === true) {
            return false;
        }
        return this.callParent(arguments);
    }

});
