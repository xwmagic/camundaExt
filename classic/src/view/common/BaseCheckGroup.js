Ext.define('App.view.common.BaseCheckGroup', {
    extend: 'Ext.form.FieldContainer',
    mixins: ['App.view.common.FieldBaseFun'],//使其具有field的特性
    xtype: 'basecheckgroup',
    comType: 'combobox',//checkboxgroup/radiogroup/combo/combobox
    layout: 'hbox',
    Empty:'',//空值映射数据对象索引
    checkObject: '',//用于扩展comType指定的对象的属性
    isInitChild: true,//是否初始化孩子对象items，当列表Grid使用该对象做数据的valueToName转换时，应该设置为false
    fieldName: '',//字段名称
    /**新的配置方式
    statics:{
        Datas:{
            1:{text:'未完成',width:80,color:'#49484b'},
            2:{text:'进行中',width:80,color:'#1b83d6'},
            3:{text:'已完成',width:80,color:'#54d624'},
            4:{text:'即将过期',width:80,color:'#cbb820'},
            5:{text:'已过期',width:80,color:'#d34817'}
        }
    },
    */
    /**旧的配置方式*/
    items: [
        /*{
            boxLabel  : '待检验',
            name      : '00',
            inputValue: '00'
        }, {
            boxLabel  : '已检验',
            name      : '02',
            inputValue: '02',
            checked   : true
        }*/
    ],
    readOnly: false,

    isFormField: false,

    submitValue: false,

    initComponent: function () {
        var me = this;
        this.initPhone();
        this._Class = Ext.getClass(this);
        this.initKeyItem();
        this._dynamicMatchData();
        if(!this._Class.Datas){
            this.items = Ext.clone(this.items);
            this.myDatas = Ext.clone(this.items);
        }
        if (this.isInitChild) {
            this.items = this._getItems();
        } else {
            this.items = [];
        }
        //必填时数据验证添加红色边框效果
        this.on('beforerender', function (fieldcontainer) {
            if ('radiogroup' === fieldcontainer.comType) {
                var radiogroup = fieldcontainer.down("radiogroup");
                radiogroup.on('validitychange', function (file, isValid, eOpts) {
                    if (!isValid) {
                        file.setStyle({borderColor: '#cf4c35', borderWidth: '1px', borderStyle: 'solid'});
                    } else {
                        file.setStyle({borderWidth: '0px'});
                    }
                });
            }
        });
        this.callParent();
        if(this.down('component')){
            this.down('component').on({
                    change: function (th, v, oldv) {
                        me.onChangeValue(th, v, oldv);
                    }
                }
            );
        }
    },
    initPhone: function () {
        if (!Ext.platformTags.desktop && this.checkObject) {//如果是移动设备
            this.checkObject.columns = 2;
        }
    },
    initKeyItem:function () {//新方式
        var clazz = this._Class;
        if(clazz.Datas){
            var items = [];
            for(var i in clazz.Datas){
                items.push(Ext.applyIf({
                    boxLabel:clazz.Datas[i].text,
                    inputValue:i
                },clazz.Datas[i]))
            }
            this.items = items;
        }

    },
    onChangeValue:function(th, v, oldv){

    },
    _dynamicMatchData: function () {
        var name = this.fieldName;
        if (!this.fieldName) {
            name = this.name;
        }

        var i, arrs = this.items, j, temp;

        if (Ext.isObject(this.value)) {
            for (i = 0; i < arrs.length; i++) {
                arrs[i].name = name;

                arrs[i].checked = false;
                temp = this.value[name];
                if (Ext.isArray(temp)) {
                    for (j = 0; j < temp.length; j++) {
                        if (arrs[i].inputValue == temp[i]) {
                            arrs[i].checked = true;
                        }
                    }
                }

            }
        } else {
            for (i = 0; i < arrs.length; i++) {
                arrs[i].name = name;
            }
        }

    },
    _getItems: function () {
        var items = []
            , item = {xtype: this.comType, vertical: true, columns: 6}
        ;

        if (this.comType == 'combo' || this.comType == 'combobox') {
            item = this._createCombo();
            item.name = this.name;
        }
        if (this.hasOwnProperty('value') && (this.comType == 'checkboxgroup' || this.comType == 'radiogroup')) {
            if (Ext.isObject(this.value)) {
                item.value = Ext.clone(this.value);
            } else {
                item.value = {};
                item.value[this.name] = [this.value];
            }
        } else {
            item.value = Ext.clone(this.value);
        }

        if (this.checkObject && Ext.isObject(this.checkObject)) {
            item = Ext.applyIf(Ext.clone(this.checkObject), item);

        }

        if (!item.hasOwnProperty('items')) {
            item.items = Ext.clone(this.items);
        }

        if (this.hasOwnProperty('allowBlank')) {
            item.allowBlank = this.allowBlank;
        }

        item.readOnly = this.readOnly;

        items.push(item);

        return items;
    },
    _createCombo: function () {

        var states = Ext.create('Ext.data.Store', {
            fields: ['inputValue', 'boxLabel'],
            data: this.items ? this.items : []
        });

        var combo = {
            xtype: 'combobox',
            store: states,
            forceSelection: true,
            flex: 1,
            queryMode: 'local',
            displayField: 'boxLabel',
            valueField: 'inputValue'
        };
        return combo;
    },
    valueToName: function (val, metaData, record) {
        if(this._Class.Datas){//新方式
            if(Ext.isEmpty(val)){
                if(Ext.isEmpty(this._Class.Datas[this.Empty])){
                    return '';
                }else {
                    return Ext.Component.getColorView(this._Class.Datas[this.Empty].text,this._Class.Datas[this.Empty].color);
                }
            }
            if(!this._Class.Datas[val]){
                return '';
            }
            return Ext.Component.getColorView(this._Class.Datas[val].text,this._Class.Datas[val].color);
        }
        var i, arrs = this.myDatas;
        for (i = 0; i < arrs.length; i++) {
            if (arrs[i].inputValue == val) {
                return arrs[i].boxLabel;
                break;
            }
        }
    },
    nameToValue: function (name, metaData, record) {
        if(this._Class.Datas){//新方式
            for(var key in this._Class.Datas){
                if(name == this._Class.Datas[key].text){
                    return key;
                }
            }
            return;
        }
        //就方式
        var i, arrs = this.myDatas;
        for (i = 0; i < arrs.length; i++) {
            if (arrs[i].boxLabel == name) {
                return arrs[i].inputValue;
                break;
            }
        }
    },
    getMyDatas: function () {
        return this.myDatas;
    },

    getValue: function () {
        var val = [];
        var item = this.items.items[0];
        val = item.getValue();
        return val;
    },

    setValue: function (val) {
        this.value = Ext.clone(val);
        var item = this.items.items[0];
        item.setValue(val);
        this.checkChange();
        return this;
    },

    setReadOnly: function (boolea) {
        var item = this.items.items[0];
        item.setReadOnly(boolea);
    },

    setAllowBlank: function (boolea) {
        var item = this.items.items[0];
        if (item.setAllowBlank) {
            item.setAllowBlank(boolea);
        } else {
            item.allowBlank = boolea;
        }
        if (this.fieldLabel && boolea != this.allowBlank) {
            if (boolea === false) {
                if (this.labelWidth) {
                    this.labelWidth = this.labelWidth + 5;
                }
                this.setFieldLabel(this.fieldLabel + '<span style="color:red;font-weight:bold;">*</span>');

            } else {
                if (this.labelWidth) {
                    this.labelWidth = this.labelWidth - 5;
                }
                var label = this.fieldLabel.split('<span')[0];
                this.setFieldLabel(label);

            }
        }
        this.allowBlank = boolea;
    }
});
