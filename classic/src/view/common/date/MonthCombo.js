/**
 * 月份选择控件
 * @author xiaowei
 * @date 2017年5月4日
 */
Ext.define('App.view.common.date.MonthCombo', {
	extend: 'Ext.form.ComboBox',
	alias: 'widget.commondateMonthCombo',
    value: 1,
    queryMode: 'local',
    displayField: 'boxLabel',
    valueField: 'inputValue',
	store: Ext.create('Ext.data.Store', {
        fields: ['boxLabel', 'inputValue'],
        data : [
            {boxLabel  : '1',inputValue: 1}
            ,{boxLabel  : '2',inputValue: 2}
            ,{boxLabel  : '3',inputValue: 3}
            ,{boxLabel  : '4',inputValue: 4}
            ,{boxLabel  : '5',inputValue: 5}
            ,{boxLabel  : '6',inputValue: 6}
            ,{boxLabel  : '7',inputValue: 7}
            ,{boxLabel  : '8',inputValue: 8}
            ,{boxLabel  : '9',inputValue: 9}
            ,{boxLabel  : '10',inputValue: 10}
            ,{boxLabel  : '11',inputValue: 11}
            ,{boxLabel  : '12',inputValue: 12}
        ]
    })
});