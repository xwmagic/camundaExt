/**
 * 年份选择控件
 * @author xiaowei
 * @date 2017年5月4日
 */
Ext.define('App.view.common.date.YearCombo', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.commondateYearCombo',
    value: new Date().getFullYear(),
    queryMode: 'local',
    displayField: 'boxLabel',
    valueField: 'inputValue',
    isShowAll:true,
    getYearDatas: function () {
        var year = new Date().getFullYear() + 1,
            i,
            years = [{boxLabel: '全部', inputValue: ''}];
        if(!this.isShowAll){
            years = [];
        }
        for (i = 2019; i < year; i++) {
            years.push({boxLabel: i, inputValue: i});
        }
        return years;
    },
    initComponent:function () {
        this.store =  Ext.create('Ext.data.Store', {
            fields: ['boxLabel', 'inputValue'],
            data: this.getYearDatas()
        });
        this.callParent();
    }
});