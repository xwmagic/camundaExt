/**
 * 月份选择控件
 * @author xiaowei
 * @date 2017年5月4日
 */
Ext.define('App.view.common.date.MonthStringCombo', {
	extend: 'Ext.form.ComboBox',
	alias: 'widget.commondateMonthStringCombo',
    value: new Date().getMonth() > 9 ? new Date().getMonth() + 1 : '0' + (new Date().getMonth() + 1),
    queryMode: 'local',
    displayField: 'boxLabel',
    valueField: 'inputValue',
	store: Ext.create('Ext.data.Store', {
        fields: ['boxLabel', 'inputValue'],
        data : [
            {boxLabel  : '1',inputValue: '01'}
            ,{boxLabel  : '2',inputValue: '02'}
            ,{boxLabel  : '3',inputValue: '03'}
            ,{boxLabel  : '4',inputValue: '04'}
            ,{boxLabel  : '5',inputValue: '05'}
            ,{boxLabel  : '6',inputValue: '06'}
            ,{boxLabel  : '7',inputValue: '07'}
            ,{boxLabel  : '8',inputValue: '08'}
            ,{boxLabel  : '9',inputValue: '09'}
            ,{boxLabel  : '10',inputValue: '10'}
            ,{boxLabel  : '11',inputValue: '11'}
            ,{boxLabel  : '12',inputValue: '12'}
        ]
    })
});