Ext.define('App.view.common.date.Line', {
    extend: 'Ext.view.View',
    xtype: 'commonDateLine',
    itemSelector: 'div.thumb-wrap',
    scrollable: 'x',
    isLast:false,
    ulMaxWidth:'',
    /*store: Ext.create('Ext.data.Store', {
     fields: [
     {name: 'year', type: 'string'},
     {name: 'phase', type: 'string'},
     {name: 'status', type: 'int'}
     ],

     data: [
     {year: '2019-01-02', phase: '阶段1', status:1},
     {year: '2019-02-02', phase: '阶段2', status:1},
     {year: '2019-03-02', phase: '阶段2', status:2},
     {year: '2019-02-02', phase: '阶段2', status:3},
     {year: '2019-02-02', phase: '阶段2', status:0},
     {year: '2019-02-02', phase: '阶段2', status:0},
     ]
     }),*/
    initComponent: function () {
        var ulClass = 'time-horizontal';
        var ulMaxWidth = '';
        if(this.isLast){
            ulClass = 'time-horizontal time-horizontal-last';
        }
        if(this.ulMaxWidth){
            ulMaxWidth = 'max-width:'+this.ulMaxWidth+'px;';
        }
        this.tpl = new Ext.XTemplate(
            '<div>',
                '<ul class="time-horizontal1">',
                    '<tpl for=".">',
                        '<li>{year}</li>',
                    '</tpl>',
                '</ul>',
            '</div>',
            '</br></br>',
            '<div>',
                '<ul class="'+ulClass+'" style="'+ulMaxWidth+'">',
                    '<tpl for=".">',
                        '<tpl if="status == 0">',//默认灰色未到达
                            '<li><b class="time-circle"></b>{phase}</li>',
                        '<tpl elseif="status == 1">',//正常完成绿色
                            '<li><b class="time-circle time-circle-green"></b>{phase}</li>',
                        '<tpl elseif="status == 2">',//阶段内部延误黄色
                            '<li><b class="time-circle time-circle-yellow"></b>{phase}</li>',
                        '<tpl else>',//里程碑延误红色 if="status = 3"
                            '<li><b class="time-circle time-circle-red"></b>{phase}</li>',
                        '</tpl>',
                    '</tpl>',
                '</ul>',
            '</div>'
        );
        this.callParent();
    }

});