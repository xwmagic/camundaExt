Ext.define("App.classic.view.common.GridSelectWindow", {
    extend: "App.view.common.Window",
    alias: "widget.commonGridSelectWindow",
    layout: {type: 'vbox', align: 'stretch'},
    width: 900,
    height: 600,
    excludeDatas: [],//选择窗口需要排除的数据，被排除后，不会显示在当前列表中
    _targetObject: '',
    formXtype: '',
    gridXtype: '',
    noRepeat:true,//true为开启选择数据过滤，数据不能重复选择。false表示数据可以重复选择
    initComponent: function () {
        var me = this;
        var items = [];
        if (this.formXtype) {
            items.push({xtype: this.formXtype});
        }
        if (this.gridXtype) {
            items.push({
                xtype: this.gridXtype,
                border: true,
                isExcludeDatas:this.noRepeat,
                excludeDatas: this.excludeDatas,
                flex: 1,
                listeners: {
                    itemdblclick: function () {
                        me.onOkClick();
                    }
                }
            });
        }
        this.items = items;
        this.callParent();
    },
    listeners: {
        beforerender: function (th) {
            var form = th.down('form');
            var grid = th.down('grid');
            if (form)
                form._targetObject = grid;//给查询框关联操作对象
        }
    },
    buttons: [{
        text: '确定', handler: function () {
            this.up('window').onOkClick();
        }
    }, {
        text: '取消',
        handler: function () {
            this.up('window').close();
        }
    }],
    onOkClick: function (isContinue) {
        var win = this;
        var winGrid = win.down('grid');
        var selections = winGrid.getSelectionModel().getSelection();
        var datas = [], temp;
        for (var i = 0; i < selections.length; i++) {
            temp = Ext.clone(selections[i].data);
            temp[winGrid.excludeFieldName] = temp[winGrid.excludeMeFieldName];
            if(!winGrid.isExcludeDatas){//如果允许重复选择，那么需要删除数据的id，否则会因为id相同，无法重复添加数据
                delete temp.id;
            }
            temp._add = true;
            datas.push(temp);
        }
        win._targetObject.setValue(datas);
        if(!isContinue){//点击确定时，是否继续操作。false将关闭窗口。true,将继续操作。
            win.close();
        }else {
            if(!Ext.platformTags.desktop) {
                win.hide();
                setTimeout(function () {
                    win.show();
                }, 500);
            }
        }

    }
});
