/**
 * 中国式可合并列的gridStore
 * 目前只支持纵向合并
 * @author xiaowei
 * @date 2018年7月2日
 */
Ext.define('App.view.common.ChinaGridStore', {
    extend: 'Ext.data.Store',
    onProxyLoad:function (operation) {
        operation._resultSet.records = this.grid.beforeAddDatas(operation._resultSet.records);
        this.callParent([operation]);
    }
});