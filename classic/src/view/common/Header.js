/**
 * @author xiaowei
 * @date 2017年5月4日
 */
Ext.define('App.view.common.Header', {
    extend: 'Ext.panel.Header',
	mixins: ['App.view.common.CommonFun'],
	xtype: 'commonheader',
	itemPosition: 1,
	title: {
		maxWidth: 1
	},
	initComponent: function(){
		this.items = this._getItems();
		this.callParent();
	},
	
	_getItems: function(){
		var items = []
			,item = {xtype: 'form',bodyStyle: {backgroundColor: 'transparent'},layout: 'hbox'}
			,i
			;
		
		item.items = this._autoLabelWidth();
			
		items.push(item);
		
		return items;
	},
	
	_autoLabelWidth: function(){
		var i, confObj, items = Ext.clone(this.items), defaltLabelWidth, defaltField, width = 200;
		for(i=0; i<items.length; i++){
			confObj = items[i];
			defaltLabelWidth = confObj.fieldLabel ? this._getByteLength(confObj.fieldLabel) * 7 + 15 : 0;
			defaltField = {
				margin: '0 0 0 10',
				xtype: 'textfield',
				labelWidth: defaltLabelWidth,
				width: defaltLabelWidth + width
			};
			if(this.ownerCt && this.ownerCt._isView){//如果父容器配置了只读属性（_isView），所有控件强制设置配只读状态
				defaltField.readOnly = true;
			}
			Ext.applyIf(confObj,defaltField);
		}
		return items;
		
	}

});
