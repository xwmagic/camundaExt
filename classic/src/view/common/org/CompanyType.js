/**
 * 公司类型
 * @author xiaowei
 * @date 2019年10月6日
 */
Ext.define('App.view.common.org.CompanyType', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.CompanyType',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '全部',
			width: 50,
			inputValue: ''
		},{
			boxLabel  : '公司',
			width: 80,
			inputValue: '0'
		}, {
			boxLabel  : '组织',
			width: 80,
			inputValue: '1'
		}, {
			boxLabel  : '个人',
			width: 80,
			inputValue: '2'
		}
	],
    valueToName: function(value,metaData, record){
        var color = '#888';
        var name = '';
        value = value + '';
        switch (value){
            case "0":
                color = '#1088b9';
                name = '公司';
                break;
            case "1":
                color = '#17d118';
                name = '组织';
                break;
            case "2":
                color = '#d1c711';
                name = '个人';
                break;
            default:
                color = '#1088b9';
                name = '公司';
                break;
        }
        return Ext.Component.getColorView(name,color);
    }

});

