/**
 * 小区
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.boolea.HouseBaseGroup', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.HouseBaseGroup',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '全部',
			width: 50,
			inputValue: ''
		},{
			boxLabel  : '天御盈品',
			width: 80,
			inputValue: '天御盈品'
		}, {
			boxLabel  : '阳光嘉苑',
			width: 80,
			inputValue: '阳光嘉苑'
		}
	]
});

