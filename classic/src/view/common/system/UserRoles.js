/**
 * 小区
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.system.UserRoles', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.UserRoles',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [

	],
    valueToName: function(values,metaData, record){
	    var flag = 0;
        var color = '#3d3c43';
        var name = '';
	    for(var i in values){
            if(flag === 0){
                flag = 1;
                color = '#46ce82';
            }else{
                flag = 0;
                color = '#88d247';
            }
            name += Ext.Component.getColorView(values[i],color) + ' ';
        }

        return name;
    }

});

