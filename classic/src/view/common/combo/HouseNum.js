/**
 * 小区楼层
 * @author liguo01
 * @date 2017年6月7日
 */
Ext.define('App.view.common.combo.HouseNum', {
    extend: 'App.view.BaseCombo',
    alias: 'widget.HouseNum',
    editable:false,
    dynamicDataUrl:'houseRecordCtl/findHouseNum',//远程请求地址
    displayField:'houseNum',
    valueField:"houseNum"
});