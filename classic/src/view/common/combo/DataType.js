/**
 * 月份选择控件
 * @author xiaowei
 * @date 2017年5月4日
 */
Ext.define('App.view.common.date.DataType', {
	extend: 'Ext.form.ComboBox',
	alias: 'widget.commonDataType',
    value: 'string',
    queryMode: 'local',
    displayField: 'boxLabel',
    valueField: 'inputValue',
	store: Ext.create('Ext.data.Store', {
        fields: ['boxLabel', 'inputValue'],
        data : [
             {boxLabel  : 'varchar',inputValue: 'string'}
            ,{boxLabel  : 'date',inputValue: 'date'}
            ,{boxLabel  : 'datetime',inputValue: 'datetime'}
            ,{boxLabel  : 'int',inputValue: 'int'}
            ,{boxLabel  : 'long',inputValue: 'long'}
            ,{boxLabel  : 'text',inputValue: 'text'}
            ,{boxLabel  : 'double',inputValue: 'double'}
        ]
    })
});