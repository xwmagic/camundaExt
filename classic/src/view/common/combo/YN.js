/**
 * 基本布尔值对象Y/N
 * @author xiaowei
 * @date 2017年5月4日
 */
Ext.define('App.view.common.boolea.YN', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.booleanYN',
	checkObject: {value: 'Y',columns: 2},
	fieldsName: '',
	items: [ 
		{
			boxLabel  : '是',
			width: 50,
			inputValue: 'Y'
		}, {
			boxLabel  : '否',
			width: 50,
			inputValue: 'N'
		}
	]
});