/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.boolea.TF', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.booleanTF',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '是',
			width: 50,
			inputValue: 'true'
		}, {
			boxLabel  : '否',
			width: 50,
			inputValue: 'false'
		}
	],
    valueToName: function(value,metaData, record){
        var color = '#888';
        var name = '';
        value = value + '';
        switch (value){
            case "true":
                color = '#43ce92';
                name = '是';
                break;
            case "false":
                color = '#888';
                name = '否';
                break;
            default:
                color = '#888';
                name = '否';
                break;
        }
        return Ext.Component.getColorView(name,color);
    }
});

