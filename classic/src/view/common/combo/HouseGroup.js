/**
 * 小区组件
 * @author liguo01
 * @date 2017年6月7日
 */
Ext.define('App.view.common.combo.HouseGroup', {
    extend: 'App.view.BaseCombo',
    alias: 'widget.HouseGroup',
    editable:false,
    dynamicDataUrl:'houseBaseDataCtl/findHouseOtherName',//远程请求地址
    displayField:'otherName'
});