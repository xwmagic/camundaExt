/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.combo.TempTypeCombo', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.commonTempTypeCombo',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
        {
            boxLabel  : 'GridJson',
            width: 150,
            inputValue: 'GridJson'
        },
		{
			boxLabel  : '实体',
			width: 150,
			inputValue: 'Entity'
		}, {
			boxLabel  : '查询返回实体',
			width: 150,
			inputValue: 'DTO'
		}, {
			boxLabel  : 'Mapper 查询接口',
			width: 150,
			inputValue: 'Mapper'
		}, {
            boxLabel  : '业务查询接口',
            width: 80,
            inputValue: 'AccessServices'
        }, {
            boxLabel  : '业务查询实现',
            width: 80,
            inputValue: 'AccessServicesImpl'
        }, {
            boxLabel  : '业务操作接口',
            width: 80,
            inputValue: 'ConfigServices'
        }, {
            boxLabel  : '业务操作实现',
            width: 80,
            inputValue: 'ConfigServicesImpl'
        }, {
            boxLabel  : '控制层',
            width: 80,
            inputValue: 'Ctl'
        }
	]
});

