/**
 * 保存状态
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.combo.SaveStatus', {
    extend: 'App.view.common.BaseCheckGroup',
    alias: 'widget.commoncomboSaveStatus',
    checkObject: {columns: 2},
    fieldsName: '',
    items: [
        {
            boxLabel: '草稿',
            width: 80,
            inputValue: 1
        }, {
            boxLabel: '已提交',
            width: 80,
            inputValue: 2
        }, {
            boxLabel: '已打印',
            width: 80,
            inputValue: 3
        }
    ],
    valueToName: function (value, metaData, record) {
        var color = '#373737';
        var name = '';
        if (value) {
            value = Number(value);
        }
        switch (value) {
            case 1:
                color = '#373737';
                name = '草稿';
                break;
            case 2:
                color = '#5cdb22';
                name = '已提交';
                break;
            case 3:
                color = '#db8e20';
                name = '已打印';
                break;
            default:
                color = '#373737';
                name = '草稿';
        }
        return Ext.Component.getColorView(name, color);
    }
});

