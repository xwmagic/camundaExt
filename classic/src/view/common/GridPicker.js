/**
 * 下拉grid
 * @author xiaowei
 * @date 2018年6月7日
 */
Ext.define('App.view.common.GridPicker', {
    extend: "Ext.form.field.Picker",
    alias: 'widget.commonGridPicker',

    gridClassName: '',//可以直接配置已经写好的grid的类名

    displayField: null,

    valueField: null,

    matchFieldWidth: false,

    store: null,

    columns: null,

    pickerWidth: 800,

    pickerHeight: 400,

    editable: false,

    keyValue:'',//键值
    keyName:'',//键名

    params: {},//默认参数

    initComponent:function () {
        if(window.innerWidth < 500){
            this.pickerWidth = 280;
        }else if(window.innerWidth < 1200){
            this.pickerWidth = window.innerWidth -400;
        }
        if(window.innerHeight < 800){
            this.pickerHeight = 250;
        }
        this.callParent();
    },
    listeners: {
        expand:function () {
            if(this.picker && this.picker.getSelectionModel){
                this.picker.getSelectionModel().deselectAll();
            }
            this.picker._refresh();
        }
    },
    /**
     * 创建Picker
     * @return {Ext.grid.Panel}
     */
    createPicker: function () {
        var me = this,
            picker = me.createComponent();

        picker.on("itemclick", me.onItemClick, me);

        // me.on("focus", me.onFocusHandler, me);
        return picker;
    },

    /**
     * 创建gridPanel,子类可以扩展返回个性化grid(比如条件查询等)
     * @return {Ext.grid.Panel}
     */
    createComponent: function () {
        var picker, me = this;
        if (this.gridClassName) {
            picker = Ext.create(this.gridClassName, {
                floating: true,
                width: this.pickerWidth,
                height: this.pickerHeight
            });
            me.store = picker.getStore();
            var proxy = me.store.getProxy();
            proxy.extraParams = {};
            proxy.extraParams[me.valueField] = me.value;
            //通过valueField字段查询displayField字段的值
            /*if (!Ext.isEmpty(me.value)) {
                me.store.load({
                    scope: this,
                    callback: function (records, operation, success) {
                        if (records.length == 1 && me.value == records[0].get(me.valueField)) {
                            me.setRawValue(records[0].get(me.displayField));
                        }
                    }
                });
            }*/

            //重置查询
            if (me.params && picker.store && picker.store.config.autoLoad) {
                setTimeout(function () {
                    proxy.extraParams = me.params;
                    me.store.load();
                }, 50);
            }
        } else {
            picker = Ext.create("Ext.grid.Panel", {
                floating: true,
                store: me.store,
                columns: me.columns,
                width: me.pickerWidth,
                height: me.pickerHeight,
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: me.store,
                    dock: 'bottom',
                    displayInfo: true
                }]
            });
        }

        picker.getStore().on('beforeload', function () {
            var proxy = this.getProxy();
            if (proxy.extraParams) {
                Ext.apply(proxy.extraParams, me.params);
            } else {
                proxy.extraParams = Ext.clone(me.params);
            }
        });

        return picker;
    },

    /**
     * 处理grid行单击事件
     */
    onItemClick: function (view, record, item, index, e, eOpts) {
        var me = this;
        me.setValue(record.get(me.displayField));
        me.setKeyValue(record.get(me.valueField));
        me.getPicker().hide();
        me.inputEl.focus();
        //触发select事件
        this.fireEvent('select', me, record.get(me.displayField), record);
        this.fireEvent('itemclick', me, record.get(me.displayField), record);
    },

    /**
     * 获得焦点弹出
     */
    onFocusHandler: function () {
        var me = this;
        if (!me.isExpanded) {
            this.expand();
            this.focus();
        }
    },

    /**
     * 设置值
     * @param {Mixed} value
     * @return {Common.picker.GridPicker} this
     */
    setValue: function (value) {
        var me = this,
            record, isContinue = true;

        if (me.value != value) {
            isContinue = me.fireEvent('beforechange', me, value, me.value);
        }
        if (!isContinue) {
            return false;
        }

        me.fireEvent('change', me, value, me.value);
        me.value = value;

        if (me.store && me.store.isLoading()) {
            //当store加载暂时不做处理
            return false;
        }

        if (value === undefined) {
            return false;
        } else {
            record = me.getPicker().getSelectionModel().getSelection()[0];
        }

        me.setRawValue(record ? record.get(me.displayField) : value);
        if(record){
            me.keyValue = me.setKeyValue(record.get(me.valueField));
        }
        return me;
    },

    /**
     * 返回field的值
     * @return {String}
     */
    getValue: function () {
        return this.value;
    },

    setKeyValue:function (value) {
        this.keyValue = value;
    },
    getKeyValue:function () {
        return this.keyValue;
    },

    /**
     * 返回提交到服务器端的值
     * @return {String}
     */
    getSubmitValue: function () {
        return this.value;
    },
    /**
     * 重写重置方法
     */
    reset: function () {
        this.callParent();
        this.setRawValue();
    },
    /**
     * 增加load方法
     * @param params
     */
    load: function (params) {
        var store
            , proxy
        ;

        store = this.getPicker().getStore();

        setTimeout(function () {
            if (params) {
                proxy = store.getProxy();
                proxy.extraParams = params;
            }
            store.load();
        }, 100);
    },
    alignPicker: function () {
        var me = this,
            picker, isAbove, aboveSfx = '-above';
        if (this.isExpanded) {
            picker = me.getPicker();
            if (me.matchFieldWidth) {
                picker.setWidth(me.bodyEl.getWidth());
            }
            if (picker.isFloating()) {
                picker.alignTo(me.inputEl, "", me.pickerOffset);
                isAbove = picker.el.getY() < me.inputEl.getY();
                me.bodyEl[isAbove ? 'addCls' : 'removeCls'](me.openCls + aboveSfx);
                picker.el[isAbove ? 'addCls' : 'removeCls'](picker.baseCls + aboveSfx);
            }
        }
    }
});