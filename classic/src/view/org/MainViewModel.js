Ext.define("App.classic.view.org.MainViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.vieworgMainViewModel',
    data: {
        pageName: 'Main',
        'Main-btnCreate': true,
        'Main-btnUpdate': true,
        'Main-btnDelete': true
    }
});
