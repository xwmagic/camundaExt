/**
 * 项目下拉组件
 * @author xiaowei
 * @date 2018年5月2日
 */
Ext.define('App.view.BaseCombo', {
	extend: 'Ext.form.ComboBox',
	alias: 'widget.BaseCombo',
    valueField:"itemName",
    displayField:'itemName',
    autoSelectFirst:false,
    params:{
        pageNum:1,
        start:0,
        pageSize:1000
    },
    autoLoad:true,
    keyName:'',//键名,一般指的是 需要存储id的字段名
    keyValue:'',//键值
    valueFieldName:'id',//
    dynamicDataUrl:'',//远程请求地址
    firstExpand:true,//是否是第一次展开，用于修复iphone下面第一次展开无法看到内容的问题
    requestMethod:'get',//请求方式
    rootProperty:'list',//返回数据剥离根名
    listeners:{
	    expand:function () {
            if(this.firstExpand){
                this.firstExpand = false;
                this.collapse();
                this.expand();
            }
        }  
    },
    initComponent : function() {
	    var me = this;
        this.store = Ext.create('Ext.data.Store',{
            fields:[this.valueField, this.displayField],
            autoLoad:this.autoLoad,
            proxy : {
                type : 'ajax',
                extraParams:Ext.clone(this.params),
                method:me.requestMethod,
                url : this.dynamicDataUrl,
                reader : {
                    type : 'json',
                    rootProperty : this.rootProperty
                }
            },
            listeners:{
                load:function (th,records) {
                    //默认将第一个值作为默认值
                    if(records && records[0] && !me.value && me.autoSelectFirst){
                        me.setValue(records[0].get(this.valueField))
                    }
                }
            }
        });
        this.on({
            select:function (th,rec) {
                if(me.keyName){
                    me.keyValue = rec.get(me.valueFieldName);
                }
            }
        });
        this.callParent();
    },
    load:function (param) {
        var store = this.getStore();
        var proxy = store.getProxy();
        if(param){
            proxy.extraParams = param;
        }
        store.load();
    },
    setKeyValue:function (value) {
        this.keyValue = value;
    },
    getKeyValue:function () {
        return this.keyValue;
    }
});

