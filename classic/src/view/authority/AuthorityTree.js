Ext.define('Admin.view.authority.AuthorityTree', {
    extend: 'Ext.tree.Panel',
    xtype: 'authoritytree',
	displayField: 'name',
	rootVisible: true,
	checkPropagation: 'down',
	isAuthority: true,
	params: '',
	initComponent: function(){
		this.setMyStore();
		this.setMyTbar();
		this.callParent();
	},
	setMyTbar: function(){
		var me = this;
		this.tbar = [
		{
			xtype: 'button',
			iconCls: 'x-fa fa-minus-square-o',
			name: 'col',
			handler: function(th){
				me.collapseAll();
				th.hide();
				me.down('button[name=exp]').show();
			}
		},
		{
			xtype: 'button',
			iconCls: 'x-fa fa-plus-square-o',
			hidden: true,
			name: 'exp',
			handler: function(th){
				me.expandAll();
				th.hide();
				me.down('button[name=col]').show();
			}
		},
		{
			xtype: 'combobox',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'id',
			editable: false,
			width: 135,
			value: '2',
			store: Ext.create('Ext.data.Store', {
				fields: ['id', 'name'],
				data : [
					{"id":"1", "name":"自动双向选择"},
					{"id":"2", "name":"自动向下选择"},
					{"id":"3", "name":"自动向上选择"},
					{"id":"4", "name":"关闭自动选择"}
				]
			}),
			listeners: {
				select: function(combo, record, eOpts){
					if(combo.getValue() == '1'){
						me.checkPropagation = 'both';
					}
					if(combo.getValue() == '2'){
						me.checkPropagation = 'down';
					}
					if(combo.getValue() == '3'){
						me.checkPropagation = 'up';
					}
					if(combo.getValue() == '4'){
						me.checkPropagation = 'none';
					}
				}
			}
		},'->']
		if(this.isAuthority){
			this.tbar.push(
				{
					xtype: 'button',
					iconCls: 'x-fa fa-angle-double-right',
					iconAlign: 'right',
					text: '移除权限',
					handler: function(){
						me.onCheckedNodesClick(true);
					}
				}
			);
		}else{
			this.tbar.push(
				{
					xtype: 'button',
					iconCls: 'x-fa fa-angle-double-left',
					text: '增加权限',
					handler: function(){
						me.onCheckedNodesClick(false);
					}
				}
			);
		}
		
		this.tbar.push({
			xtype: 'button',
			margin: '0 20 0 0',
			iconCls: 'x-fa fa-refresh',
			handler: function(){
				me.refreshDatas();
			}
		});
	},
	refreshDatas: function(){
		this.lrajax();
	},
	setMyStore: function(){
		var me = this;
		this.store = Ext.create('Ext.data.TreeStore', {
			fields: ['name', 'menuId'],
			root: {                
				expanded: true, 
				children: ''
			}
		});
		
		
		this.refreshDatas();
	},
	lrajax: function(){
		var store = this.getStore();
		if(!this.params){
			return;
		}
		LRAjax.request({
			url: this.lrAction,
			params: this.params,
			success: function(obj) {
				store.setRoot(obj.tree);
			}
		});
	},
	onCheckedNodesClick: function(flag) {
        var records = this.getView().getChecked()
            ,names = []
			,info = ''
			,ids = []
			,me = this
			,url
			;

        Ext.Array.each(records, function(rec){
            if(rec.get('type') > 2){
				names.push(rec.get('name'));
				ids.push(rec.get('menuId'));
			}
        });
		if(flag){
			info = '移除';
			url = 'ftoken/remove';
		}else{
			info = '分配';
			url = 'ftoken/add';
		}
		
		Ext.MessageBox.confirm(info + '权限', '选中<span style="color:red;">'+names.length+'</span>个！'+names.toString(),function(btn){
			if (btn == 'yes'){
				me.saveDatas(ids.toString(),url);
			}
		});
		
    },
	saveDatas: function(ids,url){
		var me = this;
		var brother;
		if(this.name == 'has'){
			brother = me.up('actionauthority').down('authoritytree[name=none]');
		}else{
			brother = me.up('actionauthority').down('authoritytree[name=has]');
		}
		
		LRAjax.request({
			url: url,
			params: {
				ids: ids,
				'role.id': me.params['role.id']
			},
			success: function(obj) {
				me.refreshDatas();
				brother.refreshDatas();
			}
		});
	}
});
