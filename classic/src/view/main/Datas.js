var Process_temp_menu_orign_data = {
    "id": "7049ef44627b4e99a68cf4c5b3f9d9a0",
    "pid": "e5a2408c70ce46d7b75549ef9b5dc599",
    "perName": "菜单",
    "perType": "child",
    "perIcon": "x-fa fa-database",
    "perUrl": "",
    "perRoutingUrl": "",
    "perBtnValid": null,
    "perEnabled": null,
    "layer": 2,
    "sortNum": 13,
    "roleId": null,
    "userId": null,
    "orgId": null,
    "expanded": true,
    "leaf": false,
    "nullFlagQuery": false,
    "children": [
    ]
};
Ext.define("App.view.classic.main.Datas", {
    statics: {
        Menu: {
            "msg": "成功","errorMessage": "成功","code": "000","hasErrors": false,"success": true,
            "data": [
                Ext.applyIf({id: Ext.id(), perName: "流程设计", perIcon: "x-fa fa-database", perRoutingUrl: "classicprocessdesignMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "部署流程", perIcon: "x-fa fa-cube", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "发起流程", perIcon: "x-fa fa-anchor", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "任务管理", perIcon: "x-fa fa-archive", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "待办任务", perIcon: "x-fa fa-bolt", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "已办任务", perIcon: "x-fa fa-bookmark", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "通知任务", perIcon: "x-fa fa-bell", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "决策定义", perIcon: "x-fa fa-cogs", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
                , Ext.applyIf({id: Ext.id(), perName: "案例定义", perIcon: "x-fa fa-diamond", perRoutingUrl: "classicprocessdefinitionMain"},Process_temp_menu_orign_data)
            ]
        }
    }
});
