Ext.define('Admin.view.main.Main', {
    extend: 'Ext.container.Viewport',
    id: 'funstars_main_home',
    requires: [
        'Ext.button.Segmented',
        'Ext.list.Tree',
        'Ext.grid.column.Date',
        // 'Ext.chart.interactions.CrossZoom',
        // 'Ext.chart.axis.Category',
        // 'Ext.chart.series.Bar3D',
        // 'Ext.chart.interactions.ItemHighlight',
        'Admin.view.user.ResetPassword',
        'Ext.grid.column.Number'
    ],

    controller: 'main',
    viewModel: 'main',

    itemId: 'mainView',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    //主页面是否启用tab显示，默认false,仅对desktop设备有用。phone设备该开关无效
    openTab:false,

    listeners: {
        render: 'onMainViewRender'
    },

    logoTitle:'流程设计引擎',

    initComponent: function () {
        //浏览器宽度决定采用何种布局
        if (window.innerWidth < 1030) {//采用手机模块化布局
            this.openTab = false;
            this.items = [
                this.getTopHeaderPhone(),
                this.getCenterPanelPhone()
            ];
        } else {//采用电脑传统布局
            this.items = [
                this.getTopHeader(),
                this.getCenterPanel()
            ];
        }

        this.callParent();
    },
    getTopHeaderPhone: function () {
        return {
            xtype: 'toolbar',
            cls: 'sencha-dash-dash-headerbar shadow',
            //background-image:url(resources/images/header.jpg);
            style: '-webkit-box-shadow:0 0 10px #666;  -moz-box-shadow: 0 0 10px #666;  box-shadow: 0 0 10px #666;',
            height: 50,
            itemId: 'headerBar',
            items: [
                {
                    xtype: 'component',
                    reference: 'senchaLogo',
                    page:'home',
                    html: '<div class="sencha-font"><img style="height: 35px;width: 35px;position: relative;top: -1px" src="resources/images/logo-bs.png"/></a><span id="_home_app_cn_name" style="top: -13px;width: 150px;position:relative;margin-left: 5px;">'+this.logoTitle+'</span> </div>',
                    listeners:{
                        click: {
                            element: 'el',
                            fn: 'homePage'
                        }
                    }
                },
                '->',
                {
                    xtype:'container',
                    cls:'nav-tree-badge nav-tree-badge-hot',
                    listeners:{
                        click:{
                            element: 'el',
                            fn: 'openHomeAlertPage'
                        }
                    }
                },
                {
                    iconCls: 'x-fa fa-key',
                    ui: 'header',
                    href: '#passwordreset',
                    hrefTarget: '_self',
                    tooltip: '修改密码'
                }, {
                    iconCls: 'x-fa fa-sign-in',
                    ui: 'header',
                    //href : '#login',
                    hrefTarget: '_self',
                    tooltip: '退出登录',
                    handler: 'logout'
                }
            ]
        };
    },
    getTopHeader: function () {
        return {
            xtype: 'toolbar',
            cls: 'sencha-dash-dash-headerbar',
            //background-image:url(resources/images/header.jpg);
            style: '-webkit-box-shadow:0 0 10px #666;  -moz-box-shadow: 0 0 10px #666;  box-shadow: 0 0 10px #666;',
            height: 50,
            itemId: 'headerBar',
            items: [{
                xtype: 'component',
                reference: 'senchaLogo',
                html: '<div class="sencha-font"><img style="height: 35px;width: 35px;position: relative;top: -1px" src="resources/images/logo-bs.png"/><span id="_home_app_cn_name" style="top: -13px;width: 80px;position:relative;margin-left: 5px;">'+this.logoTitle+'</span> </div>'
            }, {
                iconCls: 'x-fa fa-chevron-left',
                id: 'main-navigation-btn',
                style: {
                    background: 'transparent',
                    border: '0'
                },
                listeners: {
                    click: 'onToggleNavigationSize',
                    boxready: function (th) {
                        setTimeout(function () {
                            th.el.dom.style.left = '165px';
                        }, 1);

                    }
                },
                tooltip: '收起菜单'
            },
                '->',
                /*{
                    iconCls : 'x-fa fa-toggle-on',
                    ui : 'header',
                    //href : '#searchresults',
                    hrefTarget : '_self',
                    tooltip : '隐藏页签栏',
                    handler : 'tabHide'
                },
                {
                    xtype : 'image',
                    cls : 'header-right-profile-image',
                    height : 35,
                    width : 35,
                    alt : 'current user image',
                    src : 'resources/images/user-profile/2.png'
                },*/
                {
                    xtype:'container',
                    cls:'nav-tree-badge nav-tree-badge-hot',
                    listeners:{
                        click:{
                            element: 'el',
                            fn: 'openHomeAlertPage'
                        }
                    }
                },
                {
                    xtype: 'tbtext',
                    id: 'mianView-userInfo',
                    text: '',
                    iconCls: 'x-fa fa-user',
                    style: {
                        'font-size': '12px',
                        color: '#fff'
                    },
                    ui: 'header'
                },
                {
                    iconCls: 'x-fa fa-arrows-alt',
                    ui: 'header',
                    hrefTarget: '_self',
                    tooltip: '全屏显示',
                    handler: 'aboutSystem'
                },
                /*{
                    iconCls: 'x-fa fa-bell',
                    ui: 'header',
                    href: '',
                    hrefTarget: ''
                },*/
                {
                    iconCls: 'x-fa fa-key',
                    ui: 'header',
                    href: '#passwordreset',
                    hrefTarget: '_self',
                    tooltip: '修改密码'
                }, {
                    iconCls: 'x-fa fa-sign-in',
                    ui: 'header',
                    //href : '#login',
                    hrefTarget: '_self',
                    tooltip: '退出登录',
                    handler: 'logout'
                }
            ]
        };
    },
    getCenterPanelPhone:function () {
        return {
            xtype: 'maincontainerwrap',
            id: 'main-view-detail-wrap',

            reference: 'mainContainerWrap',
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                this.getMenusPhone(),
                this.getCenterPage()
            ]
        };
    },
    getCenterPanel: function () {
        return {
            xtype: 'maincontainerwrap',
            id: 'main-view-detail-wrap',

            reference: 'mainContainerWrap',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                this.getMenus(),
                this.getCenterPage()
            ]
        };
    },
    getCenterPagePhone: function () {
        return {
            xtype: 'tabpanel',
            flex: 1,
            reference: 'mainCardPanel',
            itemId: 'contentPanel',
            border: true,
            items:[
                {xtype:'mainMenuPhone'}
            ],
            tabBar: {
                hidden: this.openTab ? false : true
            },
            defaults: {
                scrollable: true
            },
            layout: {
                type: 'fit',
                anchor: '100%'
            }
        };
    },
    getMenusPhone:function () {
        return {
            xtype: 'container',
            scrollable: 'y',
            width: 0,
            border: true,
            hidden: true,
            style: {
                background: '#2c3845'
            },
            items: [
                {
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    border: true,
                    ui: 'nav',

                    id: 'main-menu-navigationTreeList',
                    store: 'NavigationTree',
                    expanderFirst: false,
                    expanderOnly: false,
                    onToggleNav: function (button, pressed) {
                        var treelist = this,
                            ct = this.lookupReference('treelistContainer');

                        treelist.setExpanderFirst(!pressed);
                        treelist.setUi(pressed ? 'nav' : null);
                        treelist.setHighlightPath(pressed);
                        ct[pressed ? 'addCls' : 'removeCls']('treelist-with-nav');

                        if (Ext.isIE8) {
                            this.repaintList(treelist);
                        }
                    },
                    repaintList: function (treelist, microMode) {
                        treelist.getStore().getRoot().cascadeBy(function (node) {
                            var item, toolElement;

                            item = treelist.getItem(node);

                            if (item && item.isTreeListItem) {
                                if (microMode) {
                                    toolElement = item.getToolElement();

                                    if (toolElement && toolElement.isVisible(true)) {
                                        toolElement.syncRepaint();
                                    }
                                }
                                else {
                                    if (item.element.isVisible(true)) {
                                        item.iconElement.syncRepaint();
                                        item.expanderElement.syncRepaint();
                                    }
                                }
                            }
                        });
                    },
                    listeners: {
                        itemclick: 'itemdblclick'
                    }
                }
            ]
        }
    },
    getCenterPage: function () {
        return {
            xtype: 'tabpanel',
            flex: 1,
            reference: 'mainCardPanel',
            itemId: 'contentPanel',
            border: true,

            tabBar: {
                hidden: this.openTab ? false : true
            },
            defaults: {
                scrollable: true
            },
            layout: {
                type: 'fit',
                anchor: '100%'
            },
            listeners:{
                tabchange:function (th,newCard,oldCard) {
                    if(newCard.down('base-panel')){
                        newCard.down('base-panel')._refresh();
                    }
                }
            }
        };
    },
    getMenus: function () {
        return {
            xtype: 'container',
            scrollable: 'y',
            width: 180,
            border: true,
            cls:'x-treelist-nav ',
            items: [
                {
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    border: true,
                    ui: 'nav',
                    id: 'main-menu-navigationTreeList',
                    store: 'NavigationTree',
                    expanderFirst: false,
                    expanderOnly: false,
                    onToggleNav: function (button, pressed) {
                        var treelist = this,
                            ct = this.lookupReference('treelistContainer');

                        treelist.setExpanderFirst(!pressed);
                        treelist.setUi(pressed ? 'nav' : null);
                        treelist.setHighlightPath(pressed);
                        ct[pressed ? 'addCls' : 'removeCls']('treelist-with-nav');

                        if (Ext.isIE8) {
                            this.repaintList(treelist);
                        }
                    },
                    repaintList: function (treelist, microMode) {
                        treelist.getStore().getRoot().cascadeBy(function (node) {
                            var item, toolElement;

                            item = treelist.getItem(node);

                            if (item && item.isTreeListItem) {
                                if (microMode) {
                                    toolElement = item.getToolElement();

                                    if (toolElement && toolElement.isVisible(true)) {
                                        toolElement.syncRepaint();
                                    }
                                }
                                else {
                                    if (item.element.isVisible(true)) {
                                        item.iconElement.syncRepaint();
                                        item.expanderElement.syncRepaint();
                                    }
                                }
                            }
                        });
                    },
                    listeners: {
                        itemclick: 'itemdblclick'
                    }
                }
            ]
        }
    }
});
