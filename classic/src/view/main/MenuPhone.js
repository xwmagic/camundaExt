Ext.define('Admin.view.main.MenuPhone', {
    extend: 'Ext.panel.Panel',
    xtype:'mainMenuPhone',

    layout: {
        type:'vbox',
        align:'stretch'
    },
    scrollable:'y',
    bodyStyle: {
        background: '#ececec'
    },
    initComponent:function () {
        this.blockMode();
        this.callParent();
    },
    //方块显示模式
    blockMode:function () {
        var root = localStorage.getItem('systemMenus');
        var items = [];
        if (root) {
            root = Ext.decode(root);
            var medules = root.children;
            var i =0,j,temp;
            if(medules){
                for(i = 0; i<medules.length; i++){
                    temp = {
                        items:[],
                        xtype:'panel',
                        title:medules[i].text,
                        page:medules[i].viewType,
                        layout:'column',
                        titleAlign:'center',
                        bodyStyle:{
                           'background-color':'#ececec'
                        },
                        /*header:{
                            title:{
                                text:medules[i].text,
                                style:{
                                    color:'#3e3f4b',
                                    fontWeight:600,
                                    'font-family':"'Microsoft YaHei', sans-serif"
                                }
                            },
                            style:{
                                'background-color':'#7b7c84'
                            }
                        },*/
                        defaults:{
                            xtype:'button',
                            // scale: 'medium',
                            style:{
                               'background-color':'#31cea5',
                                'border-color':'#31cea5',
                                'border-radius':'2px'
                            },
                            columnWidth: 0.25,
                            margin:3,
                            iconAlign: 'top',
                            arrowVisible:false,
                            handler:'buttonClick'
                        },
                        data:medules[i]
                    };
                    if(medules[i].children){
                        for(j = 0; j<medules[i].children.length; j++){
                            temp.items.push({
                                text:medules[i].children[j].text,
                                data:medules[i].children[j],
                                page:medules[i].children[j].viewType,
                                handler:'buttonClick',
                                iconCls:medules[i].children[j].iconCls
                            });
                        }
                    }
                    items.push(temp);
                }

            }
        }
        this.items = items;
    },
    //菜单显示模式
    menuMode:function () {
        var root = localStorage.getItem('systemMenus');
        var bottomToolbar = {
            xtype: 'container',
            dock: 'bottom',
            layout:'hbox',
            scrollable:'x',
            defaults:{
                xtype:'button',
                scale: 'medium',
                iconAlign: 'top',
                arrowVisible:false,
                handler:'buttonClick'
            },
            items: []
        };
        if (root) {
            root = Ext.decode(root);
            var medules = root.children;
            var i =0,j,temp;
            if(medules){
                for(i = 0; i<medules.length; i++){
                    temp = {
                        text:medules[i].text,
                        menu:[],
                        page:medules[i].viewType,
                        data:medules[i]
                    };
                    bottomToolbar.items.push(temp);
                    if(medules[i].children){
                        for(j = 0; j<medules[i].children.length; j++){
                            temp.menu.push({
                                text:medules[i].children[j].text,
                                data:medules[i].children[j],
                                page:medules[i].children[j].viewType,
                                handler:'buttonClick',
                                iconCls:medules[i].children[j].iconCls
                            });
                        }
                    }
                }

            }
        }
        this.dockedItems = [bottomToolbar];
    }

});
