Ext.define("App.classic.view.home.Main", {
    extend: 'App.ux.BasePanel',
    xtype: 'viewhomeMain',
    layout: {type: 'hbox',align: 'stretch'},
    items: [
        {
            xtype: 'viewhomeLine', flex:1
        }
    ]
});
