Ext.define("App.classic.view.company.MainForm", { 
    extend: "App.view.common.Form",
    xtype: 'viewcompanyMainForm',
    _mainConfs: App.classic.view.company.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'id',hidden:true},
                {name: 'pid',xtype:'CompanyPicker'}
            ],
            [
                {name: 'companyName'},
                {name: 'companyEn'}
            ],
            [
                {name: 'companyCode'},
                {name: 'companyType',xtype:'CompanyType'}
            ],
            [
                {name: 'logo'},
                {name: 'info'}
            ]
        ]
    }
});
