Ext.define("App.classic.view.company.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'viewcompanyMain',
    requires: [ 
        "App.classic.view.company.MainForm",
        "App.classic.view.company.MainGrid",
        "App.classic.view.company.MainTreeGrid",
        "App.classic.view.company.MainSearchForm",
        "App.classic.view.company.MainToolbar",
        "App.classic.view.company.MainViewModel",
        "App.classic.view.company.MainWindow",
        "App.classic.view.company.MainWindowAdd",
        "App.classic.view.company.MainWindowEdit"
    ],
    viewModel: 'viewcompanyMainViewModel',
    layout: {type: 'hbox',align: 'stretch'},
    items: [
        {
            xtype: 'companyMainTreeGrid', width: 300, margin: '0 10 0 0', cls: 'container-shadow',
            listeners: {
                itemclick:function (th, record, item) {
                    th.up('viewcompanyMain').treeClick(record.data);
                }
            }
        },
        {
            xtype: 'ShadowContainer',
            border: true,
            flex: 1,
            layout: {type: 'vbox', align: 'stretch'},
            items: [
                {xtype: 'viewcompanyMainSearchForm'},
                {xtype: 'viewcompanyMainToolbar'},
                {xtype: 'viewcompanyMainGrid', flex: 1}
            ]
        }

    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('viewcompanyMainToolbar');
            var form = th.down('viewcompanyMainSearchForm');
            var grid = th.down('viewcompanyMainGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    },
    treeClick:function (data) {
        var grid = this.down('viewcompanyMainGrid');
        grid._search({
            pid:data.id
        });
    }
});
