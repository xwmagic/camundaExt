var LRAuto = function (xtype, name) {

    return false;
};

Ext.define('Admin.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',

    //TODO: implement central Facebook OATH handling here

    onFaceBookLogin: function () {
        this.redirectTo('monitor', true);
    },

    onLoginButton: function () {
        var me = this;
        //1.先拿到帐号和密码
        var view = this.getView();
        //帐号和密码
        var data = view.getValues();
        var myRoot = {};
        if (Ext.isEmpty(data.userName) || Ext.isEmpty(data.password) || Ext.isEmpty(data.userName.trim())) {
            return;
        }
        LRAjax.request({
            url: 'auth/token',
            params: {
                username: data.userName.trim(),
                password: hex_md5(data.password)
            },
            failure: function (obj) {
            },
            success: function (obj) {
                localStorage.setItem("Authorization", obj.data.token);
                var store = Ext.getStore('NavigationTree');
                if (obj && obj.data && obj.data.permissions) {
                    var menu = obj.data.permissions;
                    var root = {children: []};
                    me.getMenus(menu, root.children);
                    var data = me.getRootData(root, myRoot);
                    localStorage.setItem("systemMenus", Ext.encode(data));
                    localStorage.setItem("currentUser", Ext.encode(obj.data.user));
                    document.cookie = "userId="+obj.data.user.id;
                    store.setRoot(data);
                    if(window.innerWidth < 1300){
                        me.redirectTo('viewhomeMainPhone', true);
                    }else{
                        me.redirectTo('viewhomeMain', true);
                    }

                    //me.setTreeListItemDivs();
                }
            }
        });

    },
    getMenus: function (menu, children,parent) {
        var i, temp, icon;
        /*
        通用数据结构
        var a = {
            id: "a69d8760b5f7441c9b2f6f19d46883c7"
            ,perBtnValid: null
            ,perEnabled: null
            ,perIcon: "md-settings"
            ,perName: "系统管理"
            ,perRoutingUrl: ""
            ,perType: "menu"
            ,perUrl: "00"
            ,pid: ""
			,systemPermissionList:[]
		};*/
        for (i = 0; i < menu.length; i++) {
            temp = menu[i];
            if (temp.perType === 'url') {
                continue;
            }
            if (temp.perType === 'btn' && parent) {
                parent.menuConfig.push(temp);
                continue;
            }
            children.push({
                action: temp.perUrl
                , expanded: true
                , children: []
                , iconCls: temp.perIcon
                , id: temp.id
                , leaf: false
                , menuConfig: []
                , name: temp.perName
                , parentId: temp.pid
                , status: 1
                , treeSort: "000001"
                , type: 2
                , viewType: temp.perRoutingUrl
            });
            if (temp.children && temp.children.length > 0) {
                this.getMenus(temp.children, children[children.length - 1].children,children[children.length - 1]);
            }
            if (temp.perType === 'child') {
                children[children.length - 1].leaf = true;
                children[children.length - 1].type = 3;
            }

        }
    },

    //获取TreeList的所有marginLeft不等于0像素的div，并保存到document对象的treeListItemDivs树形中
    setTreeListItemDivs: function () {
        /*var treelistItems = [];
        var divs = document.getElementsByTagName("div");
        for (var i = 0; i < divs.length; i++) {
            if (divs[i].className == "x-treelist-item-wrap") {
                if (divs[i].style.marginLeft !== '0px') {
                    treelistItems.push(divs[i]);
                }
            }
        }
        document.treeListItemDivs = treelistItems;*/
    },

    onLoginAsButton: function () {
        this.redirectTo('login', true);
    },

    onNewAccount: function () {
        this.redirectTo('register', true);
    },

    onSignupClick: function () {
        this.redirectTo('login', true);
    },

    onResetClick: function (btn) {
        var me = this;
        var form = btn.up('authdialog');
        var val = form.getForm().getValues();
        LRAjax.request({
            url: 'userCtl/changePassword',
            params: {
                userPassword: hex_md5(val.newPwd),
                oldPass: hex_md5(val.pwd)
            },

            success: function (obj) {
                if (obj.success) {
                    me.redirectTo('', true);
                }
            }
        });
    },
    onCancelClick: function () {
        Ext.util.History.back();
    },
    getRootData: function (root, myRoot) {
        var me = this;
        var name = '';
        var isSuccess = false;
        myRoot.text = root.name;
        myRoot.menuConfig = root.menuConfig;

        if (root.menuConfig && root.menuConfig.length === 1) {//长度等于1时，直接获取menuConfig数组中的对象
            myRoot.menuConfig = root.menuConfig[0];
        }


        myRoot.action = root.action;
        myRoot.leaf = root.leaf;
        myRoot.iconCls = root.iconCls;
        if (root.webcof) {//获取webcof的自定义信息
            me.getWebconf(root, myRoot);
        }

        if (root.type == 3) {
            //myRoot.operations = root.children;
            myRoot.viewType = root.viewType;
            if (myRoot.action && myRoot.viewType) {
                name = root.objectName;
                //动态创建类，如果创建成功返回true
                isSuccess = LRAuto(myRoot.viewType, name);
                if (isSuccess) {
                    myRoot.viewType = name + root.viewType;
                }
            }

        } else {

            myRoot.expanded = true;
            myRoot.selectable = false;

        }

        if (root.children != null) {
            myRoot.children = [];

            for (var i = 0; i < root.children.length; i++) {
                var node = root.children[i];
                var obj = {};
                myRoot.children.push(obj);
                me.getRootData(node, obj);
            }
        }
        return myRoot;
    },
    getWebconf: function (node, myRoot) {
        var webcof = node.webcof;
        var comma = ",";
        var arr = webcof.split(comma);
        for (var i = 0; i < arr.length; i++) {
            var obj = arr[i];
            var dataArr = obj.split(":");
            var name = dataArr[0];
            var value = dataArr[1];
            myRoot[name] = value;
        }
    }
});
