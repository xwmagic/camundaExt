Ext.define('Admin.view.authentication.Login', {
	extend: 'Admin.view.authentication.LockingWindow',
	xtype: 'login',

	requires: [
		'Admin.view.authentication.Dialog',
		'Ext.container.Container',
		'Ext.form.field.Text',
		'Ext.form.field.Checkbox',
		'Ext.button.Button'
	],

	title: {
		text: '<div style="background: transparent"><img style="height: 70px; vertical-align: middle;" src=""/><span class="effect01" style="font-size:35px; vertical-align: middle;"><img  src="resources/images/logo.png">悦动星辰开发平台</span></div>',
		hidden:true,
		height: 70
	},
	defaultFocus: 'authdialog', // Focus the Auth Form to force field focus as well

	initComponent: function() {

		var color = '#fff';
		var imgURL = 'resources/images/logo-stars.png';
		var dynamicBackground = {
            xtype:'box',
            style:'height: 0;width:0px;position:fixed !important;float:left;top:0;left: 0;background:red',
            html:'<canvas style="position:fixed;float:left;top:0;left: 0" id="canvas"></canvas>',
            listeners:{
                boxready:function () {
                    if(!Ext.isIE){
                        this.setWidth(window.innerWidth);
                        this.setHeight(window.innerHeight);
                        var canvas = document.getElementById('canvas');

                        var spaceboi = new Thpace(canvas);
                        spaceboi.start();
                        this.setHeight(0);
                    }
                }
            }
        };
		if(Ext.isIE){
            this.bodyStyle = {
                'background-image':"url('resources/images/background.jpg');"
            };
            dynamicBackground = null;
		}
		this.items = [
            dynamicBackground,
            {
                xtype:'container',
                html:'<img style="width:330px; top: -200px;left:-170px;color: '+color+';position: absolute" src="'+imgURL+'"/>',
                listeners:{
                    beforerender:function () {
                        if(window.innerWidth < 800){
                            this.html = '<img style="width:250px;top: -100px;left:-130px;color: '+color+';position: absolute" src="'+imgURL+'"/>';
                        }
                    }
                }

            },
            {
                xtype: 'authdialog',
                defaultButton : 'loginButton',
                autoComplete: true,
                bodyPadding: '10 10',
                style: '-webkit-box-shadow:0 0 10px #666;  -moz-box-shadow: 0 0 10px #666;  box-shadow: 0 0 10px #666;background-color: transparent;',
                bodyStyle:{
                    backgroundColor: 'transparent',
                    opacity: 1
                },
                header: false,
                width: 320,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },

                defaults : {
                    margin : '15 0'
                },

                items: [
                    /*{
                        xtype: 'container',
                        layout: 'hbox',
                        items: [
                            {
                                xtype:'box',flex:1
                            },
                            {
                                xtype: 'label',
                                style:'color:'+color+';font-size:20px;font-weight:600',
                                text: '登录'
                            },
                            {
                                xtype:'box',flex:1
                            }
                        ]
                    },*/
                    {
                        xtype: 'textfield',
                        name: 'userName',
                        bind: '{userName}',
                        border:false,
                        hideLabel: true,
                        fieldBodyCls:'',
                        //baseBodyCls:'',
                        style:{
                            height: '46px !important'
                        },
                        fieldStyle:{
                            background:'transparent',
                            height: '46px !important',
                            color:color
                        },
                        allowBlank : false,
                        blankText: '帐号不能为空！',
                        emptyText: '帐号'
                        /*,
                         triggers: {
                            glyphed: {
                                cls: 'trigger-glyph-noop auth-email-trigger'
                            }
                        }*/
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: true,
                        emptyText: '密码',
                        inputType: 'password',
                        style:{
                            height: '46px !important'
                        },
                        fieldStyle:{
                            background:'transparent',
                            height: '46px !important',
                            color:color
                        },
                        name: 'password',
                        bind: '{password}',
                        allowBlank : false,
                        blankText: '密码不能为空！'
                        /* ,
                        triggers: {
                            glyphed: {
                                cls: 'trigger-glyph-noop auth-password-trigger'
                            }
                        } */
                    }
                    /*,{
                        xtype: 'container',
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'checkboxfield',
                                flex : 1,
                                cls: 'form-panel-font-color rememberMeCheckbox',
                                height: 30,
                                bind: '{persist}',
                                boxLabel: '记住帐号'
                            },
                            {
                                xtype: 'box',
                                html: '<a href="#passwordreset" class="link-forgot-password"> 忘记密码 ?</a>'
                            }
                        ]
                    }*/
                    ,{
                        xtype: 'button',
                        reference: 'loginButton',
                        scale: 'large',
                        //ui: 'soft-green',
                        style:{
                            // background:'transparent'
                            opacity:0.9
                        },
                        //iconAlign: 'right',
                        //iconCls: 'x-fa fa-angle-right',
                        text: '<span style="font-weight: 600;font-size: 20px">登 录</span>',
                        //formBind: true,
                        listeners: {
                            click: 'onLoginButton'
                            //afterrender: 'onLoginButton'
                        }
                    }
                ]
            }
        ];
		this.addCls('user-login-register-container');
		var progress = Ext.get('loadHTMLprogress2017');
		var text = Ext.get('loadHTMLText2017');
		progress.hide();
		text.hide();
		this.callParent(arguments);
	}
});
