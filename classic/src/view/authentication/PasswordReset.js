Ext.define('Admin.view.authentication.PasswordReset', {
    extend: 'Admin.view.authentication.LockingWindow',
    xtype: 'passwordreset',

    requires: [
        'Admin.view.authentication.Dialog',
        'Ext.button.Button',
        'Ext.form.Label',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Text',
        'Admin.view.common.LRPassword'
    ],

    title: '修改密码',
    defaultFocus: 'authdialog',  // Focus the Auth Form to force field focus as well
	
	
    items: [
        {
            xtype: 'authdialog',
            bodyPadding: '20 20',
            width: 320,
            reference : 'authDialog',

            defaultButton : 'submitButton',
            autoComplete: true,
            cls: 'auth-dialog-register',
			style: '-webkit-box-shadow:0 0 10px #666;  -moz-box-shadow: 0 0 10px #666;  box-shadow: 0 0 10px #666;',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults : {
                margin: '10 0',
                selectOnFocus : true
            },
            items: [
                {
                    xtype: 'label',
                    cls: 'lock-screen-top-label',
					name: 'info',
                    text: '修改我的密码',
					flag: false
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
                    allowBlank : false,
                    emptyText: '旧密码',
                    name: 'pwd',
                    inputType: 'password',
					enableKeyEvents: true,
					listeners: {
						keyup: function(obj){
							var ad = obj.up('authdialog');
							var info = ad.down('label[name=info]');
							var submitBtn = ad.down('button[reference=submitButton]');
							if(obj.isValid()){
								if(!info.flag){
									submitBtn.disable();
								}else{
									submitBtn.enable();
								}
							}
						}
					}
                },
                {
                    xtype: 'lrpassword',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
					enableKeyEvents: true,
					allowBlank : false,
                    emptyText: '新密码',
                    name: 'newPwd',
					listeners: {
						keyup: function(obj){
							var newValue = obj.getValue();
							var ad = obj.up('authdialog');
							var pw = ad.down('lrpassword[name=newPwdVerify]');
							var info = ad.down('label[name=info]');
							var isSame = obj.onTheSame(obj,newValue,pw,info);
							var submitBtn = ad.down('button[reference=submitButton]');
							var pwd = ad.down('textfield[name=pwd]');
							if(!isSame){
								submitBtn.disable();
								return;
							}
							if(pwd.isValid()){
								submitBtn.enable();
							}
						}
					}
                },
                {
                    xtype: 'lrpassword',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
                    emptyText: '密码确认',
					enableKeyEvents: true,
					allowBlank : false,
                    name: 'newPwdVerify',
					listeners: {
						keyup: function(obj){
							var newValue = obj.getValue();
							var ad = obj.up('authdialog');
							var pw = ad.down('lrpassword[name=newPwd]');
							var info = ad.down('label[name=info]');
							var isSame = obj.onTheSame(obj,newValue,pw,info);
							var submitBtn = ad.down('button[reference=submitButton]');
							var pwd = ad.down('textfield[name=pwd]');
							if(!isSame){
								submitBtn.disable();
								return;
							}
							if(pwd.wasValid){
								submitBtn.enable();
							}else{
								submitBtn.disable();
							}
						}
					}
                },
                {
                    xtype: 'button',
                    scale: 'large',
                    ui: 'soft-green',
                    formBind: true,
                    reference: 'submitButton',
                    bind: false,
                    margin: '5 0',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '确认',
                    listeners: {
                        click: 'onResetClick'
                    }
                },
                {
                    xtype: 'button',
                    scale: 'large',
                    ui: 'soft-blue',
                    bind: false,
                    margin: '5 0',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '不修改,去首页',
                    listeners: {
                        click: 'onCancelClick'
                    }
                }
            ]
        }
    ]
});
