/**
 * 下拉多选组件
 * @author xiaowei
 * @date 2018年5月2日
 */
Ext.define('App.view.BaseTagField', {
	extend: 'Ext.form.field.Tag',
	alias: 'widget.BaseTagField',
    valueField:"id",
    displayField:'name',
    keyName:'',
    autoSelectFirst:false,
    queryMode: 'local',
    filterPickList: true,
    params:{
        pageNum:1,
        start:1,
        pageSize:1000
    },//默认参数
    autoLoad:true,
    dynamicDataUrl:'',//远程请求地址
    initComponent : function() {
	    var me = this;
        this.store = Ext.create('Ext.data.Store',{
            fields:[this.valueField, this.displayField],
            autoLoad:this.autoLoad,
            proxy : {
                type : 'ajax',
                extraParams:Ext.clone(this.params),
                method:'post',
                url : this.dynamicDataUrl,
                reader : {
                    type : 'json',
                    rootProperty : 'list'
                }
            },
            listeners:{
                load:function (th,records) {
                    //默认将第一个值作为默认值
                    if(records && records[0] && !me.value && me.autoSelectFirst){
                        me.setValue(records[0].get('id'))
                    }
                }
            }
        });
        this.callParent();
    },
    load:function (param) {
        var store = this.getStore();
        var proxy = store.getProxy();
        if(param){
            proxy.extraParams = param;
        }
        store.load();
    },
    listeners:{
        select:function (me,recs) {
            if(me.keyName){
                var keyValue = [];
                for(var i in recs){
                    keyValue.push(recs[i].get(me.displayField))
                }
                me.setKeyValue(keyValue);
            }
        }
    }
});

