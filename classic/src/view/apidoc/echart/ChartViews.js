Ext.define("App.classic.view.apidoc.echart.ChartViews", {
    extend: 'Ext.container.Container',
    xtype: 'apidocechartChartViews',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {
            xtype: 'panel',
            title: 'Echart展示',
            height: window.innerHeight,
            tools:[{
                itemId: 'refresh',
                type: 'refresh',
                tooltip: '刷新',
                callback: function (owner) {
                    owner.removeAll();
                    owner.setHtml('<iframe style="width: 100%;height: 100%;position: relative;top:-55px;" src="https://gallery.echartsjs.com/explore.html#sort=rank~timeframe=all~author=all"></iframe>');
                }
            }],
            html: '<iframe style="width: 100%;height: 100%;position: relative;top:-55px;" src="https://gallery.echartsjs.com/explore.html#sort=rank~timeframe=all~author=all"></iframe>'
        }
    ]
});
