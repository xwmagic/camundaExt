Ext.define("App.classic.view.apidoc.echart.ChartDemos", {
    extend: 'Ext.container.Container',
    xtype: 'apidocechartChartDemos',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {
            xtype: 'panel',
            title: 'Echart分类',
            height: window.innerHeight,
            tools:[{
                itemId: 'refresh',
                type: 'refresh',
                tooltip: '刷新',
                callback: function (owner) {
                    owner.removeAll();
                    owner.setHtml('<iframe style="width: 100%;height: 100%;position: relative;top:-55px;" src="https://www.echartsjs.com/examples/zh/index.html"></iframe>');
                }
            }],
            html: '<iframe style="width: 100%;height: 100%;top:-20px;position: relative;top:-55px;" src="https://www.echartsjs.com/examples/zh/index.html"></iframe>'
        }
    ]
});
