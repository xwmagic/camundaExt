Ext.define("App.classic.view.apidoc.echart.ChartAPI", {
    extend: 'Ext.container.Container',
    xtype: 'apidocechartChartAPI',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {
            xtype: 'panel',
            title: 'EchartAPI',
            height: window.innerHeight,
            tools:[{
                itemId: 'refresh',
                type: 'refresh',
                tooltip: '刷新',
                callback: function (owner) {
                    owner.removeAll();
                    owner.setHtml('<iframe style="width: 100%;height: 100%;position: relative;top:-55px;" src="https://www.echartsjs.com/zh/cheat-sheet.html"></iframe>');
                }
            }],
            html: '<iframe style="width: 100%;height: 100%;position: relative;top:-55px;" src="https://www.echartsjs.com/zh/cheat-sheet.html"></iframe>'
        }
    ]
});
