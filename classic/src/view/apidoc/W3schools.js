Ext.define("App.classic.view.apidoc.W3schools", {
    extend: 'Ext.container.Container',
    xtype: 'apidocW3schools',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {
            xtype: 'panel',
            title: 'W3schools',
            height: window.innerHeight,
            tools:[{
                itemId: 'refresh',
                type: 'refresh',
                tooltip: '刷新',
                callback: function (owner) {
                    owner.removeAll();
                    owner.setHtml('<iframe style="width: 100%;height: 100%;position: relative;top:-70px;" src="https://www.quanzhanketang.com/"></iframe>');
                }
            }],
            html: '<iframe style="width: 100%;height: 100%;position: relative;top:-70px;" src="https://www.quanzhanketang.com/"></iframe>'
        }
    ]
});
