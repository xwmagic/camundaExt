Ext.define("App.classic.view.system.user.Main", {
    extend: 'App.ux.BasePanel',
    xtype: 'viewsystemuserMain',
    requires: [
        "App.classic.view.system.user.MainForm",
        "App.classic.view.system.user.MainGrid",
        "App.classic.view.system.user.MainSearchForm",
        "App.classic.view.system.user.MainToolbar",
        "App.classic.view.system.user.MainViewModel",
        "App.classic.view.system.user.MainWindow",
        "App.classic.view.system.user.MainWindowAdd",
        "App.classic.view.system.user.MainWindowEdit"
    ],
    viewModel: 'viewsystemuserMainViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'viewsystemuserMainSearchForm'},
        {xtype: 'viewsystemuserMainToolbar'},
        {xtype: 'viewsystemuserMainGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('viewsystemuserMainToolbar');
            var form = th.down('viewsystemuserMainSearchForm');
            var grid = th.down('viewsystemuserMainGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
            grid._commonToolbar = toolbar;
        }
    }
});
