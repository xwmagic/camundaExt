Ext.define("App.classic.view.system.user.MainSearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'viewsystemuserMainSearchForm',
    _mainConfs: App.classic.view.system.user.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'userName'}, //配置需要查询的字段名
                {name: 'userRealName'}, //配置需要查询的字段名
                {name: 'orgName',xtype:'classicsystemorgPicker',width:340}, //配置需要查询的字段名
                'sr' //s代表查询按钮，r代表重置按钮
            ]
        ]
    }
});
