Ext.define("App.classic.view.system.user.Datas", {
    statics: {
        Main:[
            {name: "id", type: "string", text: "主键ID"}
            ,{name:"userName", type:"string", text:"账号"}
            ,{name:"userPassword", type:"string", text:"密码"}
            ,{name:"roleName", type:"string", text:"角色"}
            ,{name:"userPasswordConfirm", type:"string", text:"密码确认"}
            ,{name:"userEnabled", type:"string", text:"是否可用"}
            ,{name:"isLock", type:"string", text:"是否锁定"}
            ,{name:"userAddTime", type:"string", text:"添加用户时间"}
            ,{name:"userRealName", type:"string", text:"姓名"}
            ,{name:"userPhone", type:"string", text:"电话"}
            ,{name:"userImage", type:"string", text:"头像"}
            ,{name:"userEmail", type:"string", text:"邮箱"}
            ,{name:"companyId", type:"string", text:"公司ID"}
            ,{name:"companyName", type:"string", text:"公司名称"}
            ,{name:"orgId", type:"string", text:"组织"}
            ,{name:"orgName", type:"string", text:"组织名称"}
            ,{name:"roleIds", type:"string", text:"角色"}
        ]
    }
});