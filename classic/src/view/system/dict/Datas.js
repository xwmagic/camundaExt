Ext.define("App.view.classic.system.dict.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'id'}
            ,{ name:'name', type:'string', text:'名称'}
            ,{ name:'code', type:'string', text:'代码'}
            ,{ name:'pid', type:'string', text:'父ID'}
            ,{ name:'layer', type:'int', text:'层级'}
            ,{ name:'sortNum', type:'int', text:'排序号'}
            ,{ name:'appId', type:'string', text:'系统id'}
            ,{ name:'appName', type:'string', text:'APP名称'}
            ,{ name:'appCode', type:'string', text:'APP编码'}
            ,{ name:'orgCode', type:'string', text:'所属组织代码'}
            ,{ name:'companyId', type:'string', text:'所属公司'}
            ,{ name:'companyName', type:'string', text:'所属公司'}
            ,{ name:'companyCode', type:'string', text:'所属公司代码'}
            ,{ name:'orgId', type:'string', text:'所属组织'}
            ,{ name:'orgName', type:'string', text:'所属组织'}
        ]
    }
});
