Ext.define("App.view.classic.system.dict.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemdictViewModel',
    data: {
        pageName: 'classicsystemdictMain',
        lock: true,
        add: true,
        addChild: true,
        save: true,
        del: true
    }
});
