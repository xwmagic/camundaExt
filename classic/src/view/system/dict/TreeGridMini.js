Ext.define("App.view.classic.system.dict.TreeGridMini", { 
    extend: "App.view.common.EditTreeGridMini",
    xtype: 'classicsystemdictTreeGridMini',
    _mainConfs: App.view.classic.system.dict.Datas.Main,
    url:'sysDictCtl/findTree',//tree的查询地址
    filterName: 'name',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",xtype:"treecolumn",flex:1}
        ]
    }
});
