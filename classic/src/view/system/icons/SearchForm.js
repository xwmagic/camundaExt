Ext.define("App.view.classic.system.icons.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemiconsSearchForm',
    _mainConfs: App.view.classic.system.icons.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"iconName",maxLength:50,xtype:'classicsystemiconsPickerTab',width:350},
                {name:"iconTag",maxLength:20}
                ,"sr"
            ]
        ]
    }
});
