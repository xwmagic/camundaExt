Ext.define("App.view.classic.system.icons.GridMiniTab", {
    extend: 'Ext.tab.Panel',
    xtype: 'classicsystemiconsGridMiniTab',
    layout:'fit',
    columnsNumber:10,
    _refresh:function () {

    },
    initComponent:function () {
        this.items =  [
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'Web',defaultParams:{iconTag:'Web'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'文本编辑',defaultParams:{iconTag:'文本编辑'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'图表',defaultParams:{iconTag:'图表'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'可旋转',defaultParams:{iconTag:'可旋转'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'性别',defaultParams:{iconTag:'性别'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'手势',defaultParams:{iconTag:'手势'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'指示方向',defaultParams:{iconTag:'指示方向'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'支付',defaultParams:{iconTag:'支付'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'文件类型',defaultParams:{iconTag:'文件类型'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'标志',defaultParams:{iconTag:'标志'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'表单',defaultParams:{iconTag:'表单'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'视频播放',defaultParams:{iconTag:'视频播放'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'货币',defaultParams:{iconTag:'货币'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'辅助功能',defaultParams:{iconTag:'辅助功能'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'运输',defaultParams:{iconTag:'运输'}},
            {xtype: 'classicsystemiconsGridMini',columnsNumber:this.columnsNumber,title:'医疗',defaultParams:{iconTag:'医疗'}}
        ];
        this.callParent();
    },
    /**
     * 传入的me对象的方法与当前grid的cellClick事件绑定
     * 为每个grid添加cellclick事件，每次click都会调用传入的me对象的onCellClick方法
     * @param me
     */
    addCellClick:function (me) {
        var grids = this.query('classicsystemiconsGridMini');
        for(var i in grids){
            grids[i].on("cellClick", me.onCellClick, me);
        }
    },
    listeners:{
        tabchange:function (tabPanel, newCard, oldCard) {
            newCard._refresh();
        }
    }
});
