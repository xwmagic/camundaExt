Ext.define("App.view.classic.system.icons.Grid", {
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicsystemiconsGrid',
    _mainConfs: App.view.classic.system.icons.Datas.Main,
    url:'sysIconsCtl/pageList',//grid的查询地址
    columnsDefault:{
        align:'center'
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"iconName",flex:1,renderer:function (v,re) {
                return '<i class="'+v+'" aria-hidden="true"></i>';
            }},
            {name:"iconName",flex:1},
            {name:"iconType",flex:1},
            {name:"iconTag",flex:1}
        ]
    }
});
