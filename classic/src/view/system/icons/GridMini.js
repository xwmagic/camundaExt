Ext.define("App.view.classic.system.icons.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicsystemiconsGridMini',
    isPageGrid:false,
    hideHeaders: true,
    showRowNum:true,
    pageSize:10000,
    /*_mainConfs: [
        { name:'1', type:'string', text:'图标'}
        ,{ name:'2', type:'string', text:'图标'}
        ,{ name:'3', type:'string', text:'图标'}
        ,{ name:'4', type:'string', text:'图标'}
        ,{ name:'5', type:'string', text:'图标'}
        ,{ name:'6', type:'string', text:'图标'}
        ,{ name:'7', type:'string', text:'图标'}
        ,{ name:'8', type:'string', text:'图标'}
        ,{ name:'9', type:'string', text:'图标'}
        ,{ name:'10', type:'string', text:'图标'}
        ,{ name:'11', type:'string', text:'图标'}
        ,{ name:'12', type:'string', text:'图标'}
        ,{ name:'13', type:'string', text:'图标'}
        ,{ name:'14', type:'string', text:'图标'}
        ,{ name:'15', type:'string', text:'图标'}
        ,{ name:'16', type:'string', text:'图标'}
        ,{ name:'19', type:'string', text:'图标'}
        ,{ name:'20', type:'string', text:'图标'}
    ],*/
    url:'sysIconsCtl/pageList',//tree的查询地址
    filterName: 'iconName',
    columnsDefault:{
        align:'center'
    },
    columnsNumber:10,//列数
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"iconName",flex:1,renderer:function (v,re) {
                return '<i class="'+v+'" aria-hidden="true"></i>';
            }},
            {name:"iconTag",flex:1}
        ]
    },
    initComponent:function () {
        var _mainConfs = [],columns = [];
        for(var i=1; i<=this.columnsNumber; i++){
            _mainConfs.push({ name:i, type:'string', text:'图标'});
            columns.push({name:i,minWidth:50,flex:1,renderer:function (v,re) {
                return '<i class="'+v+'" aria-hidden="true"></i>';
            }})
        }
        if(this.showRowNum){
            columns.push({
                text: '...',
                xtype: 'rownumberer',
                width: 40
            });
        }
        this._mainConfs = _mainConfs;
        this._confs.columns = columns;
        this.callParent();
    },
    beforeAddDatas:function (records) {
        var j = 1,temp = {},res = [], number = 0;
        for(var i=0; i<records.length; i++){
            temp[j] = records[i].data.iconName;
            if(i === records.length -1){
                records[number].data = temp;
                res.push(records[number]);
                break;
            }
            if(j===this.columnsNumber){
                records[number].data = temp;
                res.push(records[number]);
                temp = {};
                j = 1;
                number ++ ;
                continue;
            }
            j++;
        }
        //自己的处理逻辑
        return res;
    }
});
