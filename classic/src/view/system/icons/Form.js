Ext.define("App.view.classic.system.icons.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicsystemiconsForm',
    _mainConfs: App.view.classic.system.icons.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true},
                {name:"iconName",maxLength:50}
            ],
            [
                {name:"iconType",maxLength:50}
            ],
            [
                {name:"iconTag",maxLength:20}
            ]
        ]
    }
});
