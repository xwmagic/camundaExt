Ext.define("App.view.classic.system.icons.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemiconsMain',
    requires: [ 
        "App.view.classic.system.icons.Form",
        "App.view.classic.system.icons.Grid",
        "App.view.classic.system.icons.SearchForm",
        "App.view.classic.system.icons.Toolbar",
        "App.view.classic.system.icons.ViewModel",
        "App.view.classic.system.icons.Window",
        "App.view.classic.system.icons.WindowAdd",
        "App.view.classic.system.icons.WindowEdit"
    ],
    viewModel: 'classicsystemiconsViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemiconsSearchForm',hidden: false},
        {xtype: 'classicsystemiconsToolbar',hidden: false},
        {xtype: 'classicsystemiconsGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemiconsToolbar');
            var form = th.down('classicsystemiconsSearchForm');
            var grid = th.down('classicsystemiconsGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
