Ext.define("App.view.classic.system.icons.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemiconsPicker',
    gridClassName:'App.view.classic.system.icons.GridMini',
    displayField: 'iconName',
    valueField:'id',
    columnsNumber:5,//列数
    createPicker: function () {
        var me = this,
            picker = me.createComponent();

        picker.on("cellClick", me.onCellClick, me);

        // me.on("focus", me.onFocusHandler, me);
        return picker;
    },
    onCellClick: function (view, cell, index, record, e, eOpts) {
        var me = this;
        me.setValue(record.data[index]);
        me.setKeyValue(record.data[index]);
        me.setRawValue(record.data[index]);
        me.getPicker().hide();
        me.inputEl.focus();
    },
    setValue: function (value) {
        var me = this,
            isContinue = true;

        if (me.value != value) {
            isContinue = me.fireEvent('beforechange', me, value, me.value);
        }
        if (!isContinue) {
            return false;
        }

        me.fireEvent('change', me, value, me.value);
        me.value = value;

        if (value === undefined) {
            return false;
        }
        return me;
    },
    createComponent: function () {
        var picker, me = this;
        if (this.gridClassName) {
            picker = Ext.create(this.gridClassName, {
                floating: true,
                columnsNumber:this.columnsNumber,
                width: this.pickerWidth,
                height: this.pickerHeight
            });
            me.store = picker.getStore();
            var proxy = me.store.getProxy();
            proxy.extraParams = {};
            proxy.extraParams[me.valueField] = me.value;
            //通过valueField字段查询displayField字段的值
            if (!Ext.isEmpty(me.value)) {
                me.store.load({
                    scope: this,
                    callback: function (records, operation, success) {
                        if (records.length == 1 && me.value == records[0].get(me.valueField)) {
                            me.setRawValue(records[0].get(me.displayField));
                        }
                    }
                });
            }

            //重置查询
            if (me.params && picker.store && picker.store.config.autoLoad) {
                setTimeout(function () {
                    proxy.extraParams = me.params;
                    me.store.load();
                }, 50);
            }
        }

        picker.getStore().on('beforeload', function () {
            var proxy = this.getProxy();
            if (proxy.extraParams) {
                Ext.apply(proxy.extraParams, me.params);
            } else {
                proxy.extraParams = Ext.clone(me.params);
            }
        });

        return picker;
    }
});
