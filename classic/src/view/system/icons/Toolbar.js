Ext.define("App.view.classic.system.icons.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicsystemiconsToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classicsystemiconsWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'classicsystemiconsWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'sysIconsCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
