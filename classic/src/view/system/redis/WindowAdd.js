Ext.define("App.view.classic.system.redis.WindowAdd", { 
    extend: "App.view.classic.system.redis.Window",
    alias: "widget.classicsystemredisWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'sysRedisCtl/add'}, 'close']
});
