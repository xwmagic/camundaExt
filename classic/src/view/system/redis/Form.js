Ext.define("App.view.classic.system.redis.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicsystemredisForm',
    _mainConfs: App.view.classic.system.redis.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        key:{allowBlank:false}
        ,value:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36}
            ],
            [
                {name:"key",maxLength:500}
            ],
            [
                {name:"value",maxLength:6000}
            ],
            [
                {name:"createDate"}
            ]
        ]
    }
});
