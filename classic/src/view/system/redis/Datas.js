Ext.define("App.view.classic.system.redis.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'id'}
            ,{ name:'keyRedis', type:'string', text:'key'}
            ,{ name:'valueJson', type:'string', text:'值'}
            ,{ name:'createDate', type:'date', text:'创建时间'}
        ]
    }
});
