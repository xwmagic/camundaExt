Ext.define("App.classic.view.system.role.Window", { 
    extend: "App.view.common.Window",
    alias: "widget.viewsystemroleWindow",
    openValidation:true,
    isGetChangeValue:true,
    width: 1000, // 窗口宽度
    height: 768, // 窗口高度
    bodyPadding: '15px'
});
