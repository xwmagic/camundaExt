Ext.define("App.classic.view.system.role.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'roleName', type:'string', text:'角色名称'}
            ,{ name:'roleType', type:'int', text:'角色类型'}
            ,{ name:'roleTypeName', type:'string', text:'角色类型'}
            ,{ name:'createName', type:'string', text:'创建人'}
            ,{ name:'createId', type:'string', text:'创建人id'}
            ,{ name:'createDate', type:'date', text:'创建时间'}
            ,{ name:'lastUpdateDate', type:'date', text:'修改时间'}
            ,{ name:'lastUpdateId', type:'string', text:'修改人id'}
            ,{ name:'isEnable', type:'string', text:'是否可用'}
            ,{ name:'companyId', type:'string', text:'所属公司'}
            ,{ name:'companyName', type:'string', text:'所属公司'}
            ,{ name:'companyCode', type:'string', text:'所属公司代码'}
            ,{ name:'orgId', type:'string', text:'所属组织'}
            ,{ name:'orgName', type:'string', text:'所属组织'}
            ,{ name:'orgCode', type:'string', text:'所属组织代码'}
            ,{ name:'lastUpdateName', type:'string', text:'修改人'}
            ,{ name:'version', type:'int', text:'版本'}
            ,{ name:'isDelete', type:'string', text:'是否删除'}
            ,{ name:'appCode', type:'string', text:'APP编码'}
            ,{ name:'appName', type:'string', text:'APP名称'}
        ],
        User:[
            {name: "id", type: "string", text: "主键ID"}
            ,{name:"userName", type:"string", text:"账号"}
            ,{name:"userId", type:"string", text:"用户id"}
            ,{name:"userRealName", type:"string", text:"姓名"}
            ,{name:"companyId", type:"string", text:"公司ID"}
            ,{name:"companyName", type:"string", text:"公司名称"}
            ,{name:"orgId", type:"string", text:"组织id"}
            ,{name:"orgName", type:"string", text:"组织名称"}
        ]
    }
});
