Ext.define("App.classic.view.system.role.TagField", {
    extend: "App.view.BaseTagField",
    xtype: 'viewsystemroleTagField',
    dynamicDataUrl:'sysRoleCtl/pageList',//远程请求地址
    displayField: 'roleName',
    valueField:'id'
});
