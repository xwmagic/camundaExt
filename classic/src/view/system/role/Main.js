Ext.define("App.classic.view.system.role.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'viewsystemroleMain',
    requires: [ 
        "App.classic.view.system.role.Form",
        "App.classic.view.system.role.Grid",
        "App.classic.view.system.role.SearchForm",
        "App.classic.view.system.role.Toolbar",
        "App.classic.view.system.role.ViewModel",
        "App.classic.view.system.role.Window",
        "App.classic.view.system.role.WindowAdd",
        "App.classic.view.system.role.WindowEdit"
    ],
    viewModel: 'viewsystemroleViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'viewsystemroleSearchForm',hidden: false},
        {xtype: 'viewsystemroleToolbar',hidden: false},
        {xtype: 'viewsystemroleGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('viewsystemroleToolbar');
            var form = th.down('viewsystemroleSearchForm');
            var grid = th.down('viewsystemroleGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
