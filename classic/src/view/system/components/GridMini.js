Ext.define("App.view.classic.system.components.GridMini", {
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicsystemcomponentsGridMini',
    _mainConfs: App.view.classic.system.components.Datas.Main,
    url:'sysJsItemCtl/pageList',//grid的查询地址
    filterName: 'name',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",minWidth:250},
            {name:"className",minWidth:350},
            {name:"path",flex:1,minWidth:350},
            {name:"remarks",minWidth:150}
        ]
    }
});
