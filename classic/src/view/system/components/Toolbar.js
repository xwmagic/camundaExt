Ext.define("App.view.classic.system.components.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicsystemcomponentsToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classicsystemcomponentsWindowAdd',text:'生成组件'},//处理方法_create
            {name: 'delAll',text:'删除所有',handler: function () { this.up('commontoolbar')._delAll();},bind: {hidden: '{delAll}'}},
            {name: 'd',actionUrl:'sysJsItemCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    },
    _delAll: function () {
        var me = this;
        var wait = Ext.Msg.show({
            width:300,
            msg: '数据提交中...',
            wait: {
                interval: 500
            }
        });
        App.ux.Ajax.request({
            url: 'sysJsItemCtl/delAll',
            success: function (obj) {
                wait.hide();
                me._targetObject._refresh();
            },
            failure: function () {
                wait.hide();
            }
        });
    }
});
