Ext.define("App.view.classic.system.components.Picker", {
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemcomponentsPicker',
    gridClassName:'App.view.classic.system.components.GridMini',
    editable:true,
    defaultListenerScope:true,
    displayField: 'name',
    valueField:'id',
    listeners:{
        blur:function (th,val) {
            if(th.editable){
                th.setValue(th.rawValue);
            }
        }
    }
});
