Ext.define("App.view.classic.system.components.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicsystemcomponentsGrid',
    _mainConfs: App.view.classic.system.components.Datas.Main,
    url:'sysJsItemCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",flex:1},
            {name:"className",flex:1},
            {name:"path",flex:1},
            {name:"remarks",flex:1}
        ]
    }
});
