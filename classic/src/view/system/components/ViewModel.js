Ext.define("App.view.classic.system.components.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemcomponentsViewModel',
    data: {
        pageName: 'classicsystemcomponentsMain',
        add: true,
        delAll: true,
        del: true
    }
});
