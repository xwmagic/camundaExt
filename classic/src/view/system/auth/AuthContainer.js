Ext.define("App.classic.view.system.auth.AuthContainer", {
    extend: "Ext.container.Container",
    alias: "widget.sysAuthContainer",
    bodyPadding: '5px',
    layout: {type:'hbox',align: 'stretch'},

    defaults: {
        margin: '0 0 0 0',
        border: true,
        xtype: 'menuMainTreeGrid',
        searchMask:true,
        forceNoCheckTree:false,
        isCheckTree:true
    },
    searchUrl:'',
    searchNotUrl:'',
    addUrl:'',
    removeUrl:'',
    paramName:'',
    initComponent: function () {
        this._addItems();

        this.callParent();
    },
    _addItems: function () {
        var me = this;
        this.items = [
            {
                name: 'hasAuth', title: '已拥有权限', flex: 1,
                url:me.searchUrl,
                checkPropagation: 'down',
                defaultParams: me.getHasAuthParam()
            },
            {
                xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, width: 80,
                items: [
                    {xtype: 'container', flex: 1},
                    {
                        text: '<<',
                        tooltip: '添加',
                        xtype: 'button',
                        name: 'add',
                        margin: '0 0 20 0',
                        handler: function () {
                            me.addAuth();
                        }
                    },
                    {
                        text: '>>',
                        tooltip: '移除',
                        margin: '0 0 20 0',
                        name: 'remove',
                        xtype: 'button',
                        handler: function () {
                            me.removeAuth();
                        }
                    },
                    {
                        text: '刷新',
                        margin: '0 0 0 0',
                        xtype: 'button',
                        handler: function () {
                            me.refreshAuth();
                        }
                    },
                    {xtype: 'container', flex: 1}
                ]
            },
            {
                name: 'noneAuth', title: '系统权限', flex: 1,
                checkPropagation: 'upAll',
                url:me.searchNotUrl,
                defaultParams: me.getNoneAuthParam(),
                tbar: [{
                    xtype: 'button', text: '选择所有子节点', handler: function () {
                        var tree = this.up('treepanel');
                        var record = tree.getSelection();
                        if (record.length < 1) {
                            return;
                        }
                        tree.checkAllChild(record[0]);
                    }
                }]
            }
        ];
    },
    getHasAuthParam:function(){
        var hasAuthParam = {
            hasRes: true
        };
        if(this._record && this._record[this.paramName]){
            hasAuthParam[this.paramName] = this._record[this.paramName];
        }else if(this._record){
            hasAuthParam[this.paramName] = this._record.id;
        }
        return hasAuthParam;
    },
    refreshAuth:function () {
        if(this.down('treepanel[name=hasAuth]')){
            this.down('treepanel[name=hasAuth]').defaultParams = this.getHasAuthParam();
            this.down('treepanel[name=hasAuth]')._refresh();
        }
        if(this.down('treepanel[name=noneAuth]')){
            this.down('treepanel[name=noneAuth]').defaultParams = this.getNoneAuthParam();
            this.down('treepanel[name=noneAuth]')._refresh();
        }
    },

    searchAuth:function (recordData) {
        this._record = recordData;
        this.down('[name=hasAuth]').defaultParams = this.getHasAuthParam();
        this.down('treepanel[name=hasAuth]')._refresh();
    },

    getNoneAuthParam:function(){
        var noneAuthParam = {
            hasRes: false
        };
        if(this._record){
            noneAuthParam[this.paramName] = this._record.id;
        }
        return noneAuthParam;
    },
    addAuth:function () {
        var noneAuth = this.down('treepanel[name=noneAuth]');
        var ids = noneAuth._getCheckedDataIds();
        if(ids.length < 1){
            App.ux.Toast.show('提示', '请在【未拥有权限】列表中选择需要添加的权限！','i');
            return;
        }

        this.submitAuth(ids,this.addUrl);
    },

    removeAuth:function () {
        var hasAuth = this.down('treepanel[name=hasAuth]');
        var ids = hasAuth._getCheckedDataIds();
        if(ids.length < 1){
            App.ux.Toast.show('提示', '请在【已拥有权限】列表中选择需要移除的权限！','i');
            return;
        }
        this.submitAuth(ids,this.removeUrl);
    },

    buttonDisable:function () {
        this.down('button[name=add]').disable();
        this.down('button[name=remove]').disable();
    },
    buttonEnable:function () {
        this.down('button[name=add]').enable();
        this.down('button[name=remove]').enable();
    },
    getParam: function (ids, me) {
        var param = {
            ids: ids.toString()
        };
        param[this.paramName] = me._record.id;
        return param;
    },
    submitAuth:function (ids,url) {
        var me = this;
        this.buttonDisable();
        var param = this.getParam(ids, me);

        App.ux.Ajax.request({
            url: url,
            params: param,
            success: function () {
                me.submitAuthSuccess();
                me.refreshAuth();
                me.buttonEnable();
            },
            failure: function () {
                me.buttonEnable();
            }
        });
    },
    submitAuthSuccess:function () {

    }
});
