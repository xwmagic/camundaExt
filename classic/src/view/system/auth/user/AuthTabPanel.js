Ext.define("App.view.sys.user.AuthTabPanel", {
    extend: "Ext.tab.Panel",
    xtype: 'sysuserAuthTabPanel',
    initComponent:function(){
        this.items = [{
            title: '所有权限',
            name:'allAuth',
            xtype:'sysuserAuthUserAll'
        }, {
            title: '个人权限',
            name:'userAuth',
            _autoLoad:false,
            xtype:'sysAuthUserReadContainer'
        },{
            title: '角色权限',
            name:'roleAuth',
            xtype:'sysuserAuthRolesTabPanel'
        }, {
            title: '组织权限',
            name:'orgAuth',
            _autoLoad:false,
            xtype:'sysAuthOrgReadContainer'
        }];
        this.callParent();
    },
    listeners:{
        tabchange:function (tabPanel, newCard, oldCard) {
            newCard.refreshAuth();
        }
    },
    searchAuth:function (recordData) {
        this.down('[name=allAuth]')._record = recordData;
        this.down('[name=userAuth]')._record = recordData;
        this.down('[name=orgAuth]')._record = recordData;
        this.down('[name=roleAuth]').searchAuth(recordData);
        this.getActiveTab().refreshAuth();
    }

});
