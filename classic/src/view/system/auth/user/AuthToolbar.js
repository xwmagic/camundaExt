Ext.define("App.classic.view.system.user.AuthToolbar", {
    extend: "App.view.common.Toolbar",
    xtype: 'viewsystemuserAuthToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'setAuth',actionObject:'sysAuthUserWindow',text:'分配权限',handler:function (btn) {
                this.up('commontoolbar')._update(btn);
            }}//处理方法_update
        ]
    }
});
