Ext.define("App.classic.view.system.user.AuthUserWindow", {
    extend: "App.classic.view.system.auth.AuthWindow",
    alias: "widget.sysAuthUserWindow",
    searchUrl:'auth/getUserPermission',
    searchNotUrl:'auth/getAllMenus',
    addUrl:'systemPerUserCtl/add',
    removeUrl:'systemPerUserCtl/delById',
    paramName:'userId'
});
