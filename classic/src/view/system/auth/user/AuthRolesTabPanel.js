Ext.define("App.view.sys.user.AuthRolesTabPanel", {
    extend: "Ext.tab.Panel",
    xtype: 'sysuserAuthRolesTabPanel',
    listeners:{
        tabchange:function (tabPanel, newCard, oldCard) {
            newCard.refreshAuth();
        }
    },
    refreshAuth:function () {
        this.setActiveItem(this.down('sysAuthRoleReadContainer'));
    },
    searchAuth:function (recordData) {
        var items = [];
        this.removeAll();
        if(recordData && recordData.roleIds){
            for(var i in recordData.roleIds){
                items.push({
                    title: recordData.roleName[i],
                    _record:{roleId:recordData.roleIds[i]},
                    _autoLoad:false,
                    xtype:'sysAuthRoleReadContainer'
                });
            }

        }
        this.add(items);
    }

});
