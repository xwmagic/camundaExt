Ext.define("App.view.sys.AuthUserReadContainer", {
    extend: "App.classic.view.system.auth.AuthContainer",
    alias: "widget.sysAuthUserReadContainer",
    paramName:'userId',
    _autoLoad:true,
    _addItems: function () {
        var me = this;
        this.items = [
            {
                name: 'hasAuth', flex: 1,forceNoCheckTree:true,
                url:'auth/getUserPermission',
                _autoLoad:this._autoLoad,
                defaultParams: me.getHasAuthParam()
            }
        ];
    },

    refreshAuth:function () {
        this.down('treepanel[name=hasAuth]').defaultParams = this.getHasAuthParam();
        this.down('treepanel[name=hasAuth]')._refresh();
    }
});
