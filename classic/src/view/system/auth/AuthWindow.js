Ext.define("App.classic.view.system.auth.AuthWindow", {
    extend: "App.view.common.Window",
    alias: "widget.sysAuthWindow",
    openValidation: true,
    width: 700, // 窗口宽度
    height: 600, // 窗口高度
    bodyPadding: '5px',
    layout:'fit',
    searchUrl:'',
    searchNotUrl:'',
    addUrl:'',
    removeUrl:'',
    paramName:'',
    initComponent: function () {
        this._addItems();

        this.callParent();
    },
    _addItems: function () {
        var me = this;
        this.items = [
            {
                xtype:'sysAuthContainer',
                searchUrl:me.searchUrl,
                searchNotUrl:me.searchNotUrl,
                addUrl:me.addUrl,
                removeUrl:me.removeUrl,
                paramName:me.paramName,
                _record:me._record
            }
        ];
    }

});
