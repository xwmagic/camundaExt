Ext.define("App.view.sys.AuthOrgOneContainer", {
    extend: "App.view.sys.AuthOrgContainer",
    alias: "widget.sysAuthOrgOneContainer",
    getParam: function (ids, me) {
        var param = {
            ids: ids.toString(),
            orgIds:me._record.ids.toString(),
            orgNames:me._record.orgNames.toString()
        };
        return param;
    },
    removeAuth:function () {
        var hasAuth = this.down('treepanel[name=noneAuth]');
        var ids = hasAuth._getCheckedDataIds();
        if(ids.length < 1){
            App.ux.Toast.show('提示', '请在【系统权限】列表中选择需要移除的权限！','i');
            return;
        }
        this.submitAuth(ids,this.removeUrl);
    },
    submitAuth:function (ids,url) {
        var me = this;
        var param = this.getParam(ids, me);
        App.ux.Ajax.request({
            url: url,
            params: param,
            success: function () {
                me.submitAuthSuccess();
            },
            failure: function () {
            }
        });
    }
});
