Ext.define("App.view.sys.org.AuthOrgWindowRemove", {
    extend: "App.view.sys.org.AuthOrgWindow",
    alias: "widget.sysAuthOrgWindowRemove",
    editFlag:'remove',
    buttons:[{name:'submit',text:'确定移除'},'close'],
    _onSubmit: function(btn){
        btn.up('window').down('sysAuthContainer').removeAuth();
    }
});
