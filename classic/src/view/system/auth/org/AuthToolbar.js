Ext.define("App.view.classic.system.org.AuthToolbar", {
    extend: "App.view.common.TreeToolbar",
    xtype: 'classicsystemorgAuthToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'setAuthAdd', actionObject: 'sysAuthOrgWindowAdd', text: '批量添加权限',handler:function (btn) {
                this.up('commontoolbar')._update(btn);
            }},//处理方法_update
            {name: 'setAuthDel', actionObject: 'sysAuthOrgWindowRemove', text: '批量移除权限',handler:function (btn) {
                this.up('commontoolbar')._update(btn);
            }}
        ]
    },
    _update: function (btn) {
        var me = btn.up('commontoolbar');
        var tree = me._targetObject,
            data, win;

        var orgNames = tree._getCheckedDataPropertyValue('orgName');
        var ids = tree._getCheckedDataPropertyValue('id');

        if (ids.length < 1) {
            App.ux.Toast.show('提示', '请勾选需要' + btn.tooltip + '的组织架构！','i');
            return false;
        }
        data = {
            ids: ids,
            orgNames: orgNames,
            id: ''
        };
        btn.text = '为' + orgNames.length + '个组织[' + orgNames.toString() + '] 分配权限';
        win = me._getWindow(btn, data);
        win._show();
        return win;
    }
});
