Ext.define("App.view.classic.system.org.AuthTreeGrid", {
    extend: "App.view.common.EditTreeGridMini",
    xtype: 'classicsystemorgAuthTreeGrid',
    _mainConfs: App.view.classic.system.org.Datas.Main,
    url:'sysOrgCtl/findTree',//tree的查询地址
    filterName: 'orgName',
    forceNoCheckTree:false,
    isCheckTree:true,
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"orgName",xtype:"treecolumn",flex:1}
        ]
    },
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            xtype: 'button',
            tooltip:'选中所有子节点',
            iconCls:'x-fa fa-check-square-o',
            handler: function () {
                var tree = this.up('treepanel');
                var record = tree.getSelection();
                if(record.length < 1){
                    return;
                }
                tree.checkAllChild(record[0]);
            }
        },{
            xtype: 'button',
            iconCls:'x-fa fa-close',
            tooltip:'取消选中所有子节点',
            handler: function () {
                var tree = this.up('treepanel');
                var record = tree.getSelection();
                if(record.length < 1){
                    return;
                }
                tree.uncheckAllChild(record[0]);
            }
        },{
            xtype: 'button',
            tooltip: '展开所有',
            iconCls:'x-fa fa-minus-square-o',
            handler: function () {
                var tree = this.up('treepanel');
                tree.expandAll();
            }
        },{
            xtype: 'button',
            tooltip: '折叠所有',
            iconCls:'x-fa fa-plus-square-o',
            handler: function () {
                var tree = this.up('treepanel');
                tree.collapseAll();
            }
        }]
    }]
});
