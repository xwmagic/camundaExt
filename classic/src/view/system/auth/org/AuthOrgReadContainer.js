Ext.define("App.view.sys.AuthOrgReadContainer", {
    extend: "App.view.sys.AuthOrgContainer",
    alias: "widget.sysAuthOrgReadContainer",
    _autoLoad:true,
    _addItems: function () {
        var me = this;
        this.items = [
            {
                name: 'hasAuth', flex: 1,forceNoCheckTree:true,
                url:'auth/getOrgPermission',
                _autoLoad:this._autoLoad,
                defaultParams: me.getHasAuthParam()
            }
        ];
    },

    refreshAuth:function () {
        this.down('treepanel[name=hasAuth]').defaultParams = this.getHasAuthParam();
        this.down('treepanel[name=hasAuth]')._refresh();
    }
});
