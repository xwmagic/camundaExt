Ext.define("App.view.sys.org.AuthOrgWindow", {
    extend: "App.classic.view.system.auth.AuthWindow",
    alias: "widget.sysAuthOrgWindow",
    buttons:[{name:'submit'},'close'],
    width: 400, // 窗口宽度,
    editFlag:'',
    _addItems: function () {
        var me = this;
        var checkPropagation = 'upAll';
        if(me.editFlag == 'remove'){
            checkPropagation = 'down';
        }
        this.items = [
            {
                xtype:'sysAuthOrgOneContainer',
                _record:me._record,
                _addItems: function () {
                    this.items = [
                        {
                            name: 'noneAuth', title: '系统权限', flex: 1,
                            checkPropagation: checkPropagation,
                            defaultParams: this.getNoneAuthParam(),
                            tbar: [{
                                xtype: 'button', text: '选择所有子节点',
                                handler: function () {
                                    var tree = this.up('treepanel');
                                    var record = tree.getSelection();
                                    if (record.length < 1) {
                                        return;
                                    }
                                    tree.checkAllChild(record[0]);
                                }
                            }]
                        }
                    ];
                },
                submitAuthSuccess:function () {
                    App.ux.Toast.show('提示','批量修改成功！','s');
                    me.close();
                }
            }
        ];
    }

});
