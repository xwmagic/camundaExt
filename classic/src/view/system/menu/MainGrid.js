Ext.define("App.classic.view.system.menu.MainGrid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'viewsystemmenuMainGrid',
    _mainConfs: App.classic.view.system.menu.Datas.Main,
    url:'permission/permissionList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'perName',width:150},
            {name: 'perType',valueToName:'commonMenuType',align:'center'},
            {name: 'perIcon'},
            {name: 'perRoutingUrl',width:160},
            {name: 'perUrl',flex:1,minWidth:150},
            {name: 'perBtnValid',width:120},
            {name: 'perEnabled',valueToName:'booleanYNC',align:'center'}
        ]
    }
});
