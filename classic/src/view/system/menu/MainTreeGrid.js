Ext.define("App.classic.view.system.menu.MainTreeGrid", {
    extend: "App.view.common.EditTreeGridMini",
    xtype: 'menuMainTreeGrid',
    _mainConfs: App.classic.view.system.menu.Datas.Main,
    url: 'auth/getAllMenus',//grid的查询地址
    filterName: 'perName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {
                name: 'perName', xtype: 'treecolumn', flex: 1,
                renderer: function (v) {
                    if (!v) {
                        v = '';
                    } else {
                        v = Ext.Component.htmlEscape(v);
                    }
                    return v;
                }
            }
        ]
    }

});
