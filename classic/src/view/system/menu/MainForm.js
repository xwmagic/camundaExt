Ext.define("App.classic.view.system.menu.MainForm", { 
    extend: "App.view.common.Form",
    xtype: 'viewsystemmenuMainForm',
    _mainConfs: App.classic.view.system.menu.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'id',hidden:true},
                {name: 'perName'},
                {name: 'perType',xtype:'commonMenuType'}
            ],
            [
                {name: 'perIcon'},
                {name: 'perEnabled',xtype:'booleanYNC',comType: 'radiogroup'}
            ],
            [
                {name: 'perRoutingUrl'}
            ],
            [
                {name: 'perUrl'}
            ],
            [

            ]
        ]
    }
});
