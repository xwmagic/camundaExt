Ext.define("App.classic.view.system.menu.MainEditToolbar", {
    extend: "App.view.common.TreeToolbar",
    xtype: 'systemmenuMainEditToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            'lock',
            'c',
            'addChild',//处理方法_create
            'd',//处理方法_delete，actionUrl配置删除的请求地址
            {
                name: 'addBaseBtn', tooltip: '添加基础按钮', iconCls: 'x-fa fa-cube', bind: {hidden: '{addBaseBtn}'},disabled:true,
                handler: function () {
                    this.up('commontoolbar')._addBaseBtn();
                }
            },
            {
                name: 'addRelateUrls', tooltip: '添加关联地址', iconCls: 'x-fa fa-exchange', bind: {hidden: '{addBaseBtn}'},disabled:true,
                handler: function () {
                    this.up('commontoolbar').addRelateUrls();
                }
            },
            {name:'save',actionUrl:'permission/add'}
        ]
    },
    _addBaseBtn: function () {
        var selections,
            currentNode;
        selections = this._targetObject.getSelectionModel().getSelection();

        if (selections.length > 0) {
            currentNode = selections[0];
        }
        if (currentNode.data.perType !== 'child' || currentNode.length === 0) {
            App.ux.Toast.show('提示', '只能给菜单添加基础操作按钮！','i');
            return;
        }

        currentNode.insertChild(currentNode.childNodes.length,
            {perName: '新增', perIcon: 'x-fa fa-plus-square', perType: 'btn', perBtnValid: 'add', perEnabled: '1'}
        );
        currentNode.insertChild(currentNode.childNodes.length + 4,
            {perName: '新增子节点', perIcon: 'x-fa fa-plus-circle', perType: 'btn', perBtnValid: 'addChild', perEnabled: '1'}
        );
        currentNode.insertChild(currentNode.childNodes.length + 1,
            {perName: '编辑', perIcon: 'x-fa fa-edit fa-lg', perType: 'btn', perBtnValid: 'update', perEnabled: '1'}
        );
        currentNode.insertChild(currentNode.childNodes.length + 2,
            {perName: '删除', perIcon: 'x-fa fa-minus-square', perType: 'btn', perBtnValid: 'del', perEnabled: '1'}
        );
        currentNode.insertChild(currentNode.childNodes.length + 3,
            {perName: '保存', perIcon: 'x-fa fa-save', perType: 'btn', perBtnValid: 'save', perEnabled: '1'}
        );

        currentNode.expand();
        if (this.isRefreshRownum) {
            this.getView().refresh();
        }
    },
    addRelateUrls: function () {
        var me = this;
        var currentNode;

        currentNode = this._targetObject.getSelectOnlyRecord(true);
        if (!currentNode) {
            return;
        }
        Ext.create('App.view.classic.system.urls.SelectWindow', {
            _targetObject: {
                setValue: function (datas) {
                    var name;
                    for (var i in datas) {
                        name = datas[i].urlInfo;
                        if(!name){
                            name = '关联URL';
                        }
                        currentNode.insertChild(0,
                            {
                                perName: datas[i].urlInfo,
                                perUrl: '/' + datas[i].urlName,
                                perIcon: '',
                                perType: 'url',
                                perBtnValid: 'search',
                                perEnabled: '1'
                            }
                        );
                    }

                }
            }
        }).show();
    }
});
