Ext.define("App.classic.view.system.menu.MainToolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'viewsystemmenuMainToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'viewsystemmenuMainWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'viewsystemmenuMainWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'permission/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
