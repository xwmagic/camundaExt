Ext.define("App.view.classic.system.urls.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicsystemurlsGrid',
    _mainConfs: App.view.classic.system.urls.Datas.Main,
    url:'sysUrlsCtl/pageList',//grid的查询地址
    columnsDefault:{
        flex:1
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"urlName",hyperlinkMode:true,hyperlinkWindow:"classicsystemurlsWindowView",align:'left'},
            {name:"urlClass",align:'left'},
            {name:"urlTags"},
            {name:"urlInfo"},
            {name:"urlPackage"}
        ]
    }
});
