Ext.define("App.view.classic.system.urls.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'urlName', type:'string', text:'URL'}
            ,{ name:'urlClass', type:'string', text:'URL类'}
            ,{ name:'urlPackage', type:'string', text:'URL包'}
            ,{ name:'createName', type:'string', text:'创建人'}
            ,{ name:'createId', type:'string', text:'创建人id'}
            ,{ name:'createDate', type:'date', text:'创建时间'}
            ,{ name:'lastUpdateDate', type:'date', text:'修改时间'}
            ,{ name:'lastUpdateId', type:'string', text:'修改人id'}
            ,{ name:'isEnable', type:'string', text:'是否可用'}
            ,{ name:'companyId', type:'string', text:'所属公司'}
            ,{ name:'companyName', type:'string', text:'所属公司'}
            ,{ name:'companyCode', type:'string', text:'所属公司代码'}
            ,{ name:'orgId', type:'string', text:'所属组织'}
            ,{ name:'orgName', type:'string', text:'所属组织'}
            ,{ name:'orgCode', type:'string', text:'所属组织代码'}
            ,{ name:'appName', type:'string', text:'APP名称'}
            ,{ name:'appCode', type:'string', text:'APP编码'}
            ,{ name:'urlTags', type:'undefined', text:'URL标签'}
            ,{ name:'urlInfo', type:'undefined', text:'URL描述'}
        ]
    }
});
