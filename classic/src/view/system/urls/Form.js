Ext.define("App.view.classic.system.urls.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicsystemurlsForm',
    _mainConfs: App.view.classic.system.urls.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        urlName:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
            ],
            [
                {name:"urlName",maxLength:500}
            ],
            [
                {name:"urlClass",maxLength:200}
            ],
            [
                {name:"urlPackage",maxLength:100}
            ],
            [
                {name:"urlTags",maxLength:200}
            ],
            [
                {name:"urlInfo",maxLength:200}
            ]
        ]
    }
});
