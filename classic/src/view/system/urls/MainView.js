Ext.define("App.view.classic.system.urls.MainView", {
    extend: 'Ext.container.Container',
    xtype: 'classicsystemurlsMainView',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemurlsSearchForm',hidden: false},
        {xtype: 'classicsystemurlsGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var form = th.down('classicsystemurlsSearchForm');
            var grid = th.down('classicsystemurlsGrid');
            form._targetObject = grid;//给查询框关联操作对象
        }
    }
});
