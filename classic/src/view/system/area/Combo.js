Ext.define("App.view.classic.system.area.Combo", { 
    extend: "App.view.BaseCombo",
    xtype: 'classicsystemareaCombo',
    dynamicDataUrl:'sysAreaCtl/pageList',//数据的查询地址
    valueField: 'wholeName',
    displayField: 'wholeName'
});
