Ext.define("App.view.classic.system.area.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemareaPicker',
    gridClassName:'App.view.classic.system.area.GridMini',
    editable:true,
    displayField: 'wholeName',
    valueField:'id'
});
