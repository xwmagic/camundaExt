Ext.define("App.view.classic.system.area.PickerSimple", {
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemareaPickerSimple',
    pickerWidth: 370,
    editable:true,
    value:'广东省深圳市光明区',
    gridClassName:'App.view.classic.system.area.GridMiniSimple',
    displayField: 'wholeName',
    valueField:'id'
});
