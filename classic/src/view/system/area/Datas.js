Ext.define("App.view.classic.system.area.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'provinceId', type:'string', text:'省id'}
            ,{ name:'cityId', type:'string', text:'城市id'}
            ,{ name:'countyId', type:'string', text:'区域id'}
            ,{ name:'parentId', type:'string', text:'父id'}
            ,{ name:'level', type:'int', text:'等级'}
            ,{ name:'wholeName', type:'string', text:'全称'}
            ,{ name:'areaName', type:'string', text:'地区名称'}
            ,{ name:'simpleName', type:'string', text:'简称'}
            ,{ name:'pinYin', type:'string', text:'拼音'}
            ,{ name:'prePinYin', type:'string', text:'拼音首字母'}
            ,{ name:'simplePy', type:'string', text:'拼音每个字首字母'}
            ,{ name:'areaCode', type:'string', text:'城市编码'}
            ,{ name:'zipCode', type:'string', text:'邮政编码'}
            ,{ name:'lat', type:'string', text:'纬度'}
            ,{ name:'lon', type:'string', text:'经度'}
            ,{ name:'remark', type:'string', text:'备注'}
        ]
    }
});
