Ext.define("App.view.classic.system.app.Form", {
    extend: "App.view.common.Form",
    xtype: 'classicsystemappForm',
    _mainConfs: App.view.classic.system.app.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        appName: {allowBlank: false}
        , appEnName: {allowBlank: false}
        , appType: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: "id", maxLength: 36, hidden: true}
                , {name: "appName", maxLength: 100}
                , {name: "appEnName", maxLength: 100}
                , {name: "appType", maxLength: 10}
            ],
            [
                {name: "appCode", maxLength: 50}
                , {name: "appUser", maxLength: 100}
                , {
                name: "orgName",keyName:'orgId',
                xtype: "classicsystemorgPicker",
                listeners: {
                    select: function (th,val,record) {

                    }
                }
            }
            ],
            [
                {name: "appLogo", maxLength: 100}
            ],
            [
                {name: "appBkg", maxLength: 100}
            ],
            [
                {name: "appUrl", maxLength: 200}
            ],
            [
                {name: "appInfo", maxLength: 2000}
            ]
        ]
    }
});
