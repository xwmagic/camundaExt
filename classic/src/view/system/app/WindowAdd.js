Ext.define("App.view.classic.system.app.WindowAdd", { 
    extend: "App.view.classic.system.app.Window",
    alias: "widget.classicsystemappWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'sysAppCtl/add'}, 'close']
});
