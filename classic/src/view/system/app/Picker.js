Ext.define("App.view.classic.system.app.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemappPicker',
    gridClassName:'App.view.classic.system.app.GridMini',
    displayField: 'appName',
    valueField:'id'
});
