Ext.define("App.view.classic.system.app.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键ID'}
            ,{ name:'appName', type:'string', text:'APP名称'}
            ,{ name:'appCode', type:'string', text:'APP编码'}
            ,{ name:'appType', type:'int', text:'APP类型'}
            ,{ name:'appTypeName', type:'string', text:'APP类型'}
            ,{ name:'appUrl', type:'string', text:'APP访问地址'}
            ,{ name:'appEnName', type:'string', text:'APP英文描述'}
            ,{ name:'appInfo', type:'string', text:'APP描述'}
            ,{ name:'appLogo', type:'string', text:'APP图标'}
            ,{ name:'appUser', type:'string', text:'APP负责人'}
            ,{ name:'appBkg', type:'string', text:'APP背景图'}
            ,{ name:'createName', type:'string', text:'创建人'}
            ,{ name:'createId', type:'string', text:'创建人id'}
            ,{ name:'createDate', type:'date', text:'创建时间'}
            ,{ name:'lastUpdateDate', type:'date', text:'修改时间'}
            ,{ name:'lastUpdateId', type:'string', text:'修改人id'}
            ,{ name:'isEnable', type:'string', text:'是否可用'}
            ,{ name:'companyId', type:'string', text:'所属公司'}
            ,{ name:'companyName', type:'string', text:'所属公司'}
            ,{ name:'companyCode', type:'string', text:'所属公司代码'}
            ,{ name:'orgId', type:'string', text:'所属组织'}
            ,{ name:'orgName', type:'string', text:'所属组织'}
            ,{ name:'orgCode', type:'string', text:'所属组织代码'}
            ,{ name:'lastUpdateName', type:'string', text:'修改人'}
            ,{ name:'version', type:'int', text:'版本'}
            ,{ name:'isDelete', type:'string', text:'是否删除'}
        ]
    }
});
