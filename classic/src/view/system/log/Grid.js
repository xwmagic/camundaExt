Ext.define("App.view.classic.system.log.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicsystemlogGrid',
    _mainConfs: App.view.classic.system.log.Datas.main,
    url:'operationLogCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"appId"},
            {name:"appName",width:150},
            {name:"describeLog",width:200},
            {name:"createName"},
            {name:"operationType",valueToName:'sysLogType',align:'center'},
            {name:"url"},
            {name:"sysIp"},
            {name:"createDate",width:200},
            {name:"level"},
            {name:"operationUnit"},
            {name:"method"},
            {name:"args"},
            {name:"runTime"},
            {name:"returnValue"}

        ]
    }
});
