Ext.define("App.view.classic.system.log.Datas", { 
    statics: {
        main: [
            {"id":32,"name":"id","text":"id","type":"string","length":36}
            ,{"id":33,"name":"appId","text":"系统id","type":"string","length":36}
            ,{"id":34,"name":"appName","text":"系统名称","type":"string","length":100}
            ,{"id":35,"name":"url","text":"请求地址","type":"string","length":300}
            ,{"id":36,"name":"sysIp","text":"ip地址","type":"string","length":50}
            ,{"id":37,"name":"createDate","text":"创建时间","type":"date"}
            ,{"id":38,"name":"level","text":"日志等级","type":"int"}
            ,{"id":39,"name":"operationUnit","text":"被操作的对象","type":"string","length":100}
            ,{"id":40,"name":"method","text":"方法名","type":"string","length":50}
            ,{"id":41,"name":"args","text":"参数","type":"string","length":510}
            ,{"id":42,"name":"createId","text":"操作人id","type":"string","length":36}
            ,{"id":43,"name":"createName","text":"操作人","type":"string","length":50}
            ,{"id":44,"name":"operationType","text":"操作类型","type":"string","length":51}
            ,{"id":45,"name":"describeLog","text":"日志描述","type":"string","length":510}
            ,{"id":46,"name":"runTime","text":"日志时间","type":"long","length":null,"default":"","isQuery":null,"operate":""}
            ,{"id":47,"name":"returnValue","text":"方法返回值","type":"string","length":500,"isQuery":null}
        ]
    }
});
