Ext.define("App.view.classic.system.log.WindowEdit", { 
    extend: "App.view.classic.system.log.Window",
    alias: "widget.classicsystemlogWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'operationLogCtl/update'}, 'close']
});
