Ext.define("App.view.classic.system.log.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicsystemlogForm',
    _mainConfs: App.view.classic.system.log.Datas.main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
                ,{name:"appId",maxLength:36}
                ,{name:"appName",maxLength:100,width:150}
            ],
            [
                {name:"url",maxLength:300}
                ,{name:"sysIp",maxLength:50}
            ],
            [
                {name:"createDate"}
                ,{name:"level"}
            ],
            [
                {name:"operationUnit",maxLength:100}
                ,{name:"method",maxLength:50}
            ],
            [
                {name:"args",maxLength:510}
                ,{name:"createId",maxLength:36}
            ],
            [
                {name:"createName",maxLength:50}
                ,{name:"operationType",maxLength:51}
            ],
            [
                {name:"runTime"}
                ,{name:"returnValue",maxLength:500}
            ],
            [
                {name:"describeLog",maxLength:510}
            ]
        ]
    }
});
