Ext.define("App.view.classic.system.log.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemlogViewModel',
    data: {
        pageName: 'classicsystemlogmain',
        add: true,
        update: true,
        del: true
    }
});
