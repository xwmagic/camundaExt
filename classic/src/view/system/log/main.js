Ext.define("App.view.classic.system.log.main", {
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemlogmain',
    requires: [ 
        "App.view.classic.system.log.Form",
        "App.view.classic.system.log.Grid",
        "App.view.classic.system.log.SearchForm",
        "App.view.classic.system.log.Toolbar",
        "App.view.classic.system.log.ViewModel",
        "App.view.classic.system.log.Window",
        "App.view.classic.system.log.WindowAdd",
        "App.view.classic.system.log.WindowEdit"
    ],
    viewModel: 'classicsystemlogViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemlogSearchForm',hidden: false},
        {xtype: 'classicsystemlogToolbar',hidden: true},
        {xtype: 'classicsystemlogGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemlogToolbar');
            var form = th.down('classicsystemlogSearchForm');
            var grid = th.down('classicsystemlogGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
