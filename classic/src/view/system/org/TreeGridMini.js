Ext.define("App.view.classic.system.org.TreeGridMini", { 
    extend: "App.view.common.EditTreeGridMini",
    xtype: 'classicsystemorgTreeGridMini',
    _mainConfs: App.view.classic.system.org.Datas.Main,
    url:'sysOrgCtl/findTree',//tree的查询地址
    filterName: 'orgName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"orgName",xtype:"treecolumn",width:250}
        ]
    }
});
