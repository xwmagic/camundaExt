Ext.define("App.view.classic.system.org.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemorgViewModel',
    data: {
        pageName: 'classicsystemorgMain',
        lock: true,
        add: true,
        addChild: true,
        save: true,
        del: true
    }
});
