Ext.define("App.view.classic.system.org.Toolbar", { 
    extend: "App.view.common.TreeToolbar",
    xtype: 'classicsystemorgToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            'lock',
            'c',
            'addChild',//处理方法_create
            'd',//处理方法_delete，actionUrl配置删除的请求地址
            {name: 'save', actionUrl:'sysOrgCtl/add'}//处理方法_save
        ]
    }
});
