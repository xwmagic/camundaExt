Ext.define("App.view.classic.system.org.Picker", { 
    extend: "App.view.common.TreeMuiltiPicker",
    xtype: 'classicsystemorgPicker',
    gridClassName:'App.view.classic.system.org.TreeGridMini',
    checkModel:'single',//选择模式
    onlyLeaf:false,
    displayField: 'orgName',
    valueField: 'id'
});
