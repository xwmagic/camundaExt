Ext.define("App.view.classic.system.org.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'ID'}
            ,{ name:'orgName', type:'string', text:'组织名称'}
            ,{ name:'orgType', type:'string', text:'组织类型'}
            ,{ name:'orgCode', type:'string', text:'组织编号'}
            ,{ name:'orgEn', type:'string', text:'组织英文名称'}
            ,{ name:'info', type:'string', text:'描述'}
            ,{ name:'pid', type:'string', text:'父ID'}
            ,{ name:'layer', type:'int', text:'层级'}
            ,{ name:'sortNum', type:'int', text:'排序号'}
            ,{ name:'companyCode', type:'string', text:'所属公司代码'}
            ,{ name:'createName', type:'string', text:'创建人'}
            ,{ name:'createId', type:'string', text:'创建人id'}
            ,{ name:'createDate', type:'date', text:'创建时间'}
            ,{ name:'lastUpdateDate', type:'date', text:'修改时间'}
            ,{ name:'lastUpdateId', type:'string', text:'修改人id'}
            ,{ name:'isEnable', type:'string', text:'是否可用'}
            ,{ name:'companyId', type:'string', text:'所属公司'}
            ,{ name:'companyName', type:'string', text:'所属公司'}
            ,{ name:'lastUpdateName', type:'string', text:'修改人'}
        ]
    }
});
