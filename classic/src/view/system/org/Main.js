Ext.define("App.view.classic.system.org.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemorgMain',
    requires: [ 
        "App.view.classic.system.org.TreeEditGrid",
        "App.view.classic.system.org.SearchForm",
        "App.view.classic.system.org.Toolbar",
        "App.view.classic.system.org.TreeGridMini",
        "App.view.classic.system.org.ViewModel",
        "App.view.classic.system.org.Picker"
    ],
    viewModel: 'classicsystemorgViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemorgSearchForm',hidden: false},
        {xtype: 'classicsystemorgToolbar',hidden: false},
        {xtype: 'classicsystemorgTreeEditGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemorgToolbar');
            var form = th.down('classicsystemorgSearchForm');
            var grid = th.down('classicsystemorgTreeEditGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
