Ext.define("App.view.classic.system.org.TreeEditGrid", { 
    extend: "App.view.common.EditTreeGridDrag",
    xtype: 'classicsystemorgTreeEditGrid',
    _mainConfs: App.view.classic.system.org.Datas.Main,
    url:'sysOrgCtl/findTree',//tree的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"orgName",xtype:"treecolumn",flex:1},
            {name:"orgType",width:130},
            {name:"orgCode",width:130},
            {name:"orgEn",flex:1},
            {name:"companyName",flex:1},
            {name:"info",flex:1}
        ]
    }
});
