Ext.define("App.view.classic.system.job.WindowEdit", { 
    extend: "App.view.classic.system.job.Window",
    alias: "widget.classicsystemjobWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'myScheduleJobCtl/update'}, 'close']
});
