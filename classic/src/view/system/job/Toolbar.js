Ext.define("App.view.classic.system.job.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicsystemjobToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classicsystemjobWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'classicsystemjobWindowEdit'},//处理方法_update
            {name: 'start',actionUrl:'myScheduleJobCtl/start',text:'启动',iconCls:'x-fa fa-play',
                handler: function (btn) {
                    this.up('commontoolbar')._delete(btn);
                }},
            {name: 'pause',actionUrl:'myScheduleJobCtl/pause',text:'暂停',iconCls:'x-fa fa-ban',
                handler: function (btn) {
                    this.up('commontoolbar')._delete(btn);
                }},
            {name: 'startAllJob',actionUrl:'myScheduleJobCtl/startAllJob',text:'启动所有',iconCls:'x-fa fa-thumb-tack'},
            {name: 'pauseAllJob',actionUrl:'myScheduleJobCtl/pauseAllJob',text:'暂停所有',iconCls:'x-fa fa-stop fg-lg'},
            {name: 'd',actionUrl:'myScheduleJobCtl/delJobById',text:'移除任务',iconCls:'x-fa fa-random'},
            {name: 'd',actionUrl:'myScheduleJobCtl/delById'}
            //处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
