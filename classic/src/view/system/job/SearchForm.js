Ext.define("App.view.classic.system.job.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemjobSearchForm',
    _mainConfs: App.view.classic.system.job.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:50}
                ,"sr","ec"
            ],
            [
                {name:"statusJob",maxLength:10}
            ],
            [
                {name:"jobTaskType",maxLength:10}
            ]
        ]
    }
});
