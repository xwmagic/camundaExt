Ext.define("App.view.classic.system.company.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemcompanyMain',
    requires: [ 
        "App.view.classic.system.company.TreeEditGrid",
        "App.view.classic.system.company.SearchForm",
        "App.view.classic.system.company.Toolbar",
        "App.view.classic.system.company.TreeGridMini",
        "App.view.classic.system.company.ViewModel",
        "App.view.classic.system.company.Picker"
    ],
    viewModel: 'classicsystemcompanyViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemcompanySearchForm',hidden: false},
        {xtype: 'classicsystemcompanyToolbar',hidden: false},
        {xtype: 'classicsystemcompanyTreeEditGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemcompanyToolbar');
            var form = th.down('classicsystemcompanySearchForm');
            var grid = th.down('classicsystemcompanyTreeEditGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
