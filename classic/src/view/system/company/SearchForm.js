Ext.define("App.view.classic.system.company.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemcompanySearchForm',
    _mainConfs: App.view.classic.system.company.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"companyName"}
                ,{name:"companyEn"}
                ,"sr","ec"
            ],
            [
                {name:"companyCode",maxLength:100}
                ,{name:"companyType",maxLength:5}
                ,{name:"createName",maxLength:100}
            ]
        ]
    }
});
