Ext.define("App.view.classic.system.company.Picker", { 
    extend: "App.view.common.TreeMuiltiPicker",
    xtype: 'classicsystemcompanyPicker',
    gridClassName:'App.view.classic.system.company.TreeGridMini',
    checkModel:'single',//选择模式
    onlyLeaf:false,
    displayField: 'companyName',
    valueField: 'id'
});
