Ext.define("App.view.classic.system.company.TreeGridMini", { 
    extend: "App.view.common.EditTreeGridDrag",
    xtype: 'classicsystemcompanyTreeGridMini',
    _mainConfs: App.view.classic.system.company.Datas.Main,
    url:'sysCompanyCtl/findTree',//tree的查询地址
    filterName: 'companyName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"companyName",xtype:"treecolumn",flex:1,width:150}
        ]
    }
});
