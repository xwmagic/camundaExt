Ext.define("App.view.classic.system.company.TreeEditGrid", { 
    extend: "App.view.common.EditTreeGridDrag",
    xtype: 'classicsystemcompanyTreeEditGrid',
    _mainConfs: App.view.classic.system.company.Datas.Main,
    url:'sysCompanyCtl/findTree',//tree的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"companyName",xtype:"treecolumn",flex:1,width:150},
            {name:"companyEn",width:150},
            {name:"companyCode",width:120},
            {name:"companyType",width:120},
            {name:"logo"},
            {name:"info",flex:1}
        ]
    }
});
