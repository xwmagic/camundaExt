/**使得所有form表单具备初始值orginValues，并判断数据是否被修改
 * @author xiaowei
 * @date 2017年5月3日
 */
Ext.define('App.overrides.base.Form', {
    override: 'Ext.form.Panel',
	initComponent : function() {
		this.on({
			boxready: function(){
				this.setOrginValues();
			}
		});
		this.callParent(arguments);
	},
	
	setOrginValues: function(val){
		if(val){
			this.orginValues = Ext.clone(val);
		}else{
			this.orginValues = Ext.clone(this.getValues());
		}
	},
	
	getChangeValues: function(requires){
		var change = {}, orgin = this.orginValues, value = this.getValues();
		
		if(!this.isChange()){
			return;
		}
		
		Ext.Object.each(value, function(key, value, myself) {
			if (orgin[key] != value) {
				change[key] = value;
			}
		});
		
		if(Ext.isArray(requires)){
			for(var i = 0; i<requires.length; i++){
				change[requires[i]] = value[requires[i]];
			}
		}

        if(Ext.Object.isEmpty(change)){
            return { _hasFileFieldChange: true };
        }
		return change;
	},
	
	
	isChange: function(){
        var fileFields = this.query('commonFileFieldExp');
        for(var i=0; i<fileFields.length; i++){//判断附件是否修改
            if(fileFields[i].orginValue != fileFields[i].value){
            	return true;
			}
        }
        //如果存在多选附件组件，始终标记为已经发生改变
        if(this.down('fileFields')){
            return true;
        }
		return !Ext.Object.equals(this.orginValues,this.getValues());
	},

	setReadOnly:function (flag) {
        var fields = this.query('field');
        for(var i=0; i<fields.length; i++){//全局设置只读效果
            fields[i].setReadOnly(flag);
        }
    },
    isGridValid:function () {
        var grids = this.query('grid');
        var valid = {valid:true,message:''};
        if(grids.length > 0){
            for(var i=0; i<grids.length; i++){
                if(grids[i].openValidation){
                    valid = grids[i].isValid();
                    if(!valid.valid){
                        return valid;
                    }
                }
            }
        }
        return valid;
    },
    isValid:function () {
        var valid = this.isGridValid();
        if(!valid.valid){
            Ext.toast({
                title : '提示',
                html : valid.message,
                align : 't',
                closable : true,
                slideInDuration : 150,
                minWidth : 300,
                minHeight : 30
            });
            return false;
        }
        return this.callParent();
    },
    isNewValid: function() {
        var me = this,
            fieldcontainer,
            isOk,valid = {valid:true,message:'当前表单'};

        Ext.suspendLayouts();
        me.form.getFields().filterBy(function(field) {
            isOk = field.validate();
            if(!isOk){
                valid.valid = false;
                if(field.config.fieldLabel){
                    valid.message += ' ['+field.config.fieldLabel+']';
                }else if(field.is('filefield')){
                    valid.message += '[附件]';
                }else{
                    fieldcontainer = field.up('fieldcontainer');
                    if(fieldcontainer && fieldcontainer.config.fieldLabel){
                        valid.message += ' ['+fieldcontainer.config.fieldLabel+']';
                    }
                }
            }
            return !isOk;
        });
        Ext.resumeLayouts(true);
        if(!valid.valid){//表单验证
            valid.message += '合法性验证失败！';
            return valid;
		}

        valid = this.isGridValid();
        if(!valid.valid){//表单中的可编辑列表验证
            return valid;
        }

        return valid;
    },
    clearInvalid:function () {
        Ext.suspendLayouts();
        var fields = this.query('field'),i;
        for(i=0 ;i < fields.length; i++){
            if(!fields[i].validation){
                fields[i].clearInvalid();
            }
        }
        var files = this.query('commonFileFieldExp');
        for(i=0 ;i < files.length; i++){
            files[i].clearInvalid();
        }
        Ext.resumeLayouts(true);
    }
});