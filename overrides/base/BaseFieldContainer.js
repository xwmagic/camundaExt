/**为form表单中必填项添加红色*号标志, 针对form中的基本组件 .
 * @author xiaowei
 * @date 2017年5月3日
 */
Ext.define('App.overrides.base.BaseFieldContainer', {
    override: 'Ext.form.FieldContainer',
	initComponent : function() {
		if(this.fieldLabel){
			if (this.allowBlank !== undefined && !this.allowBlank) {
				this.fieldLabel += '<span style="color:red;font-weight:bold;">*</span>';
			}else{
				this.fieldLabel += '<span>&nbsp;&nbsp;</span>';
			}
		}
		
		this.callParent(arguments);
	}
});