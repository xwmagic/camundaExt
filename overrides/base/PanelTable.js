Ext.define('App.overrides.base.PanelTable', {
    override: 'Ext.panel.Table',
    editStatus:true,//编辑状态，为True可以编辑，为false将无法编辑所有内容
    initComponent: function () {
        this.on({
            mousedown: {
                element: 'el',
                fn: function (a) {
                    //修复拖动与点击事件冲突
                    if (a.parentEvent && a.parentEvent.point) {
                        this.component.pointTop = a.parentEvent.point.top;
                        this.component.pointLeft = a.parentEvent.point.left;
                    }
                }
            },
            beforeitemclick: function (a, b, c, d, e, f) {
                //修复拖动与点击事件冲突
                if (e.parentEvent && e.parentEvent.touch && (Math.abs(this.pointTop - e.parentEvent.touch.point.top) > 2 || Math.abs(this.pointLeft - e.parentEvent.touch.point.left) > 2)) {
                    return false
                }
            },
            beforecellclick: function (a, b, c, d, e, f, g, h) {
                //修复拖动与点击事件冲突
                if (g.parentEvent && g.parentEvent.touch && (Math.abs(this.pointTop - g.parentEvent.touch.point.top) > 2 || Math.abs(this.pointLeft - g.parentEvent.touch.point.left) > 2)) {
                    return false
                }
            }
        });
        this.callParent();
    },
    /**
     * 通过名字获取列column
     * @param name
     */
    getColumnByName: function (name) {
        if (!name) {
            return;
        }
        var cols = this.getColumns();
        for (var i = 0; i < cols.length; i++) {
            if (cols[i].name == name) {
                return cols[i];
            }
        }
    },
    isCanEdit: function (index, record) {
        return this.editStatus;
    },
    /**
     * 获取选择的数据,带有提示
     * isAlert true 将弹出提示
     */
    getSelectDatas: function (isAlert) {
        var sels = this.getSelectionModel().getSelection();
        var selDatas = [];
        if (sels.length > 0) {
            for (var i in sels) {
                selDatas.push(Ext.clone(sels[i].data));
            }
        }else{
            if(isAlert){
                App.ux.Toast.show('提示', '请在列表中勾选需要的数据！','i');
                return [];
            }
        }
        return selDatas;
    },
    /**
     * 获取选择一条数据,带有提示
     * isAlert true 将弹出提示
     */
    getSelectOnlyOne:function (isAlert) {
        var selections;
        selections = this.getSelectionModel().getSelection();

        if (selections.length !== 1) {
            if(isAlert){
                App.ux.Toast.show('提示', '请在列表中选中一条数据！','i');
            }
            return;
        }
        return selections[0].data;
    },
    /**
     * 获取选择的数据,带有提示
     * isAlert true 将弹出提示
     */
    getSelectRecords: function (isAlert) {
        var sels = this.getSelectionModel().getSelection();
        if (sels.length > 0) {
            return sels;
        }else{
            if(isAlert){
                App.ux.Toast.show('提示', '请正列表中勾选需要的数据！','i');
                return [];
            }
        }
    },
    /**
     * 获取选择一条数据,带有提示
     * isAlert true 将弹出提示
     */
    getSelectOnlyRecord:function (isAlert) {
        var selections;
        selections = this.getSelectionModel().getSelection();

        if (selections.length !== 1) {
            if(isAlert){
                App.ux.Toast.show('提示', '请在列表中选中一条数据！','i');
            }
            return;
        }
        return selections[0];
    }
});