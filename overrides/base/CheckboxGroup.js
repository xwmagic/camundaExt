/**使得所有CheckboxGroup具备readOnly只读功能
 * @author xiaowei
 * @date 2017年6月22日
 */
Ext.define('App.overrides.base.CheckboxGroup', {
    override: 'Ext.form.CheckboxGroup',
	readOnly: false,
	initComponent : function() {
		/*this.on({
			afterrender: function(){
				if(this.defaults && this.defaults.hasOwnProperty('readOnly')){
                    this.readOnly = this.defaults.readOnly;
					this.setReadOnly(this.defaults.readOnly);
				}else{
					this.setReadOnly(this.readOnly);
				}
				
			}
		});*/
		this.callParent();
        this.setReadOnly(this.readOnly);
	},
	setReadOnly: function(flag){

		var radios = this.query('checkboxfield');
		var i;
		if(flag){
			this.setStyle({background: '#f9f9f9'});
		}else{
			this.setStyle({background: 'transparent'});
		}
		for(i=0; i<radios.length; i++){
			radios[i].setReadOnly(flag);
		}
	}
});