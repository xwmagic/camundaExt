/**
 * Created by csot.qhlinyunfu on 2018/10/11.
 */
Ext.define('App.overrides.base.Monitor', {
    override: 'Ext.container.Monitor',

    /**
     * 重写这个方法，解决grid中有附件控件提交时没有传附件信息到后台的问题
     * @returns {Ext.util.MixedCollection}
     */
    getItems: function () {
        var me = this,
            items = me.items,
            grids;
        if (!items) {
            items = me.items = new Ext.util.MixedCollection();
            items.addAll(me.target.query(me.selector));
        }
        // 一定要在grid里添加这个hasFileField:true属性
        if (me.target) {
            grids = me.target.query('grid[hasFileField=true]');
            if(grids.length > 0){
                var file;
                for(var i=0; i<grids.length; i++){
                    file = grids[i].getFileFields();
                    items.addAll(file.files);
                    items.addAll(file.hiddens);
                }
            }
        }
        return items;
    }
});