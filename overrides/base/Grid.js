//每一个列都会出现鼠标悬浮上去显示内容
/**
 * //适用于Extjs4.x
 * @class Ext.grid.GridView
 * @override Ext.grid.GridView
 * GridPanel单元格不能选中复制问题
 * 单元格数据显示不完整 ,增加title 浮动提示信息
 */
Ext.define('App.overrides.base.Grid', {
    override: 'Ext.grid.GridPanel',
    cellTip: true,
    reserveScrollbar: false,
    excludeDatas: [],
    excludeFieldName: 'id',
    excludeMeFieldName: 'id',
    /**
     * 是否开启排除 excludeDatas数组中的数据，
     * true时每次请求回来的数据，都将移除与excludeDatas中相同的数据。
     * 移除前提条件是：excludeDatas中数据的的字段名(当前列表的excludeFieldName)的值 与 当前列表的字段名（配置的excludeMeFieldName）的值相同的数据将被移除。
     */
    isExcludeDatas: false,
    initComponent: function () {
        this.on({
            beforerender: function () {
                var me = this;
                this.getStore().grid = this;
                this.getStore().on({
                    load: function () {
                        if (me.isExcludeDatas && me.excludeDatas) {
                            Ext.suspendLayouts();
                            var index;
                            for (var i in me.excludeDatas) {
                                index = this.find(me.excludeMeFieldName, me.excludeDatas[i][me.excludeFieldName]);
                                if (index < 0) {
                                    continue;
                                }
                                this.removeAt(index);
                            }
                            Ext.resumeLayouts(true);
                        }
                    }
                });

            }/*,
            mousedown: {
                element: 'el',
                fn: function (a) {
                    //修复拖动与点击事件冲突
                    if (a.parentEvent) {
                        this.component.pointTop = a.parentEvent.point.top;
                        this.component.pointLeft = a.parentEvent.point.left;
                    }
                }
            },
            beforeitemclick: function (a, b, c, d, e, f) {
                //修复拖动与点击事件冲突
                if (e.parentEvent && (Math.abs(this.pointTop - e.parentEvent.touch.point.top) > 2 || Math.abs(this.pointLeft - e.parentEvent.touch.point.left) > 2)) {
                    return false
                }
            },
            beforecellclick: function (a, b, c, d, e, f, g, h) {
                //修复拖动与点击事件冲突
                if (g.parentEvent && (Math.abs(this.pointTop - g.parentEvent.touch.point.top) > 2 || Math.abs(this.pointLeft - g.parentEvent.touch.point.left) > 2)) {
                    return false
                }
            }*/
        });
        this.callParent();
    },
    //添加数据移除单元格提示
    afterRender: Ext.Function.createSequence(Ext.grid.GridPanel.prototype.afterRender,
        function () {
            // 默认显示提示
            if (!this.cellTip) {
                return;
            }

            var view = this.getView();

            this.tip = new Ext.ToolTip({
                target: view.el,
                delegate: '.x-grid-cell-inner',
                trackMouse: true,
                renderTo: document.body,
                ancor: 'top',
                style: 'background-color: #FFFFCC;',
                listeners: {
                    beforeshow: function updateTipBody(tip) {
                        if (tip.triggerElement.clientWidth >= tip.triggerElement.scrollWidth) {
                            return false;
                        }
                        //取cell的值
                        //fireFox  tip.triggerElement.textContent
                        //IE  tip.triggerElement.innerText
                        var tipText = (tip.triggerElement.innerText || tip.triggerElement.textContent);
                        if (Ext.isEmpty(tipText) || Ext.isEmpty(tipText.trim())) {
                            return false;
                        }

                        tip.update(Ext.Component.htmlEscape(tipText));
                    }
                }
            });
        }
    ),
    /**
     * 数据请求成功后的处理数据
     * @param records
     * @returns {records}
     */
    beforeAddDatas: function (records) {
        //自己的处理逻辑
        return records;
    },
    _refresh: function () {
        this.getStore().load();
    },
    showHideColumn:function () {
        var cols = this.getColumns();
        var hiddenButtons = this._hiddenButtons;
        Ext.suspendLayouts();
        if(hiddenButtons){
            for(var key in hiddenButtons){
                if(hiddenButtons[key].hidden){
                    hiddenButtons[key].show();
                }else{
                    hiddenButtons[key].hide();
                }
            }
        }else{
            hiddenButtons = [];
            for(var key in cols){
                if(cols[key].hidden){
                    cols[key].show();
                    hiddenButtons.push(cols[key]);
                }
            }
            this._hiddenButtons = hiddenButtons;
        }
        Ext.resumeLayouts(true);
    },
    /**
     * 检查数据是否有更新，不包含新增和删除的数据
     * @returns {boolean}
     */
    hasDataUpdate:function () {
        if(this.getStore().getUpdatedRecords().length > 0){
            return true;
        }else {
            return false;
        }
    }
});