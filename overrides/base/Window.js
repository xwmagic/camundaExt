//每一个列都会出现鼠标悬浮上去显示内容
/**
 * window
 */
Ext.define('App.overrides.base.Window', {
    override: 'Ext.window.Window',
    initComponent: function () {
        if (!Ext.platformTags.desktop && !this.buttonAlign) {//移动设备默认按钮位置为中部
            this.buttonAlign = 'center';
        }
        this.callParent();
    }
});