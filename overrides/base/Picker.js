/**使得所有ComboBox具备双击展开功能
 * @author xiaowei
 * @date 2017年6月22日
 */
Ext.define('App.overrides.base.Picker', {
    override: 'Ext.form.field.Picker',
    keyValue:'',//键值
    keyName:'',//键名
    dynamicAddMode:false,
    initComponent:function () {
        if(this.dynamicAddMode){
            this.on({
                blur:function () {
                    var val = this.getRawValue();
                    if(this.value !== val){
                        this.setValue(val);
                        this.setKeyValue(val);
                    }
                }
            });
        }
        this.callParent();
    },
    setKeyValue:function (value) {
        this.keyValue = value;
    },
    getKeyValue:function () {
        return this.keyValue;
    }
});