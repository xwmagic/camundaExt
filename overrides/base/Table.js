
Ext.define('App.overrides.base.Table', {
    override: 'Ext.view.Table',
    //使用自定义样式，表格内容垂直居中
    cellValues: {
        classes: [
            Ext.baseCSSPrefix + 'grid-cell ' + Ext.baseCSSPrefix + 'grid-td-autoLinefeed' // for styles shared between cell and rowwrap
        ]
    }
});