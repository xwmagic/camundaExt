/**GridColumn
 * @author xiaowei
 * @date 2017年7月7日
 */
Ext.define('App.overrides.base.Button', {
    override: 'Ext.button.Button',
    autoShowTooltip: false,
    /**
     * 自定义类型，不同的类型有不同的样式
     * add,edit,del,search,notAction
     */
    cusType: '',
    initComponent: function () {
        var me = this;
        this.setCusTypeStyle();
        //防止拖动按钮时触发按钮点击事件
        me.on({
            mousedown: {
                element: 'el',
                fn: function (a) {
                    //修复拖动与点击事件冲突
                    if (a.parentEvent && a.parentEvent.touch && a.parentEvent.touch.point) {
                        this.component.pointTop = a.parentEvent.point.top;
                    }
                }
            },
            click: function (a, b, c, d, e, f) {
                //修复拖动与点击事件冲突
                if (b.parentEvent && b.parentEvent.touch && b.parentEvent.touch.point && Math.abs(this.pointTop - b.parentEvent.touch.point.top) > 2) {
                    return false
                }
            }
        });
        this.callParent(arguments);
    },
    setCusTypeStyle: function () {
        if(this.cls){//如果存在，不进行样式设置
            return;
        }
        switch (this.cusType) {
            case 'edit':
                this.cls = 'stars-button-edit';
                break;
            case 'del':
                this.cls = 'stars-button-alert';
                break;
            case 'search':
                this.cls = 'stars-button-search';
                break;
            case 'notAction':
                this.cls = 'stars-button-notaction';
                break;
        }
    },
    listeners: {
        afterrender: function () {
            if (this.autoShowTooltip) {
                this.showTooltip();
            }
        }
    },
    showTooltip: function (title) {
        if (this.hidden) {
            return;
        }
        var me = this;
        if (!title) {
            title = me.tooltip;
        }
        if (!title) {
            title = me.text;
        }

        var tip = Ext.create('Ext.tip.ToolTip', {
            target: me,
            dismissDelay: 10000,
            html: '<span style="color:#d63128;">操作指引：</span></br>  —— ' + title
        });

        setTimeout(function () {
            if (me.hidden || me.disabled) {
                return;
            }
            tip.show();
            setTimeout(function () {
                tip.destroy();
            }, 10000)
        }, 800)
    }

});