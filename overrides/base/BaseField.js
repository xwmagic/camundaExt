/**
 * 为form表单中必填项添加红色*号标志, 针对form中的基本组件 .
 * 
 * create by gongzhenwen 2016/9/1 9:00
 */
Ext.define('App.overrides.base.BaseField', {
    override: 'Ext.form.field.Base',
	initComponent : function() {

    	if (this.allowBlank !== undefined && !this.allowBlank) {
			if (this.fieldLabel) {

				this.fieldLabel += '<span style="color:red;font-weight:bold;padding:0px 2px 0px 0px;">*</span>';
			}
		}else{
			if (this.fieldLabel) {
				this.fieldLabel += '<span>&nbsp;&nbsp;</span>';
			}
		}

		if(this.readOnly){
			this.fieldStyle = {
				background:'#f9f9f9',
				color: '#999'
			};
		}
		
		this.callParent(arguments);
	},
	setReadOnly: function(readOnly) {
        var me = this,
            inputEl = me.inputEl,
            old = me.readOnly;

        readOnly = !!readOnly;
        me[readOnly ? 'addCls' : 'removeCls'](me.readOnlyCls);
        me.readOnly = readOnly;
        
        if (inputEl) {
            inputEl.dom.readOnly = readOnly;
            me.ariaEl.dom.setAttribute('aria-readonly', readOnly);
        }
        else if (me.rendering) {
            me.setReadOnlyOnBoxReady = true;
        }
        if (readOnly !== old) {
            me.fireEvent('writeablechange', me, readOnly);
        }
		
		if(readOnly){
			me.setFieldStyle({
				background:'#f9f9f9',
				color: '#999'
			});
			
		}else{
			me.setFieldStyle({
				background:'#fff',
				color: '#000'
			});
			
		}
    },
	setAllowBlank: function(boolea){
		if(this.allowBlank === boolea){
			return;
		}
		this.allowBlank = boolea;
		if(this.fieldLabel){
			if(boolea === false){
				if(this.labelWidth){
					this.labelWidth = this.labelWidth + 5;
				}
				this.setFieldLabel(this.fieldLabel + '<span style="color:red;font-weight:bold;">*</span>');
				
			}else{
				if(this.labelWidth){
					this.labelWidth = this.labelWidth - 5;
				}
				var label = this.fieldLabel.split('<span')[0];
				this.setFieldLabel(label);
				
			}
		}
	}
});