/**使得所有ComboBox具备双击展开功能
 * @author xiaowei
 * @date 2017年6月22日
 */
Ext.define('App.overrides.base.Combobox', {
    override: 'Ext.form.ComboBox',
    queryMode: 'local',
    forceSelection: true,
    //动态添加模式，当用户输入值时，如果值在store中不存在，那么失去光标时，自动加入store，达到用户可以输入不在combobox中的数据。
    dynamicAddMode:false,
    initComponent:function () {
        this.queryParam = this.displayField;
        this.on({
            dblclick:{
                element: 'el',
                fn: function(){
                    //只读状态下，禁止展开
                    if(this.component && (this.component._isView || this.component.readOnly)){
                        return false;
                    }
                    this.component.expand();
                }
            }
        });
        if(this.dynamicAddMode){
            this.on({
                blur:function () {
                    var val = this.getRawValue();
                    var record = this.getStore().findRecord('name',val);
                    if(!record){
                        var data = {};
                        data[this.valueField] = val;
                        data[this.displayField] = val;
                        this.getStore().loadData([data],true);
                    }
                }
            });
        }
        this.callParent();
    }
});