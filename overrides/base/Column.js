/**GridColumn
 * @author xiaowei
 * @date 2017年7月7日
 */
Ext.define('App.overrides.base.Column', {
    override: 'Ext.grid.column.Column',
	isPercent:false,
	percentFormat:'0.00',
	htmlEscape:false,//是否转换html关键字后进行显示,防止Xss攻击
    editEffect:false,//是否启用编辑效果
    dateFormat:'Y-m-d',//日期格式化
    isDate:false,//是否是日期
    //align:'center',
    autoLinefeed:false,//是否自动换行
    canEditColor:'#f1fffd',
    notCanEditColor:'#ffffff',
    numberFormat:'',
    color:'',//内容颜色标记
    initComponent : function() {
        var me = this;
        /*this.setStyle({
            'font-size':'6px;',
            //padding:'2px;'
        });*/
        if(this.isPercent && !this.renderer){
            this.renderer = function (val) {
                if (val && val != 'null')
                    return Ext.util.Format.number(val * 100, me.percentFormat) + '%';
                return val;
            }
		}

		if(this.isDate){
            this._isDate_renderer = Ext.clone(this.renderer);
            if(this.renderer){
                this.renderer = function (val,a,b,c,d,e,f,g) {
                    if (val && val != 'null'){
                        return me._isDate_renderer(Ext.util.Format.date(val,me.dateFormat),a,b,c,d,e,f,g);
                    }else{
                        return me._isDate_renderer(val,a,b,c,d,e,f,g)
                    }
                }
            }else{
                this.renderer = function (val) {
                    if (val && val != 'null'){
                        return Ext.util.Format.date(val,me.dateFormat);
                    }
                }
            }
        }

		if(this.numberFormat){
            if(this.renderer){
                this._numberFormat_renderer = Ext.clone(this.renderer);
                this.renderer = function (val,a,b,c,d,e,f,g) {
                    if(Ext.isNumber(me._numberFormat_renderer(val,a,b,c,d,e,f,g))){
                        return Ext.util.Format.number(me._numberFormat_renderer(val,a,b,c,d,e,f,g),this.numberFormat)
                    }else {
                        return me._numberFormat_renderer(val,a,b,c,d,e,f,g)
                    }
                }
            }else{
                this.renderer = Ext.util.Format.numberRenderer(this.numberFormat)
            }
        }
		if(this.color){
            if(this.renderer){
                this._color_renderer = Ext.clone(this.renderer);
                this.renderer = function (val,a,b,c,d,e,f,g) {
                    return "<span style='color: "+me.color+"'>" + me._color_renderer(val,a,b,c,d,e,f,g) + "</span>";
                }
            }else{
                this.renderer = function (val) {
                    return "<span style='color: "+me.color+"'>" + val + "</span>";
                }
            }
        }

        if(this.htmlEscape){
            if(this.renderer){
                this._temp_renderer = Ext.clone(this.renderer);
                this.renderer = function (val,a,b,c,d,e,f,g) {
                    return Ext.Component.htmlEscape(me._temp_renderer(val,a,b,c,d,e,f,g));
                }
            }else{
                this.renderer = function (val) {
                    if (val)
                        return Ext.Component.htmlEscape(val);
                    return val;
                }
            }

        }

        if(this.editEffect){
            if(this.renderer){
                this._temp_editEffect_renderer = Ext.clone(this.renderer);
                this.renderer = function (val, cell,a,b,c,d,e,f) {
                    if(cell){
                        cell.tdStyle = "background-color:"+this.canEditColor;
                    }
                    me.columnRenderer(val, cell,a);
                    return me._temp_editEffect_renderer(val, cell,a,b,c,d,e,f);
                }
            }else{
                this.renderer = function (val, cell,a) {
                    if(cell){
                        cell.tdStyle = "background-color:"+this.canEditColor;
                    }
                    me.columnRenderer(val, cell,a);
                    return val;
                }
            }

        }
        //自动换行
        if(this.autoLinefeed){
            if(this.renderer){
                this._temp_autoLinefeed_renderer = Ext.clone(this.renderer);
                this.renderer = function (val, cell,a,b,c,d,e,f) {
                    if(!val) {return me._temp_autoLinefeed_renderer(val, cell,a,b,c,d,e,f);}
                    return '<div style="white-space: normal;word-wrap:break-word;vertical-align: middle;">'+me._temp_autoLinefeed_renderer(val, cell,a,b,c,d,e,f)+'</div>';
                }
            }else{
                this.renderer = function (val) {
                    if(!val) {return;}
                    return '<div style="white-space: normal;word-wrap:break-word;vertical-align: middle;">'+val+'</div>';
                }
            }
        }

		this.callParent(arguments);
	},
    columnRenderer: function (value, metaData, record) {
        if (metaData && record && this.up('tablepanel').isCanEdit) {
            if (this.up('tablepanel').isCanEdit(metaData.column.fullColumnIndex, record)) {
                metaData.tdStyle = "background-color:"+this.canEditColor;
            }else{
                metaData.tdStyle = "background-color:"+this.notCanEditColor;
            }
        }
    }
	
});