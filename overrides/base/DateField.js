/**
 * 重写的DateField，时间控件默认不可编辑.
 * 
 * create by xiaowei 2017/7/24 9:00
 */
Ext.define('App.overrides.base.DateField', {
    override: 'Ext.form.field.Date',
	editable: true,
    submitFormat:'Y-m-d',
    format:'Y-m-d'
});