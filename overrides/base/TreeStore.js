/**让treestore具有复选框功能
 * @author xiaowei
 * @date 2017年5月3日
 */
Ext.define('App.overrides.base.TreeStore', {
    override: 'Ext.data.TreeStore',
    isCheckTree: false,//将树转换为带复选框的树
    forceNoCheckTree: false,//将在将树转换为不带复选框的树
    parentIdProperty:'pid',
    onProxyLoad: function (operation) {
        if (operation._resultSet && operation._resultSet.records && operation._resultSet.records.length > 0) {
            this.convertCheckTree([operation._resultSet.records[0].data]);
        }

        this.callParent([operation]);
    },
    convertCheckTree: function (datas) {
        if(!datas){
            return;
        }
        var i;
        if (this.forceNoCheckTree) {
            for (i = 0; i < datas.length; i++) {
                delete datas[i].checked;
                if (datas[i].children && datas[i].children.length > 0) {
                    this.convertCheckTree(datas[i].children);
                }
            }
        } else if (this.isCheckTree) {
            for (i = 0; i < datas.length; i++) {
                if (!datas[i].checked) {
                    datas[i].checked = false;
                    if (datas[i].children && datas[i].children.length > 0) {
                        this.convertCheckTree(datas[i].children);
                    }
                }
            }
        }
    }
});