Ext.define("App.overrides.Component", {
    override: 'Ext.Component',
    statics: {
        //转换html关键字
        htmlEscape: function (text) {
            if (Ext.isEmpty(text)) {
                return;
            }
            return text.replace(/[<>"&]/g, function (match, pos, originalText) {
                switch (match) {
                    case "<":
                        return "&lt;";
                    case ">":
                        return "&gt;";
                    case "&":
                        return "&amp;";
                    case "\"":
                        return "&quot;";
                }
            });
        },
        checkPhone: function (phone) {
            if (!(/^1[3456789]\d{9}$/.test(phone))) {

                return false;
            }
            return true;
        },
        getDate: function (date) {
            if (Ext.isString(date)) {
                return new Date(Date.parse(date));
            } else {
                return date;
            }
        },
        /**
         * 获取某个日期N个工作日之后的日期
         * @param startDate 开始日期
         * @param limitDay 工作日天数
         * @returns {*} Date
         */
        getWorkDate: function (startDate, limitDay) {
            var time, startTime;
            if (Ext.isString(startDate)) {
                startTime = new Date(Date.parse(startDate));
            } else {
                startTime = startDate;
            }
            //var startTime = new Date(Date.parse(startDate));
            var startTime = startTime.getTime();
            var T = 24 * 60 * 60 * 1000;
            var endTime = startTime + (limitDay * T);
            if (limitDay > 0) {
                var holidays = 0;
                for (var i = startTime + T; i <= endTime; i += T) {
                    var date = new Date(i);
                    //此处为节假日逻辑
                    if (date.getDay() == 0 || date.getDay() == 6) {
                        holidays++;
                    }
                    //判断日期是否在节假日数组中
                    /*if (Ext.Date.isInArray(Holiday, date.toLocaleDateString()) == true) {
                     holidays++;
                     }*/
                }
                return this.getWorkDate(new Date(endTime), holidays);
            } else {
                return startDate;
            }
        },
        isInArray: function (arr, value) {
            for (var i = 0; i < arr.length; i++) {
                if (value === arr[i]) {
                    return true;
                }
            }
            return false;
        },
        getAfterDate: function (dateStr, day) {
            if (!dateStr) {
                return '';
            }
            var date;
            var str = "";
            if (Ext.isString(dateStr)) {
                // 转换日期格式
                str = dateStr.replace(/-/g, '/'); // "2010/08/01";
                str = str.split('T')[0];
                // 创建日期对象
                date = new Date(str);
            } else {
                date = Ext.clone(dateStr);
            }
            date.setDate(date.getDate() + day);

            return date;
        },
        /**
         * 获取一个颜色包裹文字的显示效果
         * @param v
         * @param color
         * @returns {string}
         */
        getColorView: function (v, color) {
            if (!v) {
                return;
            }
            return "<span style='font-weight: bold;background:" + color +
                ";border-color:" + color +
                ";color:#fff;padding: 3px 6px;" + "'>" + v + "</span>";
        },
        /**
         * 驼峰转下横杠数据库字段命名方式
         * @param str
         * @returns string
         */
        camelCase: function (str) {
            var strArr = str.split('_');
            for (var i = 0; i < strArr.length; i++) {
                strArr[i] = strArr[i].charAt(0).toUpperCase() + strArr[i].substring(1).toLowerCase();
            }
            strArr = strArr.join('');
            strArr = strArr.charAt(0).toLowerCase() + strArr.substring(1);
            return strArr;
        },
        /**
         * 驼峰转下横杠数据库字段命名方式
         * 正则表达式版本
         * @param str
         * @returns string
         */
        camelToField: function (name) {
            return name.replace(/([A-Z])/g, "_$1").toLowerCase();
        },
        /**
         * 下横杠字段转驼峰
         * @param str
         * @returns string
         */
        fieldToCamel: function (name) {
            return name.replace(/\_(\w)/g, function (all, letter) {
                return letter.toUpperCase();
            });
        },
        /**
         * 对象深度复制，类似于Ext.applyIf 区别在于，该方法能进行对象的深度复制
         * @param object
         * @param config
         */
        depthCopyIf: function (object, config) {
            var key;
            for (key in config) {
                if (!object.hasOwnProperty(key)) {//object不存在该属性，那么进行克隆复制
                    object[key] = Ext.clone(config[key]);
                    continue;
                }
                if (Ext.isObject(config[key]) && Ext.isObject(object[key])) {
                    //深度复制
                    Ext.Component.depthCopyIf(object[key],config[key]);
                    continue;
                }
            }
        }
    }


});
